﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_IsSearchedNotFoundToVisibilityPlus : IMultiValueConverter
    {
        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Collapsed;

            try
            {
                /// Always test MultiValueConverter inputs for non-null
                /// (to avoid crash bugs for views in the designer)
                if (values[0] is bool && values[1] is int)
                {
                    bool IsSearched = (bool)values[0];
                    int CountCards = (int)values[1];

                    if (IsSearched
                        && CountCards == 0)
                    {
                        returnValue = Visibility.Visible;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
