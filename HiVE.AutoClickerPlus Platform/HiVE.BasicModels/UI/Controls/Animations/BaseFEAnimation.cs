﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.PageTransitions;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace HiVE.BasicModels.Animations
{
    public class BaseFEAnimation : FrameworkElement
    {
        public BaseFEAnimation()
        {

        }

        #region Feilds

        private TransitionType _currentTransitionType;

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty TransitionTypeProperty =
            DependencyProperty.Register(
                "TransitionType",
                typeof(TransitionType),
                typeof(BaseFEAnimation),
                new PropertyMetadata(TransitionType.Grow));

        public TransitionType TransitionType
        {
            get
            {
                return (TransitionType)GetValue(TransitionTypeProperty);
            }
            set
            {
                SetValue(TransitionTypeProperty, value);
            }
        }

        #endregion

        /// <summary>
        /// Needs for test in different displays
        /// </summary>
        /// <param name="frameworkElement">the page or window or user control that need to be scale</param>
        public void AnimatedEventIsPressed(object sender)
        {
            try
            {
                /// We may have already set the LayoutTransform to a ScaleTransform.
                /// If not, do so now.
                var scaler = (sender as FrameworkElement).LayoutTransform as ScaleTransform;

                /// ScaleTransform for click event animation
                #region ScaleTransform

                if (scaler == null)
                {
                    scaler = new ScaleTransform(1.0, 1.0);
                    (sender as FrameworkElement).LayoutTransform = scaler;
                }

                /// We'll need a DoubleAnimation object to drive 
                /// the ScaleX and ScaleY properties.
                DoubleAnimation animator_Grow = new DoubleAnimation()
                {
                    Duration = BasicMethods.BasicAnimatedEventIsPressedDuration,
                    AutoReverse = true,
                };

                /// Toggle the scale between 1.0 and 0.375.
                /// Scale Display from 1920*1080 to 720*405
                if (scaler.ScaleX == 1.0)
                { animator_Grow.To = 0.9; }
                else { animator_Grow.To = 1.0; }

                scaler.BeginAnimation(ScaleTransform.ScaleXProperty, animator_Grow);
                scaler.BeginAnimation(ScaleTransform.ScaleYProperty, animator_Grow);

                #endregion

                /// Dynamic Storyboard for click evrnt animation
                /// No completed!
                #region Dynamic Storyboard

                _currentTransitionType = TransitionType;

                if (_currentTransitionType == TransitionType.Default)
                {
                    _currentTransitionType = BasicMethods.CurrentMainSettings.BasicDefaultTransitionType;

                    if (_currentTransitionType == TransitionType.Default)
                    {
                        _currentTransitionType = TransitionType.Fade;
                    }
                }

                Storyboard hidePage = (Application.Current.FindResource(string.Format("{0}Out", _currentTransitionType.ToString())) as Storyboard).Clone();

                hidePage.AutoReverse = true;

                //hidePage.Begin(sender as FrameworkElement);

                #endregion
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Need for Dynamic Storyboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FadeOutCompleted(object sender, EventArgs e)
        {
            try
            {
                // after the fade out
                //   1. re-enable hittesting
                //   2. initiate the delayed navigation
                //   3. invoke the FadeIn animation at Loaded priority

                if ((sender as ClockGroup) != null)
                { (sender as ClockGroup).Completed -= FadeOutCompleted; }

                //if (_contentPresenter != null)
                {
                    //_contentPresenter.IsHitTestVisible = true;

                    Dispatcher.BeginInvoke(DispatcherPriority.Loaded,
                        (ThreadStart)delegate ()
                        {
                            /// We may have already set the LayoutTransform to a ScaleTransform.
                            /// If not, do so now.
                            var scaler = (sender as FrameworkElement).LayoutTransform as ScaleTransform;

                            if (scaler == null)
                            {
                                scaler = new ScaleTransform(1.0, 1.0);
                                (sender as FrameworkElement).LayoutTransform = scaler;
                            }

                            /// We'll need a DoubleAnimation object to drive 
                            /// the ScaleX and ScaleY properties.
                            DoubleAnimation animator = new DoubleAnimation()
                            { Duration = new Duration(TimeSpan.FromMilliseconds(100)), };

                            /// Toggle the scale between 1.0 and 0.375.
                            /// Scale Display from 1920*1080 to 720*405
                            if (scaler.ScaleX == 1.0)
                            { animator.To = 0.8; }
                            else { animator.To = 1.0; }

                            _currentTransitionType = TransitionType;

                            if (_currentTransitionType == TransitionType.Default)
                            {
                                _currentTransitionType = BasicMethods.CurrentMainSettings.BasicDefaultTransitionType;

                                if (_currentTransitionType == TransitionType.Default)
                                {
                                    _currentTransitionType = TransitionType.Fade;
                                }
                            }

                            Storyboard showNewPage = (Application.Current.FindResource(string.Format("{0}In", _currentTransitionType.ToString())) as Storyboard).Clone();

                            scaler.BeginAnimation(ScaleTransform.ScaleXProperty, animator);
                            scaler.BeginAnimation(ScaleTransform.ScaleYProperty, animator);

                            //showNewPage.Begin(sender as FrameworkElement);
                        });
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
