﻿using HiVE.BasicModels.PageTransitions;
using HiVE.BasicModels.BoxItems;

namespace HiVE.AutoClickerPlus.Utility.Helpers
{
    public class Currents
    {
        #region Extra Data Contents

        public static BoxMainSettings CurrentMainSettings { get; set; }
        public static BoxRestauranDetails CurrentRestauran { get; set; }

        #endregion

        #region Basic Properties

        public static FrameTransition CurrentFrameTransition { get; set; }
        public static BoxInformationDetails CurrentInformationDetails { get; set; }

        public static MainPage CurrentMainPage { get; set; }

        #endregion

        #region Details Functions

        #endregion
    }
}
