﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Controls;
using static HiVE.BasicModels.BoxTreeNodes.BoxTreeNodeMethods;

namespace HiVE.BasicModels.BoxTreeNodes
{
    /// <summary>
    /// Represents a node in the FooBoxTreeNode. A node may have a name, text, font style, image and 
    /// may contain child nodes. If so, it can be expanded or collapsed.
    /// </summary>
    [DefaultProperty("Text")]
    public class FooBoxTreeNode : IComparable<FooBoxTreeNode>, INotifyPropertyChanged
    {
        #region Feilds

        private FooBoxTreeNode _parent;

        private string _name;
        private string _text;
        private string _toolTip;
        private object _tag;

        private byte[] _imageByteArray;
        private string _background { get; set; }
        private string _description { get; set; }
        private UserControl _userControlPath { get; set; }

        private MethodCheckState _checkState;
        private bool _threeState;
        private bool _cascadeCheckState;

        private int _imageIndex;
        private string _imageKey;
        private bool _expanded;
        private int _expandedImageIndex;
        private string _expandedImageKey;

        private bool _isInitiallySelected;

        private FooBoxTreeNodeCollection _nodes;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the node that owns this node, or null for a top-level node.
        /// </summary>
        [Browsable(false)]
        public FooBoxTreeNode Parent
        {
            get { return _parent; }
            internal set { _parent = value; }
        }

        /// <summary>
        /// Gets or sets the name of the node.
        /// </summary>
        [Description("The name of the node."), DefaultValue(""), Category("Design")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        /// <summary>
        /// Gets or sets the text displayed on the node.
        /// </summary>
        [DefaultValue("BoxTreeNode"), Description("The text displayed on the node."), Category("Appearance")]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
        /// <summary>
        /// Gets or sets the tooltip text associated with this node.
        /// </summary>
        [DefaultValue(""), Description("The tooltip text associated with this node."), Category("Appearance")]
        public string ToolTip
        {
            get { return _toolTip; }
            set { _toolTip = value; }
        }
        /// <summary>
        /// Gets or sets a user-defined object associated with this BoxTreeNode.
        /// </summary>
        [Description("User-defined object associated with this BoxTreeNode."), DefaultValue(""), Category("Data"), TypeConverter(typeof(StringConverter))]
        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        /// <summary>
        /// Gets or sets the image displayed on the node.
        /// </summary>
        [DefaultValue("BoxTreeNode"), Description("The image displayed on the node."), Category("Appearance")]
        public byte[] ImageByteArray
        {
            get { return _imageByteArray; }
            set { _imageByteArray = value; }
        }
        /// <summary>
        /// Gets or sets the background displayed on the node.
        /// </summary>
        [DefaultValue("BoxTreeNode"), Description("The background displayed on the node."), Category("Appearance")]
        public string Background
        {
            get { return _background; }
            set { _background = value; }
        }
        /// <summary>
        /// Gets or sets the description displayed on the node.
        /// </summary>
        [DefaultValue("BoxTreeNode"), Description("The description displayed on the node."), Category("Appearance")]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        /// <summary>
        /// Gets or sets a user-defined UserControl associated with this BoxTreeNode.
        /// </summary>
        [Description("User-defined UserControl associated with this BoxTreeNode."), DefaultValue(""), Category("Data")]
        public UserControl UserControlPath
        {
            get { return _userControlPath; }
            set { _userControlPath = value; }
        }

        /// <summary>
        /// Gets or sets the check state when the <see cref="FooBoxTreeNode.ShowCheckBoxes"/> property is set to true.
        /// </summary>
        [DefaultValue(MethodCheckState.Unchecked), Category("Appearance")]
        public MethodCheckState CheckState
        {
            get { return _checkState; }
            set
            {
                bool diff = (_checkState != value);
                _checkState = value;
                if (diff) OnCheckStateChanged();
            }
        }
        /// <summary>
        /// Gets/sets the state of the associated UI toggle (ex. CheckBox).
        /// The return value is calculated based on the check state of all
        /// child FooBoxTreeNode.  Setting this property to true or false
        /// will set all children to the same check state, and setting it 
        /// to any value will cause the parent to verify its check state.
        /// </summary>
        public bool? CheckStateToBoolean
        {
            get
            {
                switch (_checkState)
                {
                    case MethodCheckState.Checked: { return true; }
                    case MethodCheckState.Unchecked: { return false; }
                    case MethodCheckState.Indeterminate: { return null; }
                }
                return null;
            }
            set
            {
                switch (value)
                {
                    case true:
                        {
                            _checkState = MethodCheckState.Checked;
                            break;
                        }
                    case false:
                        {
                            _checkState = MethodCheckState.Unchecked;
                            break;
                        }
                    case null:
                        {
                            _checkState = MethodCheckState.Indeterminate;
                            break;
                        }
                }
            }
        }
        /// <summary>
        /// Gets or sets the checked state when the <see cref="FooBoxTreeNode.ShowCheckBoxes"/> property is set to true.
        /// </summary>
        [DefaultValue(false), Category("Appearance")]
        public bool? IsChecked
        {
            get { return (CheckStateToBoolean); }
            set { this.SetIsChecked(value, true, true); }
        }
        /// <summary>
        /// Gets or sets a value indicating whether node checkboxes move into the <see cref="CheckState.Indeterminate"/> state after the <see cref="CheckState.Checked"/> state.
        /// </summary>
        [DefaultValue(false), Description("Determines whether node checkboxes move into the indeterminate/mixed state after the checked state."), Category("Behavior")]
        public bool ThreeState
        {
            get { return _threeState; }
            set { _threeState = value; }
        }
        /// <summary>
        /// Gets or sets a value indicating whether the check state of a node is 
        /// determined by its child nodes, and vice versa. If set to true, this 
        /// means that only the check state of leaf nodes is significant.
        /// </summary>
        [DefaultValue(true), Description("Determines whether the check state of a node is determined by its child nodes, and vice versa."), Category("Behavior")]
        public bool CascadeCheckState
        {
            get
            {
                return _cascadeCheckState;
            }
            set
            {
                bool diff = (_cascadeCheckState != value);
                _cascadeCheckState = value;

                if (diff && _cascadeCheckState)
                {
                    // apply cascading state
                    IEnumerator<FooBoxTreeNode> e = FooBoxTreeNodeCollection.GetNodesRecursive(_nodes, true);
                    while (e.MoveNext())
                    {
                        if (e.Current.Nodes.Count > 0) e.Current.CheckState = e.Current.GetAggregateCheckState();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the index of the image (in the ImageList on the FooBoxTreeNode control) to use for this node.
        /// </summary>
        [DefaultValue(-1), Description("The index of the image (in the ImageList on the FooBoxTreeNode control) to use for this node."), Category("Appearance")]
        public int ImageIndex
        {
            get { return _imageIndex; }
            set { _imageIndex = value; }
        }
        /// <summary>
        /// Gets or sets the name of the image to use for this node.
        /// </summary>
        [DefaultValue(""), Description("The name of the image to use for this node."), Category("Appearance")]
        public string ImageKey
        {
            get { return _imageKey; }
            set { _imageKey = value; }
        }
        /// <summary>
        /// Gets or sets whether the node is expanded (i.e. its child nodes are visible). Changes are not reflected in the dropdown portion of the 
        /// control until the next time it is opened.
        /// </summary>
        [Browsable(false)]
        public bool Expanded
        {
            get { return _expanded; }
            set { _expanded = value; }
        }
        /// <summary>
        /// Gets or sets the index of the image to use for this node when expanded.
        /// </summary>
        [DefaultValue(-1), Description("The index of the image to use for this node when expanded."), Category("Appearance")]
        public int ExpandedImageIndex
        {
            get { return _expandedImageIndex; }
            set { _expandedImageIndex = value; }
        }
        /// <summary>
        /// Gets or sets the name of the image to use for this node when expanded.
        /// </summary>
        [DefaultValue(""), Description("The name of the image to use for this node when expanded."), Category("Appearance")]
        public string ExpandedImageKey
        {
            get { return _expandedImageKey; }
            set { _expandedImageKey = value; }
        }

        /// <summary>
        /// Determines the zero-based depth of the node, relative to the FooBoxTreeNode control.
        /// </summary>
        [Browsable(false)]
        public int Depth
        {
            get
            {
                int depth = 0;
                FooBoxTreeNode node = this;
                while ((node = node._parent) != null) depth++;
                return depth;
            }
        }

        public bool IsInitiallySelected
        {
            get { return _isInitiallySelected; }
            set { _isInitiallySelected = value; }
        }

        /// <summary>
        /// Gets a collection of the child nodes for this node.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Description("The collection of the child nodes for this node."), Category("Data")]
        public FooBoxTreeNodeCollection Nodes
        {
            get { return _nodes; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initialises a new instance of FooBoxTreeNode using default (empty) values.
        /// </summary>
        public FooBoxTreeNode()
        {
            _nodes = new FooBoxTreeNodeCollection(this);
            _name = _text = _description = String.Empty;
            _background = "#00000000";

            _expandedImageIndex = _imageIndex = -1;
            _expandedImageKey = _imageKey = String.Empty;
            _expanded = false;
        }

        /// <summary>
        /// Initialises a new instance of BoxTreeNode with the specified text.
        /// </summary>
        /// <param name="text"></param>
        public FooBoxTreeNode(string text) : this()
        {
            this._text = text;
        }

        /// <summary>
        /// Initialises a new instance of BoxTreeNode with the specified name and text.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="text"></param>
        public FooBoxTreeNode(string name, string text) : this()
        {
            this._text = text;
            this._name = name;
        }

        #endregion

        #region Internal\Private Methods and Events

        /// <summary>
        /// Fired when the value of the <see cref="CheckState"/> property changes.
        /// </summary>
        [Browsable(false)]
        internal event EventHandler CheckStateChanged;

        /// <summary>
        /// Returns the aggregate check state of this node's children.
        /// </summary>
        /// <returns></returns>
        internal MethodCheckState GetAggregateCheckState()
        {
            MethodCheckState state = MethodCheckState.Unchecked;
            bool all = true;
            bool any = false;
            bool chk = false;

            foreach (FooBoxTreeNode child in Nodes)
            {
                if (child.CheckState != MethodCheckState.Unchecked) any = true;
                if (child.CheckState != MethodCheckState.Checked) all = false;
                if (child.CheckState == MethodCheckState.Checked) chk = true;
            }

            if (all & chk)
                state = MethodCheckState.Checked;
            else if (any)
                state = MethodCheckState.Indeterminate;

            return state;
        }

        /// <summary>
        /// Raises the <see cref="CheckStateChanged"/> event.
        /// </summary>
        protected virtual void OnCheckStateChanged()
        {
            if (CheckStateChanged != null) CheckStateChanged(this, EventArgs.Empty);
        }

        private void SetIsChecked(bool? value, bool updateChildren, bool updateParent)
        {
            if (value == CheckStateToBoolean)
                return;

            CheckStateToBoolean = value;

            if (updateChildren && CheckStateToBoolean.HasValue)
            {
                // apply cascading state
                IEnumerator<FooBoxTreeNode> e = FooBoxTreeNodeCollection.GetNodesRecursive(_nodes, true);
                while (e.MoveNext())
                {
                    e.Current.SetIsChecked(CheckStateToBoolean, true, false);
                }
            }

            if (updateParent && _parent != null)
                _parent.VerifyCheckState();

            this.OnPropertyChanged("IsChecked");
        }

        private void VerifyCheckState()
        {
            bool? state = null;
            for (int i = 0; i < this.Nodes.Count; ++i)
            {
                bool? current = this.Nodes[i].IsChecked;
                if (i == 0)
                {
                    state = current;
                }
                else if (state != current)
                {
                    state = null;
                    break;
                }
            }
            this.SetIsChecked(state, false, true);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns the full path to this node, using the specified path separator.
        /// </summary>
        /// <param name="pathSeparator">
        /// Separator between the elements that make up the path.
        /// </param>
        /// <param name="useNodeNamesForPath">
        /// Whether to construct the path from the <see cref="Name"/> property 
        /// instead of the <see cref="Text"/> property.
        /// </param>
        /// <returns>The path string.</returns>
        public string GetFullPath(string pathSeparator, bool useNodeNamesForPath)
        {
            StringBuilder s = new StringBuilder();
            FooBoxTreeNode node = this;

            s.Append(useNodeNamesForPath ? node.Name : node.Text);

            while ((node = node.Parent) != null)
            {
                s.Insert(0, pathSeparator);
                s.Insert(0, useNodeNamesForPath ? node.Name : node.Text);
            }

            return s.ToString();
        }

        /// <summary>
        /// Returns a string representation of this <see cref="FooBoxTreeNode"/>.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (String.IsNullOrEmpty(_name))
                return String.Format("\"{0}\"", _text);
            else
                return String.Format("{0} \"{1}\"", _name, _text);
        }

        public void Initialize()
        {
            foreach (FooBoxTreeNode node in this.Nodes)
            {
                node._parent = this;
                node.Initialize();
            }
        }

        #endregion

        #region IComparable<BoxTreeNode> Members

        /// <summary>
        /// Compares two BoxTreeNode objects using a culture-invariant, case-insensitive comparison of the Text property.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(FooBoxTreeNode other)
        {
            return StringComparer.InvariantCultureIgnoreCase.Compare(this._text, other._text);
        }

        #endregion

        #region INotifyPropertyChanged Members

        void OnPropertyChanged(string prop)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

    /// <summary>
    /// Event arguments for the <see cref="FooBoxTreeNode.AfterCheck"/> event.
    /// </summary>
    [Serializable]
    public class FooBoxTreeNodeEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the affected node.
        /// </summary>
        public FooBoxTreeNode Node
        {
            get;
            private set;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="FooBoxTreeNodeEventArgs"/> class using the specified node.
        /// </summary>
        /// <param name="node"></param>
        public FooBoxTreeNodeEventArgs(FooBoxTreeNode node)
        {
            Node = node;
        }
    }
}
