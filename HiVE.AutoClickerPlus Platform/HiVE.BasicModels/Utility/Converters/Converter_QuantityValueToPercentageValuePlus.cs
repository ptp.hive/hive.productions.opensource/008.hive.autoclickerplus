﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_QuantityValueToPercentageValuePlus : IMultiValueConverter
    {
        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            decimal returnValue = 0;

            try
            {
                /// Always test MultiValueConverter inputs for non-null
                /// (to avoid crash bugs for views in the designer)
                if (values[0] is decimal && values[1] is string)
                {
                    decimal QuantityValue = (decimal)values[0];
                    decimal PercentageValue = decimal.Parse((string)values[1]);

                    returnValue = QuantityValue * PercentageValue / 100;
                }
                /// Always test MultiValueConverter inputs for non-null
                /// (to avoid crash bugs for views in the designer)
                else if (values[0] is int && values[1] is string)
                {
                    decimal QuantityValue = (int)values[0];
                    decimal PercentageValue = decimal.Parse((string)values[1]);

                    returnValue = QuantityValue * PercentageValue / 100;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue.ToString("N2");
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
