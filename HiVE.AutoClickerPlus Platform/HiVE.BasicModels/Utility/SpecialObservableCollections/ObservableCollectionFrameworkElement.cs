﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Media.Animation;

namespace HiVE.BasicModels.SpecialObservableCollections
{
    public class ObservableCollectionFrameworkElement : ObservableCollection<FrameworkElement>
    {
        private Storyboard unloadedStoryboard;

        public Storyboard UnloadedSotryBoard
        {
            get { return unloadedStoryboard; }
            set
            {
                unloadedStoryboard = value;
                unloadedStoryboard.Completed += UnloadedStoryboardOnCompleted;
            }
        }

        public Storyboard LoadedSotryBoard { get; set; }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (FrameworkElement item in e.NewItems)
                    item.BeginStoryboard(LoadedSotryBoard);
            }
            base.OnCollectionChanged(e);
        }

        private HashSet<int> indexesToRemove = new HashSet<int>();

        protected override void RemoveItem(int index)
        {
            indexesToRemove.Add(index);
            var item = Items[index];
            UnloadedSotryBoard.Begin(item);
        }

        private void UnloadedStoryboardOnCompleted(object sender, EventArgs eventArgs)
        {
            foreach (var i in new HashSet<int>(indexesToRemove))
            {
                base.RemoveItem(i);
                indexesToRemove.Remove(i);
            }
        }
    }
}
