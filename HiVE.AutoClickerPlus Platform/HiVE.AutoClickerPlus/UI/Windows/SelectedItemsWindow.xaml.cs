﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;

namespace HiVE.AutoClickerPlus.Windows
{
    /// <summary>
    /// Interaction logic for SelectedItemsWindow.xaml
    /// </summary>
    public partial class SelectedItemsWindow : Window
    {
        public SelectedItemsWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Load the main window of this application for SelectedItems
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectedItemsWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
