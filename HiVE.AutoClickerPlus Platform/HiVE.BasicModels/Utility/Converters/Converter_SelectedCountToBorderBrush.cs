﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;
using System.Windows.Media;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_SelectedCountToBorderBrush : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            //Brush returnValue = (SolidColorBrush)(new BrushConverter().ConvertFromString("#00000000"));
            Brush returnValue = (SolidColorBrush)(new BrushConverter().ConvertFromString("#FFFFFFFF"));

            try
            {
                if (value != null)
                {
                    if ((int)value > 0)
                    {
                        //returnValue = (SolidColorBrush)(new BrushConverter().ConvertFromString("#FFD2D7D3"));
                        returnValue = (SolidColorBrush)(new BrushConverter().ConvertFromString("#FFDA7074"));
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from BorderBrush back to SelectedCount");
        }
    }
}
