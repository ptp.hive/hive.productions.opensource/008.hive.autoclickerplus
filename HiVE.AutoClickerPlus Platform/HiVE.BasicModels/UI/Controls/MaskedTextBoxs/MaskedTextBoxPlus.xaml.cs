﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace HiVE.BasicModels.MaskedTextBoxs
{
    /// <summary>
    /// Interaction logic for MaskedTextBoxPlus.xaml
    /// </summary>
    public partial class MaskedTextBoxPlus : UserControl
    {
        public MaskedTextBoxPlus()
        {
            InitializeComponent();
        }

        #region Feilds

        public Xceed.Wpf.Toolkit.MaskedTextBox MaskedTextBoxObject = null;

        #endregion

        #region Properties

        /// <summary>
        /// Gets / Sets the value that the control is showing
        /// </summary>
        public string Value
        {
            get
            {
                return (string)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        /// <summary>
        /// Gets / Sets the Watermark that the control is showing
        /// </summary>
        public string Watermark
        {
            get
            {
                return (string)GetValue(WatermarkProperty);
            }
            set
            {
                SetValue(WatermarkProperty, value);
            }
        }

        /// <summary>
        /// Gets / Sets the mask that the maskedTextBoxValue is showing
        /// </summary>
        public string Mask
        {
            get
            {
                return (string)GetValue(MaskProperty);
            }
            set
            {
                SetValue(MaskProperty, value);
            }
        }

        /// <summary>
        /// Gets / Sets the Watermark that the control is showing
        /// </summary>
        public int AssignedEditPositionCount
        {
            get
            {
                return (int)GetValue(AssignedEditPositionCountProperty);
            }
            set
            {
                SetValue(AssignedEditPositionCountProperty, value);
            }
        }

        #endregion

        #region Dependency Properties

        /// <summary>
        /// Dependency Object for the value of the MaskedTextBoxPlus Control
        /// </summary>
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
                "Value",
                typeof(string),
                typeof(MaskedTextBoxPlus),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency Object for the value of the MaskedTextBoxPlus Control
        /// </summary>
        public static readonly DependencyProperty WatermarkProperty =
            DependencyProperty.Register(
                "Watermark",
                typeof(string),
                typeof(MaskedTextBoxPlus),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency Object for the value of the MaskedTextBoxPlus Control
        /// </summary>
        public static readonly DependencyProperty MaskProperty =
            DependencyProperty.Register(
                "Mask",
                typeof(string),
                typeof(MaskedTextBoxPlus),
                new PropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency Object for the value of the MaskedTextBoxPlus Control
        /// </summary>
        public static readonly DependencyProperty AssignedEditPositionCountProperty =
            DependencyProperty.Register(
                "AssignedEditPositionCount",
                typeof(int),
                typeof(MaskedTextBoxPlus),
                new FrameworkPropertyMetadata(0));

        #endregion

        #region Methods

        /// <summary>
        /// Get MaskedTextBox Text Value and remove mask chars
        /// </summary>
        /// <param name="maskedTextBoxObject"></param>
        /// <param name="maskType"></param>
        /// <returns></returns>
        public static string GetMaskedTextBoxTextValue(
            Xceed.Wpf.Toolkit.MaskedTextBox maskedTextBoxObject,
            MaskType maskType)
        {
            string maskedTextBoxValue = string.Empty;

            try
            {
                if (maskedTextBoxObject == null) return string.Empty;

                maskedTextBoxValue = maskedTextBoxObject.Text.Trim().Replace(" ", string.Empty);

                List<char> PromptCharSplited = new List<char>();

                PromptCharSplited.Add(maskedTextBoxObject.MaskedTextProvider.PromptChar);

                foreach (var promptChar in maskType.ToSpecialDescriptionString())
                {
                    PromptCharSplited.Add(promptChar);
                }

                foreach (var promptChar in PromptCharSplited)
                {
                    maskedTextBoxValue = maskedTextBoxValue.Replace(promptChar.ToString(), string.Empty);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return maskedTextBoxValue;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the Loaded event of the MaskedTextBoxPlusUC control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void maskedTextBoxPlusUC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                MaskedTextBoxObject = maskedTextBoxValue;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Check IsMatch Regex type of new value and reset to last value if needs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void maskedTextBoxValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                this.AssignedEditPositionCount = maskedTextBoxValue.MaskedTextProvider.AssignedEditPositionCount;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// To Scroll to start location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void maskedTextBoxValue_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (maskedTextBoxValue.MaskedTextProvider.AssignedEditPositionCount == 0)
                {
                    maskedTextBoxValue.Focus();
                    maskedTextBoxValue.Select(1, 0);
                    e.Handled = true;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion
    }
}
