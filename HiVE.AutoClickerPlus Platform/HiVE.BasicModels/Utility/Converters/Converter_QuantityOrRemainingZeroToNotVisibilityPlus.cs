﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_QuantityOrRemainingZeroToNotVisibilityPlus : IMultiValueConverter
    {
        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Visible;

            try
            {
                /// Always test MultiValueConverter inputs for non-null
                /// (to avoid crash bugs for views in the designer)
                if (values[0] is int && values[1] is decimal)
                {
                    int QuantityValue = (int)values[0];
                    decimal RemainingValue = (decimal)values[1];

                    if (QuantityValue == 0
                        || RemainingValue == 0)
                    {
                        returnValue = Visibility.Collapsed;
                    }
                }
                else if (values[0] is decimal && values[1] is decimal)
                {
                    decimal QuantityValue = (decimal)values[0];
                    decimal RemainingValue = (decimal)values[1];

                    if (QuantityValue == 0
                        || RemainingValue == 0)
                    {
                        returnValue = Visibility.Collapsed;
                    }
                }
                else
                {
                    returnValue = Visibility.Collapsed;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
