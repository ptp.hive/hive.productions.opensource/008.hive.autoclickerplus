﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_DecimalToDecimation : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                decimal returnValue = 0;

                try
                {
                    if (value != null)
                    {
                        returnValue = (decimal)value;
                    }
                }
                catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

                return returnValue.ToString("F");
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return 0.00;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                return System.Convert.ToDouble(value);
            }
            catch (Exception)
            {
                return value;
            }
        }
    }
}
