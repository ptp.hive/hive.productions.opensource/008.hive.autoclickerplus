﻿using Clicker;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Windows.Interop;
using HiVE.BasicModels.Utility.Helpers;

namespace HiVE.AutoClickerPlus
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region DllImport - Clicker

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        [DllImport("user32")]
        public static extern int SetCursorPos(int x, int y);

        #endregion

        #region Fields - Clicker
        private const int MOUSEEVENTF_MOVE = 0x0001; /* mouse move */
        private const int MOUSEEVENTF_LEFTDOWN = 0x0002; /* left button down */
        private const int MOUSEEVENTF_LEFTUP = 0x0004; /* left button up */
        private const int MOUSEEVENTF_RIGHTDOWN = 0x0008; /* right button down */
        private const int MOUSEEVENTF_RIGHTUP = 0x0010; /* right button up */
        private const int MOUSEEVENTF_MIDDLEDOWN = 0x0020; /* middle button down */
        private const int MOUSEEVENTF_MIDDLEUP = 0x0040; /* middle button up */
        private const int MOUSEEVENTF_XDOWN = 0x0080; /* x button down */
        private const int MOUSEEVENTF_XUP = 0x0100; /* x button down */
        private const int MOUSEEVENTF_WHEEL = 0x0800; /* wheel button rolled */
        private const int MOUSEEVENTF_VIRTUALDESK = 0x4000; /* map to entire virtual desktop */
        private const int MOUSEEVENTF_ABSOLUTE = 0x8000; /* absolute move */

        private SynchronizationContext context = null;
        private DateTime start, end;
        private bool first = true;
        private List<ActionEntry> actions;
        private Thread runActionThread;
        private bool byTextEntry;
        private Hashtable schedualeList;

        private List<ActionEntry> lvActions = new List<ActionEntry>();
        #endregion

        #region Private Methods - Clicker

        private void RunAction()
        {
            foreach (ActionEntry action in actions)
            {
                if (action.Type.Equals(ClickType.SendKeys))
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(WorkSendKeys), action);
                }
                else// if (entry is ClickEntry)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(WorkClick), action);
                }

                int tmpIntervl = action.Interval.Equals(0) ? 0 : action.Interval * 1000 - 100;
                Thread.Sleep(tmpIntervl);
            }
            ThreadPool.QueueUserWorkItem(new WaitCallback(WorkEnableButtons), null);
        }
        private void WorkSendKeys(object state)
        {
            this.context.Send(new SendOrPostCallback(delegate (object _state)
            {
                ActionEntry action = state as ActionEntry;
                SendKeys.Send(action.Text);
            }), state);
        }
        private void WorkClick(object state)
        {
            this.context.Send(new SendOrPostCallback(delegate (object _state)
            {
                ActionEntry action = state as ActionEntry;
                SetCursorPos(action.X, action.Y);
                Thread.Sleep(100);
                if (action.Type.Equals(ClickType.click))
                {
                    mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                    mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                }
                else if (action.Type.Equals(ClickType.doubleClick))
                {
                    mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                    mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                    Thread.Sleep(100);
                    mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                    mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                }
                else //if (action.Type.Equals(ClickType.rightClick))
                {
                    mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
                    mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
                }
            }), state);
        }
        private void WorkEnableButtons(object state)
        {
            this.context.Send(new SendOrPostCallback(delegate (object _state)
            {
                enableButtons(true);
            }), state);
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            lvActions.Clear();
            first = true;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            enableButtons(false);

            if (runActionThread == null || !runActionThread.IsAlive)
            {
                //actions.Clear();
                //foreach (ListViewItem lvi in lvActions.Items)
                //{
                //    actions.Add(lvi.Tag as ActionEntry);
                //}
                runActionThread = new Thread(RunAction);
                runActionThread.Start();
            }

        }
        private void enableButtons(bool enabel)
        {
            //btnClear.Enabled = enabel;
            //btnOpen.Enabled = enabel;
            //btnSave.Enabled = enabel;
            //lvActions.Enabled = enabel;
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (runActionThread != null && runActionThread.IsAlive)
                {
                    runActionThread.Abort();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (runActionThread != null && runActionThread.IsAlive)
            {
                runActionThread.Abort();
                enableButtons(true);
            }
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            bool runIt = false;
            if (System.Windows.MessageBox.Show("After openning configuration, are you want to run it?", "Clicker", MessageBoxButton.YesNo, MessageBoxImage.Question)
                 == MessageBoxResult.Yes)
            {
                runIt = true;
            }
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "XML File |*.xml";
            file.Multiselect = false;
            if (file.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                OpenFileXml(runIt, file.FileName);
                string name = file.SafeFileName;
                this.Title = "Clicer - " + name.Substring(0, name.Length - 4);
            }
        }

        private void OpenFileXml(bool runIt, string file)
        {
            //Get data from XML file
            XmlSerializer ser = new XmlSerializer(typeof(ActionsEntry));
            using (FileStream fs = System.IO.File.Open(file, FileMode.Open))
            {
                try
                {
                    ActionsEntry entry = (ActionsEntry)ser.Deserialize(fs);
                    lvActions.Clear();
                    foreach (ActionsEntryAction ae in entry.Action)
                    {
                        string point = ae.X.ToString() + "," + ae.Y.ToString();
                        string interval = (ae.interval).ToString();
                        ActionEntry acion = new ActionEntry(ae.X, ae.Y, ae.Text, ae.interval, (ClickType)(ae.Type));
                        lvActions.Add(acion);
                    }

                    if (runIt)
                    {
                        //btnStart.PerformClick();
                    }

                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message, "Clicer", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        #endregion

        #region DllImport - AppsIn

        [DllImport("user32.dll")]
        public static extern IntPtr SetParent(IntPtr hwc, IntPtr hwp);

        #endregion

        #region Private Methods - AppsIn

        private void btnOpenApp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process processAppsIn =
                    Process.Start("Calc");
                Thread.Sleep(500);
                processAppsIn.WaitForInputIdle();

                var xp = processAppsIn.MainWindowHandle;

                var interop = new WindowInteropHelper(this);
                interop.EnsureHandle();
                // this is it
                //interop.Owner = this;
                //SetParent(processAppsIn.MainWindowHandle, interop);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
