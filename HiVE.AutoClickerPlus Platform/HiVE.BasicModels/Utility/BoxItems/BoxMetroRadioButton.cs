﻿using static HiVE.BasicModels.BoxItems.BoxBasicMethods;

namespace HiVE.BasicModels.BoxItems
{
    public class BoxMetroToggleButton
    {
        public string Content { get; set; }
        public bool IsChecked { get; set; }
        public string Background { get; set; }
        public MethodGroupName GroupName { get; set; }

        public BoxMetroToggleButton() { }

        public BoxMetroToggleButton(
            string content,
            bool isChecked,
            string background,
            MethodGroupName groupName)
        {
            this.Content = content;
            this.IsChecked = isChecked;
            this.Background = background;
            this.GroupName = groupName;
        }
    }
}
