﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Interop;
using System.Threading;
using HiVE.BasicModels.Utility.Helpers;

namespace HiVE.BasicModels.UI.Controls.WpfAppControl
{
    /// <summary>
    /// AppControl.xaml 
    /// </summary>
    public partial class AppControl : UserControl, IDisposable
    {
        [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct HWND__
        {
            /// int
            public int unused;
        }

        public AppControl()
        {
            try
            {
                InitializeComponent();
                this.SizeChanged += new SizeChangedEventHandler(OnSizeChanged);
                this.Loaded += new RoutedEventHandler(OnVisibleChanged);
                this.SizeChanged += new SizeChangedEventHandler(OnResize);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        ~AppControl()
        {
            this.Dispose();
        }

        #region Feilds

        private const int SWP_NOOWNERZORDER = 0x200;
        private const int SWP_NOREDRAW = 0x8;
        private const int SWP_NOZORDER = 0x4;
        private const int SWP_SHOWWINDOW = 0x0040;
        private const int WS_EX_MDICHILD = 0x40;
        private const int SWP_FRAMECHANGED = 0x20;
        private const int SWP_NOACTIVATE = 0x10;
        private const int SWP_ASYNCWINDOWPOS = 0x4000;
        private const int SWP_NOMOVE = 0x2;
        private const int SWP_NOSIZE = 0x1;
        private const int GWL_STYLE = (-16);
        private const int WS_VISIBLE = 0x10000000;
        private const int WS_CHILD = 0x40000000;

        #endregion

        #region Properties

        /// <summary>
        /// Track if the application has been created
        /// </summary>
        private bool _iscreated = false;

        /// <summary>
        /// Track if the control is disposed
        /// </summary>
        private bool _isdisposed = false;

        /// <summary>
        /// Handle to the application Window
        /// </summary>
        IntPtr _appWin;

        private Process _childp;

        /// <summary>
        /// The name of the exe to launch
        /// Default is 'Notepad.exe'
        /// </summary>
        private string exeName = "Notepad.exe";

        /// <summary>
        /// The name of the exe to launch
        /// Default is 'Notepad.exe'
        /// </summary>
        public string ExeName
        {
            get
            {
                return exeName;
            }
            set
            {
                exeName = value;
            }
        }

        /// <summary>
        /// Time in milliseconds to fire and launch exe.
        /// Before WaitForInputIdle
        /// Wait for process to be created and enter idle condition
        /// Need to launch the exe and set to parrent
        /// A sleep of current thread
        /// Default is TimeSpan.FromMilliseconds(1 * 1000);
        /// </summary>
        private int beforeTimeoutThreshold = 1 * 1000;

        /// <summary>
        /// Time in milliseconds to fire and launch exe.
        /// Before WaitForInputIdle
        /// Wait for process to be created and enter idle condition
        /// Need to launch the exe and set to parrent
        /// A sleep of current thread
        /// Default is TimeSpan.FromMilliseconds(1 * 1000);
        /// </summary>
        public int BeforeTimeoutThreshold
        {
            get
            {
                return beforeTimeoutThreshold;
            }
            set
            {
                beforeTimeoutThreshold = value;
            }
        }

        /// <summary>
        /// Time in milliseconds to fire and launch exe.
        /// After WaitForInputIdle
        /// Wait for process to be created and enter idle condition
        /// Need to launch the exe and set to parrent
        /// A sleep of current thread
        /// Default is TimeSpan.FromMilliseconds(0 * 1000);
        /// </summary>
        private int afterTimeoutThreshold = 0 * 1000;

        /// <summary>
        /// Time in milliseconds to fire and launch exe.
        /// After WaitForInputIdle
        /// Wait for process to be created and enter idle condition
        /// Need to launch the exe and set to parrent
        /// A sleep of current thread
        /// Default is TimeSpan.FromMilliseconds(0 * 1000);
        /// </summary>
        public int AfterTimeoutThreshold
        {
            get
            {
                return afterTimeoutThreshold;
            }
            set
            {
                afterTimeoutThreshold = value;
            }
        }

        #endregion

        #region DllImports

        [DllImport("user32.dll", EntryPoint = "GetWindowThreadProcessId", SetLastError = true,
             CharSet = CharSet.Unicode, ExactSpelling = true,
             CallingConvention = CallingConvention.StdCall)]
        private static extern long GetWindowThreadProcessId(long hWnd, long lpdwProcessId);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", EntryPoint = "GetWindowLongA", SetLastError = true)]
        private static extern long GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "SetWindowLongA", SetLastError = true)]
        public static extern int SetWindowLongA([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetWindowPos(IntPtr hwnd, long hWndInsertAfter, long x, long y, long cx, long cy, long wFlags);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);

        #endregion

        #region Methods

        /// <summary>
        /// Force redraw of control when size changes
        /// </summary>
        /// <param name="e">Not used</param>
        protected void OnSizeChanged(object s, SizeChangedEventArgs e)
        {
            this.InvalidateVisual();
        }

        /// <summary>
        /// Create control when visibility changes
        /// </summary>
        /// <param name="e">Not used</param>
        protected void OnVisibleChanged(object s, RoutedEventArgs e)
        {
            try
            {
                // If control needs to be initialized/created
                if (_iscreated == false)
                {
                    // Mark that control is created
                    _iscreated = true;

                    // Initialize handle value to invalid
                    _appWin = IntPtr.Zero;

                    try
                    {
                        var procInfo = new System.Diagnostics.ProcessStartInfo(this.exeName);
                        procInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(this.exeName);
                        // Start the process
                        _childp = System.Diagnostics.Process.Start(procInfo);

                        if (BeforeTimeoutThreshold > 0)
                        {
                            Thread.Sleep(TimeSpan.FromMilliseconds(BeforeTimeoutThreshold));
                        }

                        // Wait for process to be created and enter idle condition
                        _childp.WaitForInputIdle();

                        if (afterTimeoutThreshold > 0)
                        {
                            Thread.Sleep(TimeSpan.FromMilliseconds(afterTimeoutThreshold));
                        }

                        // Get the main handle
                        _appWin = _childp.MainWindowHandle;
                    }
                    catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

                    // Put it into this form
                    var helper = new WindowInteropHelper(Window.GetWindow(this.AppContainer));
                    SetParent(_appWin, helper.Handle);

                    // Remove border and whatnot
                    SetWindowLongA(_appWin, GWL_STYLE, WS_VISIBLE);

                    // Move the window to overlay it on this window
                    MoveWindow(_appWin, 0, 0, (int)this.ActualWidth, (int)this.ActualHeight, true);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Update display of the executable
        /// </summary>
        /// <param name="e">Not used</param>
        protected void OnResize(object s, SizeChangedEventArgs e)
        {
            try
            {
                if (this._appWin != IntPtr.Zero)
                {
                    MoveWindow(_appWin, 0, 0, (int)this.ActualWidth, (int)this.ActualHeight, true);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!_isdisposed)
                {
                    if (disposing)
                    {
                        if (_iscreated && _appWin != IntPtr.Zero && !_childp.HasExited)
                        {
                            // Stop the application
                            _childp.Kill();

                            // Clear internal handle
                            _appWin = IntPtr.Zero;
                        }
                    }
                    _isdisposed = true;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        public void Dispose()
        {
            try
            {
                this.Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion
    }
}
