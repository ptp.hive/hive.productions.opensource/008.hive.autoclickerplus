﻿using HiVE.BasicModels.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace HiVE.BasicModels.PageTransitions
{
    /// <summary>
    /// Interaction logic for PageTransition.xaml
    /// </summary>
    public partial class PageTransition : UserControl
    {
        public PageTransition()
        {
            InitializeComponent();
        }

        #region Feilds

        private Stack<UserControl> pages = new Stack<UserControl>();
        private UserControl CurrentPage { get; set; }
        private TransitionType _currentTransitionType;

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty TransitionTypeProperty =
            DependencyProperty.Register(
                "TransitionType",
                typeof(TransitionType),
                typeof(PageTransition),
                new PropertyMetadata(TransitionType.Fade));

        public TransitionType TransitionType
        {
            get
            {
                return (TransitionType)GetValue(TransitionTypeProperty);
            }
            set
            {
                SetValue(TransitionTypeProperty, value);
            }
        }

        #endregion

        #region Methods

        public void ShowPage(UserControl newPage)
        {
            pages.Push(newPage);

            Task.Factory.StartNew(() => ShowNewPage());
        }

        void ShowNewPage()
        {
            Dispatcher.Invoke((Action)delegate
                {
                    if (contentPresenter.Content != null)
                    {
                        UserControl oldPage = contentPresenter.Content as UserControl;

                        if (oldPage != null)
                        {
                            oldPage.Loaded -= newPage_Loaded;

                            UnloadPage(oldPage);
                        }
                    }
                    else
                    {
                        ShowNextPage();
                    }

                });
        }

        void ShowNextPage()
        {
            UserControl newPage = pages.Pop();

            newPage.Loaded += newPage_Loaded;

            contentPresenter.Content = newPage;
        }

        void UnloadPage(UserControl page)
        {
            _currentTransitionType = TransitionType;

            if (_currentTransitionType == TransitionType.Default)
            {
                _currentTransitionType = BasicMethods.CurrentMainSettings.BasicDefaultTransitionType;

                if (_currentTransitionType == TransitionType.Default)
                {
                    _currentTransitionType = TransitionType.Fade;
                }
            }

            Storyboard hidePage = (Application.Current.FindResource(string.Format("{0}Out", _currentTransitionType.ToString())) as Storyboard).Clone();

            hidePage.Completed += hidePage_Completed;

            hidePage.Begin(contentPresenter);
        }

        void newPage_Loaded(object sender, RoutedEventArgs e)
        {
            _currentTransitionType = TransitionType;

            if (_currentTransitionType == TransitionType.Default)
            {
                _currentTransitionType = BasicMethods.CurrentMainSettings.BasicDefaultTransitionType;

                if (_currentTransitionType == TransitionType.Default)
                {
                    _currentTransitionType = TransitionType.Fade;
                }
            }

            Storyboard showNewPage = (Application.Current.FindResource(string.Format("{0}In", _currentTransitionType.ToString())) as Storyboard).Clone();

            showNewPage.Begin(contentPresenter);

            CurrentPage = sender as UserControl;
        }

        void hidePage_Completed(object sender, EventArgs e)
        {
            contentPresenter.Content = null;

            ShowNextPage();
        }

        #endregion
    }
}
