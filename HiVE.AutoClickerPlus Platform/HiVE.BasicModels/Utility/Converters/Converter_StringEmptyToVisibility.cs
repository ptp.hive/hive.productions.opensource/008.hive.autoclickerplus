﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_StringEmptyToVisibility : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Visible;

            try
            {
                if (value != null)
                {
                    if (value.ToString().Trim() != "")
                    {
                        returnValue = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from Visibility back to StringEmpty");
        }
    }
}
