﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace HiVE.AutoClickerPlus.Pages
{
    /// <summary>
    /// Interaction logic for StartPage.xaml
    /// </summary>
    public partial class StartPage : Page
    {
        public StartPage()
        {
            InitializeComponent();

            /// Scale this page to show in another resolution of display
            HelperFunctions.ScaleTransformAnimated(this);
        }

        #region Feilds

        private NavigationService oNavigationService = null;

        #endregion

        #region Methods

        #endregion

        /// <summary>
        /// Load this page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                oNavigationService =
                    NavigationService.GetNavigationService(this);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
