﻿using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using static HiVE.BasicModels.BoxItems.BoxSpecialMethods;

namespace HiVE.BasicModels.Helpers
{
    public static class GeneralModels
    {
        #region Feilds

        private static string _errorCaption = "Error";
        private static string _errorMessage = "Error in save file!\nDo you wanna to retry?";

        #endregion

        #region Methods

        /// <summary>
        /// Get RootCategoryTitle for save in database
        /// </summary>
        /// <param name="categoryTitle"></param>
        /// <returns></returns>
        public static string GetRootCategoryTitle(string categoryTitle)
        {
            string rootcategoryTitle = string.Empty;

            try
            {
                rootcategoryTitle =
                    string.Format(
                        "{0} - {1}",
                        "ریشه اصلی",
                        categoryTitle);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return rootcategoryTitle;
        }

        /// <summary>
        /// Get RootContentCategoryId for save in database with special content type
        /// </summary>
        /// <param name="methodContentTypeItem"></param>
        /// <param name="enumRootContentCategoryItem"></param>
        /// <returns></returns>
        public static int GetRootContentCategoryId(MethodContentType methodContentTypeItem, int enumRootContentCategoryItem)
        {
            int categoryId = -1;

            try
            {
                categoryId = ((EnumExtensions.GetEnumMemberIndex(methodContentTypeItem) * BasicMethods.Db_ReservationRecords) + enumRootContentCategoryItem);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return categoryId;
        }

        /// <summary>
        /// Set ExtraDataContentItem
        /// Save a content item in a xml file
        /// </summary>
        /// <param name="contentItem"></param>
        /// <param name="contentType"></param>
        /// <param name="customContentName"></param>
        /// <returns></returns>
        public static bool SetExtraDataContentItem(object contentItem, MethodContentType contentType, string customContentName)
        {
            bool result = false;
            try
            {
                if (contentItem != null)
                {
                    string contentName = "ExtraDataContentItem";

                    bool retry = true;

                    #region Select MethodContentType

                    switch (contentType)
                    {
                        case MethodContentType.MainSettings:
                            {
                                var contentItemObject = (BoxMainSettings)contentItem;

                                contentName = MethodContentType.MainSettings.ToString();

                                break;
                            }
                    }

                    #endregion

                    if (customContentName.Trim() != "")
                    {
                        contentName = customContentName;
                    }

                    do
                    {
                        result =
                            HelperFunctions.SerializeObject(
                                contentItem,
                                string.Format(
                                    "{0}\\{1}\\{2}_{3}.{4}",
                                    BasicMethods.BaseDirectory,
                                    BasicMethods.BasicExtraDataDirectory,
                                    contentName,
                                    DateTime.Now.ToString(DateTimeFormats.ISO_DATETIME_FORMAT),
                                    "xml"));

                        retry = false;

                        if (!result)
                        {
                            retry = true;

                            if (MessageBox.Show(
                                _errorMessage,
                                _errorCaption,
                                MessageBoxButton.YesNo,
                                MessageBoxImage.Stop) == MessageBoxResult.No)
                            { retry = false; }
                        }
                    }
                    while (!result && retry);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return result;
        }

        /// <summary>
        /// Set Common MainSettings for application
        /// </summary>
        public static void CommonMainSettings()
        {
            try
            {
                System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();

                customCulture.NumberFormat.CurrencySymbol = string.Empty;
                customCulture.NumberFormat.CurrencyDecimalDigits = 2;
                customCulture.NumberFormat.CurrencyDecimalSeparator = ".";
                customCulture.NumberFormat.CurrencyGroupSeparator = ",";

                customCulture.NumberFormat.NumberDecimalDigits = 2;
                customCulture.NumberFormat.NumberDecimalSeparator = ".";
                customCulture.NumberFormat.NumberGroupSeparator = ",";

                if (BasicMethods.CurrentMainSettings != null)
                {
                    customCulture.NumberFormat.CurrencyDecimalDigits = BasicMethods.CurrentMainSettings.DecimalDigits;
                    customCulture.NumberFormat.NumberDecimalDigits = BasicMethods.CurrentMainSettings.DecimalDigits;
                }

                System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
                System.Threading.Thread.CurrentThread.CurrentUICulture = customCulture;
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Active AutoRun Status of Startup applicaion
        /// </summary>
        /// <param name="runFunctions">for permission to do functions</param>
        /// <returns></returns>
        public static bool ActiveAutoRunStartupStatus(bool runFunctions)
        {
            bool isValid = false;

            try
            {
                if (!runFunctions) { return isValid; }

                if (BasicMethods.CurrentMainSettings != null)
                {
                    AutoRunStartup.IsAutoRunStartupSetPermission =
                        BasicMethods.CurrentMainSettings.IsAutoRunStartupSetPermission;

                    AutoRunStartup.IsEnabledAutoRunStartup =
                        BasicMethods.CurrentMainSettings.IsEnabledAutoRunStartup;
                }

                AutoRunStartup.AutoRunStartupStatus(runFunctions);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return isValid;
        }

        /// <summary>
        /// IsNext Allowed Click limited by TimePassed NextAllowedClick datetime Threshold
        /// </summary>
        /// <param name="NextAllowedClick">TimePassed NextAllowedClick datetime Threshold</param>
        /// <param name="TimePassedNextAllowedClickThreshold">Time in milliseconds to fire the OnTimePassed NextAllowedClick event.</param>
        /// <returns></returns>
        public static bool IsNextAllowedClick(DateTime? NextAllowedClick, int TimePassedNextAllowedClickThreshold)
        {
            try
            {
                if (NextAllowedClick != null && DateTime.UtcNow < NextAllowedClick)
                {
                    return false;
                }
                NextAllowedClick = DateTime.UtcNow + TimeSpan.FromMilliseconds(TimePassedNextAllowedClickThreshold);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return true;
        }

        #endregion
    }
}
