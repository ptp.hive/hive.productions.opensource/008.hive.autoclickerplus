﻿using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using static HiVE.BasicModels.BoxItems.BoxSpecialMethods;

namespace HiVE.AutoClickerPlus.Utility.Helpers
{
    internal static class SpecialModels
    {
        #region Feilds

        private const string _defaultPhotoFormat = "png";
        private const string _errorCaption = "Error";
        private const string _errorMessage = "Error in save file!\nDo you wanna to retry?";

        #endregion

        #region Extra Data Generators

        /// <summary>
        /// Generate New MainSettings
        /// Use to create a default values
        /// </summary>
        /// <returns></returns>
        private static BoxMainSettings GenerateNewMainSettings()
        {
            BoxMainSettings oBoxMainSettings = new BoxMainSettings();

            return oBoxMainSettings;
        }

        /// <summary>
        /// Get Current MainSettings
        /// </summary>
        public static void GetMainSettings()
        {
            try
            {
                Currents.CurrentMainSettings =
                    HelperFunctions.DeSerializeObject<BoxMainSettings>(
                        string.Format(
                            "{0}\\{1}\\{2}",
                            BasicMethods.BaseDirectory,
                            BasicMethods.BasicExtraDataDirectory,
                            MethodContentType.MainSettings.ToDescriptionString()));

                BasicMethods.CurrentMainSettings = new BoxMainSettings();

                if ((Currents.CurrentMainSettings == null
                    && BasicMethods.CurrentMainSettings.RefreshBasicExtraDataDirectory)
                    || (BasicMethods.CurrentMainSettings.UpdateBasicExtraDataDirectory))
                {
                    Currents.CurrentMainSettings = GenerateNewMainSettings();

                    BasicMethods.CurrentMainSettings = Currents.CurrentMainSettings;

                    if (Currents.CurrentMainSettings != null)
                    {
                        bool result = false;

                        bool retry = true;
                        do
                        {
                            result =
                                HelperFunctions.SerializeObject(
                                    Currents.CurrentMainSettings,
                                    string.Format(
                                        "{0}\\{1}\\{2}",
                                        BasicMethods.BaseDirectory,
                                        BasicMethods.BasicExtraDataDirectory,
                                        MethodContentType.MainSettings.ToDescriptionString()));

                            retry = false;

                            if (!result)
                            {
                                retry = true;

                                if (MessageBox.Show(
                                    _errorMessage,
                                    _errorCaption,
                                    MessageBoxButton.YesNo,
                                    MessageBoxImage.Stop) == MessageBoxResult.No)
                                { retry = false; }
                            }
                        }
                        while (!result && retry);
                    }
                }
                else
                {
                    BasicMethods.CurrentMainSettings = Currents.CurrentMainSettings;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set New MainSettings Details
        /// </summary>
        /// <param name="NewMainSettings"></param>
        public static bool SetMainSettings(BoxMainSettings NewMainSettings)
        {
            bool result = false;

            try
            {
                if ((NewMainSettings != null
                    && NewMainSettings.RefreshBasicExtraDataDirectory)
                    || (NewMainSettings.UpdateBasicExtraDataDirectory))
                {
                    bool retry = true;

                    do
                    {
                        result =
                            HelperFunctions.SerializeObject(
                                NewMainSettings,
                                string.Format(
                                    "{0}\\{1}\\{2}",
                                    BasicMethods.BaseDirectory,
                                    BasicMethods.BasicExtraDataDirectory,
                                    MethodContentType.MainSettings.ToDescriptionString()));

                        retry = false;

                        if (!result)
                        {
                            retry = true;

                            if (MessageBox.Show(
                                _errorMessage,
                                _errorCaption,
                                MessageBoxButton.YesNo,
                                MessageBoxImage.Stop) == MessageBoxResult.No)
                            { retry = false; }
                        }
                    }
                    while (!result && retry);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return result;
        }

        #endregion

        #region Special HelperFunctions

        #endregion
    }
}
