﻿using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Xml;
using System.Xml.Serialization;

namespace HiVE.BasicModels.Helpers
{
    public static class HelperFunctions
    {
        /// <summary>
        /// TreeView IsSelected All Items
        /// </summary>
        /// <param name="TreeViewItems"></param>
        /// <param name="isSelected"></param>
        public static void TreeViewIsSelectedAllItems(IEnumerable TreeViewItems, bool isSelected)
        {
            try
            {
                if (TreeViewItems != null)
                {
                    foreach (var currentItem in TreeViewItems)
                    {
                        if (currentItem is TreeViewItem)
                        {
                            TreeViewItem item = (TreeViewItem)currentItem;
                            item.IsSelected = isSelected;
                            if (item.HasItems)
                            {
                                TreeViewIsSelectedAllItems(LogicalTreeHelper.GetChildren(item), isSelected);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Virtualized Scroll Into View
        /// </summary>
        /// <param name="control"></param>
        /// <param name="item"></param>
        public static void VirtualizedScrollIntoView(this ItemsControl control, object item)
        {
            try
            {
                // this is basically getting a reference to the ScrollViewer defined in the ItemsControl's style (identified above).
                // you *could* enumerate over the ItemsControl's children until you hit a scroll viewer, but this is quick and
                // dirty!
                // First 0 in the GetChild returns the Border from the ControlTemplate, and the second 0 gets the ScrollViewer from
                // the Border.
                ScrollViewer sv = VisualTreeHelper.GetChild(VisualTreeHelper.GetChild((DependencyObject)control, 0), 0) as ScrollViewer;
                // now get the index of the item your passing in
                int index = control.Items.IndexOf(item);
                if (index != -1)
                {
                    // since the scroll viewer is using content scrolling not pixel based scrolling we just tell it to scroll to the index of the item
                    // and viola!  we scroll there!
                    sv.ScrollToVerticalOffset(index);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("What the..." + ex.Message);
            }
        }

        /// <summary>
        /// Find a Descendant from Dependency Object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T FindDescendant<T>(DependencyObject obj) where T : DependencyObject
        {
            try
            {
                // Check if this object is the specified type
                if (obj is T)
                { return obj as T; }

                // Check for children
                int childrenCount = VisualTreeHelper.GetChildrenCount(obj);
                if (childrenCount < 1)
                { return null; }

                // First check all the children
                for (int i = 0; i < childrenCount; i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                    if (child is T)
                    { return child as T; }
                }

                // Then check the childrens children
                for (int i = 0; i < childrenCount; i++)
                {
                    DependencyObject child = FindDescendant<T>(VisualTreeHelper.GetChild(obj, i));
                    if (child != null && child is T)
                    { return child as T; }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return null;
        }

        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) { return null; }

            T foundChild = null;

            try
            {
                int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < childrenCount; i++)
                {
                    var child = VisualTreeHelper.GetChild(parent, i);
                    // If the child is not of the request child type child
                    T childType = child as T;
                    if (childType == null)
                    {
                        // recursively drill down the tree
                        foundChild = FindChild<T>(child, childName);

                        // If the child is found, break so we do not overwrite the found child. 
                        if (foundChild != null) break;
                    }
                    else if (!string.IsNullOrEmpty(childName))
                    {
                        var frameworkElement = child as FrameworkElement;
                        // If the child's name is set for search
                        if (frameworkElement != null && frameworkElement.Name == childName)
                        {
                            // if the child's name is of the request name
                            foundChild = (T)child;
                            break;
                        }
                    }
                    else
                    {
                        // child element found.
                        foundChild = (T)child;
                        break;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return foundChild;
        }

        /// <summary>
        /// Find a VisualChild from Dependency Object
        /// </summary>
        /// <typeparam name="childItem"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
            where childItem : DependencyObject
        {
            try
            {
                /// Search immediate children first (breadth-first)
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                    if (child != null && child is childItem)
                    { return (childItem)child; }
                    else
                    {
                        childItem childOfChild = FindVisualChild<childItem>(child);
                        if (childOfChild != null)
                        { return childOfChild; }
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return null;
        }

        public static void ListBox_DragOver_UpDown(object sender, DragEventArgs e, ListBox listBox)
        {
            ListBox li = sender as ListBox;
            ScrollViewer sv = HelperFunctions.FindVisualChild<ScrollViewer>(listBox);

            double tolerance = 10;
            double verticalPos = e.GetPosition(li).Y;
            double offset = 3;

            if (verticalPos < tolerance) // Top of visible list?
            {
                sv.ScrollToVerticalOffset(sv.VerticalOffset - offset); //Scroll up.
            }
            else if (verticalPos > li.ActualHeight - tolerance) //Bottom of visible list?
            {
                sv.ScrollToVerticalOffset(sv.VerticalOffset + offset); //Scroll down.    
            }
        }

        /// <summary>
        /// Create a new clone from object
        /// JavaScriptSerializer method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Source"></param>
        /// <returns></returns>
        public static T NewCloneObject<T>(T Source)
        {
            try
            {
                string resultJsonSource = new JavaScriptSerializer().Serialize(Source);

                return (T)new JavaScriptSerializer().Deserialize(resultJsonSource, typeof(T));
            }
            catch { return default(T); }
        }

        /// <summary>
        /// Clone object from source to new clone object
        /// JavaScriptSerializer method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="NewClone"></param>
        /// <param name="Source"></param>
        /// <returns></returns>
        public static bool CloneObject<T>(this T NewClone, T Source)
        {
            try
            {
                string resultJsonSource = new JavaScriptSerializer().Serialize(Source);

                NewClone = (T)new JavaScriptSerializer().Deserialize(resultJsonSource, typeof(T));

                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// Clone object from source to new clone object
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="src"></param>
        public static bool DuckCopyShallow(this Object dst, object src)
        {
            try
            {
                var srcT = src.GetType();
                var dstT = dst.GetType();
                foreach (var f in srcT.GetFields())
                {
                    var dstF = dstT.GetField(f.Name);
                    if (dstF == null)
                        continue;
                    dstF.SetValue(dst, f.GetValue(src));
                }

                foreach (var f in srcT.GetProperties())
                {
                    var dstF = dstT.GetProperty(f.Name);
                    if (dstF == null)
                        continue;

                    dstF.SetValue(dst, f.GetValue(src, null), null);
                }

                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public static bool SerializeObject<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null) { return false; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);

                    /// Exists File Path
                    CreateDirectoryFullPath(fileName);

                    xmlDocument.Save(fileName);
                    stream.Close();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
                //Log exception here
            }
        }

        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception)
            {
                //Log exception here
            }

            return objectOut;
        }

        /// <summary>
        /// Needs for test in different displays
        /// Scale FrameworkElement to show in another resolution of display
        /// </summary>
        /// <param name="frameworkElement">the page or window or user control that need to be scale</param>
        public static void ScaleTransformAnimated(FrameworkElement frameworkElement)
        {
            try
            {
                if (BasicMethods.CurrentMainSettings != null && BasicMethods.CurrentMainSettings.PermissionScaleTransform)
                {
                    /// We may have already set the LayoutTransform to a ScaleTransform.
                    /// If not, do so now.
                    var scaler = frameworkElement.LayoutTransform as ScaleTransform;

                    if (scaler == null)
                    {
                        scaler = new ScaleTransform(1.0, 1.0);
                        frameworkElement.LayoutTransform = scaler;
                    }

                    /// We'll need a DoubleAnimation object to drive 
                    /// the ScaleX and ScaleY properties.
                    DoubleAnimation animator = new DoubleAnimation()
                    { Duration = new Duration(TimeSpan.FromMilliseconds(100)), };

                    /// Toggle the scale between 1.0 and 0.375.
                    /// Scale Display from 1920*1080 to 720*405
                    if (scaler.ScaleX == 1.0)
                    { animator.To = BasicMethods.CurrentMainSettings.ScaleTransformAnimation; }
                    else { animator.To = 1.0; }

                    scaler.BeginAnimation(ScaleTransform.ScaleXProperty, animator);
                    scaler.BeginAnimation(ScaleTransform.ScaleYProperty, animator);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Needs for test in different displays
        /// Scale FrameworkElement to show in another resolution of display
        /// </summary>
        /// <param name="frameworkElement">the page or window or user control that need to be scale</param>
        /// <param name="ScaleTransformAnimation"></param>
        public static void ScaleTransformAnimated(FrameworkElement frameworkElement, double ScaleTransformAnimation)
        {
            try
            {
                if (BasicMethods.CurrentMainSettings != null && BasicMethods.CurrentMainSettings.PermissionScaleTransform)
                {
                    /// We may have already set the LayoutTransform to a ScaleTransform.
                    /// If not, do so now.
                    var scaler = frameworkElement.LayoutTransform as ScaleTransform;

                    if (scaler == null)
                    {
                        scaler = new ScaleTransform(1.0, 1.0);
                        frameworkElement.LayoutTransform = scaler;
                    }

                    /// We'll need a DoubleAnimation object to drive 
                    /// the ScaleX and ScaleY properties.
                    DoubleAnimation animator = new DoubleAnimation()
                    { Duration = new Duration(TimeSpan.FromMilliseconds(100)), };

                    /// Toggle the scale between 1.0 and 0.375.
                    /// Scale Display from 1920*1080 to 720*405
                    if (scaler.ScaleX == 1.0)
                    { animator.To = ScaleTransformAnimation; }
                    else { animator.To = 1.0; }

                    scaler.BeginAnimation(ScaleTransform.ScaleXProperty, animator);
                    scaler.BeginAnimation(ScaleTransform.ScaleYProperty, animator);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Needs for use in different displays
        /// Scale FrameworkElement to show in another resolution of display
        /// For WpfAppControl
        /// </summary>
        /// <param name="frameworkElement">the page or window or user control that need to be scale</param>
        public static void ScaleTransformAnimated_WpfAppControl(FrameworkElement frameworkElement)
        {
            try
            {
                if (BasicMethods.CurrentMainSettings != null && BasicMethods.CurrentMainSettings.PermissionScaleTransform_WpfAppControl)
                {
                    /// We may have already set the LayoutTransform to a ScaleTransform.
                    /// If not, do so now.
                    var scaler = frameworkElement.LayoutTransform as ScaleTransform;

                    if (scaler == null)
                    {
                        scaler = new ScaleTransform(1.0, 1.0);
                        frameworkElement.LayoutTransform = scaler;
                    }

                    /// We'll need a DoubleAnimation object to drive 
                    /// the ScaleX and ScaleY properties.
                    DoubleAnimation animator = new DoubleAnimation()
                    { Duration = new Duration(TimeSpan.FromMilliseconds(100)), };

                    /// Toggle the scale between 1.0 and 0.375.
                    /// Scale Display from 1920*1080 to 720*405
                    if (scaler.ScaleX == 1.0)
                    { animator.To = BasicMethods.CurrentMainSettings.ScaleTransformAnimation_WpfAppControl; }
                    else { animator.To = 1.0; }

                    scaler.BeginAnimation(ScaleTransform.ScaleXProperty, animator);
                    scaler.BeginAnimation(ScaleTransform.ScaleYProperty, animator);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Needs for use in different displays
        /// Set main startup settings
        /// For WpfAppControl
        /// </summary>
        /// <param name="frameworkElement">the page or window or user control that need to be scale</param>
        public static void ApplicationSettings_WpfAppControl(FrameworkElement frameworkElement)
        {
            try
            {
                if (BasicMethods.CurrentMainSettings != null && BasicMethods.CurrentMainSettings.PermissionScaleTransform_WpfAppControl)
                {
                    if (BasicMethods.CurrentMainSettings.PermissionScaleTransform_WpfAppControl)
                    {
                        frameworkElement.Height = BasicMethods.CurrentMainSettings.MainWindowHeight_WpfAppControl;
                        frameworkElement.Width = BasicMethods.CurrentMainSettings.MainWindowWidth_WpfAppControl;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// SerializeObject to json string
        /// JavaScriptSerializer method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <returns></returns>
        public static string Json_SerializeObject<T>(T serializableObject)
        {
            try
            {
                return (new JavaScriptSerializer().Serialize(serializableObject));
            }
            catch { return string.Empty; }
        }

        /// <summary>
        /// DeSerializeObject from json string to object
        /// JavaScriptSerializer method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonStringObject"></param>
        /// <returns></returns>
        public static T Json_DeSerializeObject<T>(string jsonStringObject)
        {
            try
            {
                return ((T)new JavaScriptSerializer().Deserialize(jsonStringObject, typeof(T)));
            }
            catch { return default(T); }
        }

        /// <summary>
        /// Create Directory Path if not Exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string CreateDirectoryPath(string path)
        {
            try
            {
                if (path.Trim() == "")
                {
                    path = BasicMethods.BasicDefaultPath;
                }

                path = path.Trim();

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception) { }

            return path;
        }

        /// <summary>
        /// Create Directory Path plus FileName if not Exists
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string CreateDirectoryFullPath(string fullPath)
        {
            try
            {
                if (fullPath.Trim() == "")
                {
                    fullPath = BasicMethods.BasicDefaultPath;
                }

                fullPath = fullPath.Trim();

                if (!Directory.Exists(Path.GetDirectoryName(fullPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
                }
            }
            catch (Exception) { }

            return fullPath;
        }

        /// <summary>
        /// Build new basic full name with special type format
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string BuildFullName(string name, string type)
        {
            return (
                string.Format(
                    "{0}-{1}_{2}_{3}.{4}",
                    BasicAssemblyAttributeAccessors.EntryAssemblyProduct,
                    BasicAssemblyAttributeAccessors.EntryAssemblyVersionMajor,
                    name,
                    DateTime.Now.ToString("yyyy-MM-dd"),
                    type));
        }

        /// <summary>
        /// Build FullPath from folder fullpath and fullname
        /// </summary>
        /// <param name="folderFullPath"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public static string BuildFullPath(string folderFullPath, string fullName)
        {
            return Path.Combine(folderFullPath, fullName);
        }

        /// <summary>
        /// Refresh InformationDetails
        /// Set ToUpper charcters if IsToUpper==True
        /// </summary>
        public static void RefreshInformationDetails(BoxInformationDetails InformationDetails)
        {
            try
            {
                if (InformationDetails != null && InformationDetails.IsToUpper)
                {
                    InformationDetails.Caption = InformationDetails.Caption.ToUpper();
                    InformationDetails.MessageInformation = InformationDetails.MessageInformation.ToUpper();
                    InformationDetails.ResultDescription = InformationDetails.ResultDescription.ToUpper();
                    InformationDetails.Result = InformationDetails.Result.ToUpper();
                    InformationDetails.ButtonFinalResult = InformationDetails.ButtonFinalResult.ToUpper();
                    InformationDetails.ButtonFailureResult = InformationDetails.ButtonFailureResult.ToUpper();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        public static BoxNotification BoxInformationDetailsToBoxNotification(BoxInformationDetails InformationDetails)
        {
            BoxNotification oBoxNotification = null;

            try
            {
                if (InformationDetails != null)
                {
                    RefreshInformationDetails(InformationDetails);

                    oBoxNotification = new BoxNotification();

                    oBoxNotification.Caption = InformationDetails.Caption;
                    oBoxNotification.MessageInformation = InformationDetails.MessageInformation;
                    oBoxNotification.ResultDescription = InformationDetails.ResultDescription;
                    oBoxNotification.Result = InformationDetails.Result;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return oBoxNotification;
        }
    }
}
