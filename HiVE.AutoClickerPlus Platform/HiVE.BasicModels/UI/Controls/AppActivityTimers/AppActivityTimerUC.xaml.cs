﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Navigation;

namespace HiVE.BasicModels.AppActivityTimers
{
    /// <summary>
    /// Interaction logic for AppActivityTimerUC.xaml
    /// </summary>
    public partial class AppActivityTimerUC : UserControl
    {
        public AppActivityTimerUC()
        {
            InitializeComponent();

            /// Scale this page to show in another resolution of display
            HelperFunctions.ScaleTransformAnimated(this);
        }

        #region Feilds

        private NavigationService oNavigationService = null;

        private Storyboard countdownStoryboard = new Storyboard();

        #endregion

        #region Dependency Properties

        #region IsStartAppActivityPage

        /// <summary>
        /// Dependency Object for the value of the AppActivityTimerUC Control
        /// </summary>
        public static readonly DependencyProperty IsStartAppActivityPageProperty =
            DependencyProperty.Register(
                "IsStartAppActivityPage",
                typeof(bool),
                typeof(AppActivityTimerUC),
                new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Gets / Sets the IsStartAppActivityPage that the control is showing
        /// </summary>
        public bool IsStartAppActivityPage
        {
            get
            {
                return (bool)GetValue(IsStartAppActivityPageProperty);
            }
            set
            {
                SetValue(IsStartAppActivityPageProperty, value);
            }
        }

        #endregion

        #region IsShowAppActivityTimer

        /// <summary>
        /// Dependency Object for the value of the AppActivityTimerUC Control
        /// </summary>
        public static readonly DependencyProperty IsShowAppActivityTimerProperty =
            DependencyProperty.Register(
                "IsShowAppActivityTimer",
                typeof(bool),
                typeof(AppActivityTimerUC),
                new PropertyMetadata(false));

        /// <summary>
        /// Gets / Sets the IsShowAppActivityTimer that the control is showing
        /// </summary>
        public bool IsShowAppActivityTimer
        {
            get
            {
                return (bool)GetValue(IsShowAppActivityTimerProperty);
            }
            set
            {
                SetValue(IsShowAppActivityTimerProperty, value);
            }
        }

        #endregion

        #region TimePassedThreshold

        /// <summary>
        /// Dependency Object for the value of the AppActivityTimerUC Control
        /// </summary>
        public static readonly DependencyProperty TimePassedThresholdProperty =
            DependencyProperty.Register(
                "TimePassedThreshold",
                typeof(int),
                typeof(AppActivityTimerUC),
                new FrameworkPropertyMetadata(30000));

        /// <summary>
        /// Gets / Sets the TimePassedThreshold that the control is showing
        /// Time in milliseconds to fire the OnTimePassed event.
        /// Plain timer, for example going off every 30 secs
        /// Default is TimeSpan.FromMilliseconds(30 * 1000);
        /// </summary>
        public int TimePassedThreshold
        {
            get
            {
                return (int)GetValue(TimePassedThresholdProperty);
            }
            set
            {
                SetValue(TimePassedThresholdProperty, value);
            }
        }

        #endregion

        #region NavigateUri

        /// <summary>
        /// Dependency Object for the value of the AppActivityTimerUC Control
        /// </summary>
        public static readonly DependencyProperty NavigateUriProperty =
            DependencyProperty.Register(
                "NavigateUri",
                typeof(Uri),
                typeof(AppActivityTimerUC),
                new FrameworkPropertyMetadata(new Uri("UI/Pages/StartPage.xaml", UriKind.Relative)));

        /// <summary>
        /// Gets / Sets the NavigateUri that the control is showing
        /// </summary>
        internal Uri NavigateUri
        {
            get
            {
                return (Uri)GetValue(NavigateUriProperty);
            }
            set
            {
                SetValue(NavigateUriProperty, value);
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Start Countdown Timer
        /// </summary>
        /// <param name="target"></param>
        public void StartCountdown(FrameworkElement target)
        {
            var countdownAnimation = new StringAnimationUsingKeyFrames();

            for (var i = (TimePassedThreshold / 1000); i >= 0; i--)
            {
                var keyTime = TimeSpan.FromSeconds((TimePassedThreshold / 1000) - i);
                var frame = new DiscreteStringKeyFrame(i.ToString(), KeyTime.FromTimeSpan(keyTime));
                countdownAnimation.KeyFrames.Add(frame);
            }
            countdownAnimation.KeyFrames.Add(new DiscreteStringKeyFrame(" ", KeyTime.FromTimeSpan(TimeSpan.FromSeconds((TimePassedThreshold / 1000) + 1))));
            Storyboard.SetTargetName(countdownAnimation, target.Name);
            Storyboard.SetTargetProperty(countdownAnimation, new PropertyPath(TextBlock.TextProperty));

            Storyboard countdownStoryboard = new Storyboard();

            countdownStoryboard.Children.Clear();
            countdownStoryboard.Children.Add(countdownAnimation);
            countdownStoryboard.Completed += CountdownTimer_Completed;
            countdownStoryboard.Begin(this);
        }

        /// <summary>
        /// Start Countdown Timer
        /// </summary>
        public void StartCountdown()
        {
            try
            {
                IsShowAppActivityTimer = true;

                var countdownAnimation = new StringAnimationUsingKeyFrames();

                for (var i = (TimePassedThreshold / 1000); i >= 0; i--)
                {
                    var keyTime = TimeSpan.FromSeconds((TimePassedThreshold / 1000) - i);
                    var frame = new DiscreteStringKeyFrame(i.ToString(), KeyTime.FromTimeSpan(keyTime));
                    countdownAnimation.KeyFrames.Add(frame);
                }
                countdownAnimation.KeyFrames.Add(new DiscreteStringKeyFrame(" ", KeyTime.FromTimeSpan(TimeSpan.FromSeconds((TimePassedThreshold / 1000) + 1))));
                Storyboard.SetTargetName(countdownAnimation, textBlockCountdownDisplay.Name);
                Storyboard.SetTargetProperty(countdownAnimation, new PropertyPath(TextBlock.TextProperty));

                countdownStoryboard = new Storyboard();

                countdownStoryboard.Children.Clear();
                countdownStoryboard.Children.Add(countdownAnimation);
                countdownStoryboard.Completed += CountdownTimer_Completed;
                countdownStoryboard.Begin(this);
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Stop Countdown Timer
        /// </summary>
        public void StopCountdown()
        {
            try
            {
                countdownStoryboard.Stop(this);
                countdownStoryboard.Remove(this);
                IsShowAppActivityTimer = false;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// CountdownTimer Completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CountdownTimer_Completed(object sender, EventArgs e)
        {
            try
            {
                if (!IsStartAppActivityPage && IsShowAppActivityTimer)
                {
                    if (oNavigationService == null)
                    {
                        oNavigationService =
                            NavigationService.GetNavigationService(this);
                    }

                    oNavigationService.Navigate(NavigateUri);
                }
            }
            catch (Exception) { }
        }

        public void SetAppActivityTimerUCDetails(int timePassedThreshold, Uri navigateUri)
        {
            try
            {
                TimePassedThreshold = timePassedThreshold;
                NavigateUri = navigateUri;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        /// <summary>
        /// Load this user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                oNavigationService = NavigationService.GetNavigationService(this);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
