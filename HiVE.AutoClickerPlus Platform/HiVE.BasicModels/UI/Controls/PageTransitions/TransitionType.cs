﻿namespace HiVE.BasicModels.PageTransitions
{
    public enum TransitionType
    {
        /// <summary>
        /// Fade
        /// </summary>
        Default,

        Fade,
        Slide,
        SlideRTL,
        SlideAndFade,
        SlideAndFadeRTL,
        Grow,
        GrowAndFade,
        Flip,
        FlipRTL,
        FlipAndFade,
        FlipAndFadeRTL,
        Spin,
        SpinRTL,
        SpinAndFade,
        SpinAndFadeRTL
    }
}
