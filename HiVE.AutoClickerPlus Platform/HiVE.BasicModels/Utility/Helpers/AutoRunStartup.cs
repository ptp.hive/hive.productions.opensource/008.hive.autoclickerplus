﻿using Microsoft.Win32;
using System;

namespace HiVE.BasicModels.Utility.Helpers
{
    public class AutoRunStartup
    {
        #region Properties

        #region FilePath

        /// <summary>
        /// For getting the location of exe file ( it can change when you change the location of exe)
        /// </summary>
        private static string filePath = string.Empty;

        /// <summary>
        /// For getting the location of exe file ( it can change when you change the location of exe)
        /// </summary>
        public static string FilePath
        {
            get
            {
                if (filePath == null || filePath.Trim() == "")
                {
                    filePath = System.Reflection.Assembly.GetEntryAssembly().Location;
                }
                return filePath;
            }
            set
            {
                filePath = value;
            }
        }

        #endregion

        #region FileName

        /// <summary>
        /// For getting the name of exe file( it can change when you change the name of exe)
        /// </summary>
        private static string fileName = string.Empty;

        /// <summary>
        /// For getting the name of exe file( it can change when you change the name of exe)
        /// </summary>
        public static string FileName
        {
            get
            {
                if (fileName == null || fileName.Trim() == "")
                {
                    fileName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                }
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        #endregion

        #region RegistryKeyApp

        /// <summary>
        /// Windows Startup Locations for HKEY_LOCAL_MACHINE
        /// @"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run";
        /// </summary>
        private const string registryName_LOCAL_MACHINE = @"Software\Microsoft\Windows\CurrentVersion\Run";

        /// <summary>
        /// Windows Startup Locations for CURRENT_USER
        /// @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run";
        /// </summary>
        private const string registryName_CURRENT_USER = @"Software\Microsoft\Windows\CurrentVersion\Run";

        /// <summary>
        /// Windows Startup Locations for LOCAL_MACHINE_Explorer
        /// @"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run";
        /// </summary>
        private const string registryName_LOCAL_MACHINE_Explorer = @"Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run";

        /// <summary>
        /// The path to the key where Windows looks for startup applications
        /// </summary>
        private static RegistryKey registryKeyApp;

        /// <summary>
        /// The path to the key where Windows looks for startup applications
        /// </summary>
        public static RegistryKey RegistryKeyApp
        {
            get
            {
                if (registryKeyApp == null)
                {
                    registryKeyApp = Registry.CurrentUser.OpenSubKey(registryName_CURRENT_USER, true);
                }
                return registryKeyApp;
            }
            set { registryKeyApp = value; }
        }

        #endregion

        #region IsEnabledAutoRunStartupStatus

        /// <summary>
        /// Get status of auto run startup of this applicaion
        /// </summary>
        private static bool isEnabledAutoRunStartupStatus = false;

        /// <summary>
        /// Get status of auto run startup of this applicaion
        /// </summary>
        public static bool IsEnabledAutoRunStartupStatus
        {
            get
            {
                try
                {
                    if (RegistryKeyApp.GetValue(FileName) == null)
                    {
                        // The value doesn't exist, the application is not set to run at startup
                        isEnabledAutoRunStartupStatus = false;
                    }
                    else
                    {
                        // The value exists, the application is set to run at startup
                        isEnabledAutoRunStartupStatus = true;
                    }
                }
                catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

                return isEnabledAutoRunStartupStatus;
            }
        }

        #endregion

        #region IsAutoRunStartupSetPermission

        /// <summary>
        /// Permission for change status of auto run startup of this applicaion
        /// Default = False
        /// </summary>
        private static bool isAutoRunStartupSetPermission = false;

        /// <summary>
        /// Permission for change status of auto run startup of this applicaion
        /// Default = False
        /// </summary>
        public static bool IsAutoRunStartupSetPermission
        {
            get
            {
                return isAutoRunStartupSetPermission;
            }
            set
            {
                isAutoRunStartupSetPermission = value;
            }
        }

        #endregion

        #region IsEnabledAutoRunStartup

        /// <summary>
        /// Set and change status of auto run startup of this applicaion if is have permission for change status
        /// Default = True
        /// </summary>
        private static bool isEnabledAutoRunStartup = true;

        /// <summary>
        /// Set and change status of auto run startup of this applicaion if is have permission for change status
        /// Default = True
        /// </summary>
        public static bool IsEnabledAutoRunStartup
        {
            get
            {
                return isEnabledAutoRunStartup;
            }
            set
            {
                isEnabledAutoRunStartup = value;
            }
        }

        #endregion

        #endregion

        #region Methods

        private static bool SetAutoRunStartupStatus()
        {
            bool isValid = false;

            try
            {
                if (IsAutoRunStartupSetPermission)
                {
                    if (IsEnabledAutoRunStartup && !IsEnabledAutoRunStartupStatus)
                    {
                        // Add the value in the registry so that the application runs at startup
                        RegistryKeyApp.SetValue(FileName, FilePath);
                    }
                    else if (!IsEnabledAutoRunStartup && IsEnabledAutoRunStartupStatus)
                    {
                        // Remove the value from the registry so that the application doesn't start
                        RegistryKeyApp.DeleteValue(FileName, false);
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return isValid;
        }

        public static void GetExeLocation()
        {
            try
            {
                /// For getting the location of exe file ( it can change when you change the location of exe)
                filePath = System.Reflection.Assembly.GetEntryAssembly().Location;
                /// For getting the name of exe file( it can change when you change the name of exe)
                fileName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Start the exe autometically when computer is stared
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filepath"></param>
        public bool StartExeWhenPcStartup(string filename, string filepath)
        {
            bool isValid = false;

            try
            {
                RegistryKeyApp.SetValue(filename, filepath);

                isValid = true;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return isValid;
        }

        /// <summary>
        /// Active AutoRun Status of Startup applicaion
        /// </summary>
        /// <param name="runFunctions">for permission to do functions</param>
        /// <returns></returns>
        public static bool AutoRunStartupStatus(bool runFunctions)
        {
            bool isValid = false;

            try
            {
                if (!runFunctions) { return isValid; }

                SetAutoRunStartupStatus();

                isValid = true;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return isValid;
        }

        #endregion
    }
}
