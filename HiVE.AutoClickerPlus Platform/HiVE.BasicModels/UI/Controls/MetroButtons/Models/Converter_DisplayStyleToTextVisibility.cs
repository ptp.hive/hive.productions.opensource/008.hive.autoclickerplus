﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.MetroButtons
{
    public class Converter_DisplayStyleToTextVisibility : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Collapsed;

            try
            {
                if (value != null && value is MetroButtonDisplayStyle)
                {
                    if ((MetroButtonDisplayStyle)value == MetroButtonDisplayStyle.Text
                        || (MetroButtonDisplayStyle)value == MetroButtonDisplayStyle.ImageAndText)
                    {
                        returnValue = Visibility.Visible;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from TextVisibility back to DisplayStyle");
        }
    }
}
