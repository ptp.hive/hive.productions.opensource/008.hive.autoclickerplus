﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_CountZeroToIsEnabled : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            bool returnValue = false;

            try
            {
                if (value != null)
                {
                    if (value is int)
                    {
                        if ((int)value > 0)
                        {
                            returnValue = true;
                        }
                    }
                    else if (value is decimal)
                    {
                        if ((decimal)value > 0)
                        {
                            returnValue = true;
                        }
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from IsEnabled back to CountZero");
        }
    }
}
