﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace HiVE.BasicModels.MetroDateTimePickers
{
    /// <summary>
    /// Interaction logic for MetroDateTimePicker.xaml
    /// </summary>
    public partial class MetroDateTimePicker : UserControl
    {
        public MetroDateTimePicker()
        {
            InitializeComponent();
        }

        #region Feilds

        private int MaxHoursValue { get; set; } = 23;
        private int MaxMinutesValue { get; set; } = 59;

        #endregion

        #region Dependency Properties

        #region DateTimePickerType

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty DateTimePickerTypeProperty =
            DependencyProperty.Register(
                "DateTimePickerType",
                typeof(DateTimePickerType),
                typeof(MetroDateTimePicker),
                new PropertyMetadata(DateTimePickerType.ShortTime, OnDateTimePickerTypeChanged));

        /// <summary>
        /// Gets / Sets the DateTimePickerType that the control is showing 
        /// </summary>
        public DateTimePickerType DateTimePickerType
        {
            get
            {
                return (DateTimePickerType)GetValue(DateTimePickerTypeProperty);
            }
            set
            {
                SetValue(DateTimePickerTypeProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the DateTimePickerType property.
        /// </summary>
        /// <param name="d">MetroDateTimePicker</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnDateTimePickerTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroDateTimePicker = (MetroDateTimePicker)d;
            var oldDateTimePickerType = (DateTimePickerType)e.OldValue;
            var newDateTimePickerType = MetroDateTimePicker.DateTimePickerType;
            MetroDateTimePicker.OnDateTimePickerTypeChanged(oldDateTimePickerType, newDateTimePickerType);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the DateTimePickerType property.
        /// </summary>
        /// <param name="oldDateTimePickerType">Old Value</param>
        /// <param name="newDateTimePickerType">New Value</param>
        protected virtual void OnDateTimePickerTypeChanged(DateTimePickerType oldDateTimePickerType, DateTimePickerType newDateTimePickerType)
        {
            SetMetroDateTimePickerProperties(oldDateTimePickerType);
        }

        #endregion

        #region DateValue

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty DateValueProperty =
            DependencyProperty.Register(
                "DateValue",
                typeof(DateTime),
                typeof(MetroDateTimePicker),
                new FrameworkPropertyMetadata(DateTime.Now));

        /// <summary>
        /// Gets / Sets the DateValue that the control is showing
        /// </summary>
        public DateTime DateValue
        {
            get
            {
                return (DateTime)GetValue(DateValueProperty);
            }
            set
            {
                SetValue(DateValueProperty, value);
            }
        }

        #endregion

        #region HoursValue

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty HoursValueProperty =
            DependencyProperty.Register(
                "HoursValue",
                typeof(int),
                typeof(MetroDateTimePicker),
                new FrameworkPropertyMetadata(0, OnHoursValueChanged));

        /// <summary>
        /// Gets / Sets the HoursValue that the control is showing
        /// </summary>
        public int HoursValue
        {
            get
            {
                return (int)GetValue(HoursValueProperty);
            }
            set
            {
                SetValue(HoursValueProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the HoursValue property.
        /// </summary>
        /// <param name="d">MetroDateTimePicker</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnHoursValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroDateTimePicker = (MetroDateTimePicker)d;
            var oldHoursValue = (int)e.OldValue;
            var newHoursValue = MetroDateTimePicker.HoursValue;
            MetroDateTimePicker.OnHoursValueChanged(oldHoursValue, newHoursValue);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the HoursValue property.
        /// </summary>
        /// <param name="oldHoursValue">Old Value</param>
        /// <param name="newHoursValue">New Value</param>
        protected virtual void OnHoursValueChanged(int oldHoursValue, int newHoursValue)
        {
            try
            {
                int _hoursValue = HoursValue;
                bool toTimeTypePM = false;

                if ((DateTimePickerType == DateTimePickerType.ShortTime
                    || DateTimePickerType == DateTimePickerType.QuickSelect)
                    && TimeType == TimeType.PM)
                {
                    _hoursValue += 12;
                }
                else if ((DateTimePickerType == DateTimePickerType.ShortTime
                    || DateTimePickerType == DateTimePickerType.QuickSelect)
                    && TimeType == TimeType.AM && HoursValue > MaxHoursValue)
                {
                    toTimeTypePM = true;
                }

                DateValue
                    = new DateTime(
                        DateValue.Year,
                        DateValue.Month,
                        DateValue.Day,
                        _hoursValue,
                        DateValue.Minute,
                        DateValue.Second);

                if (toTimeTypePM)
                {
                    //TimeType = TimeType.PM;
                }

                SetQuickHoursSelected();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region MinutesValue

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty MinutesValueProperty =
            DependencyProperty.Register(
                "MinutesValue",
                typeof(int),
                typeof(MetroDateTimePicker),
                new FrameworkPropertyMetadata(0, OnMinutesValueChanged));

        /// <summary>
        /// Gets / Sets the MinutesValue that the control is showing
        /// </summary>
        public int MinutesValue
        {
            get
            {
                return (int)GetValue(MinutesValueProperty);
            }
            set
            {
                SetValue(MinutesValueProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the MinutesValue property.
        /// </summary>
        /// <param name="d">MetroDateTimePicker</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnMinutesValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroDateTimePicker = (MetroDateTimePicker)d;
            var oldMinutesValue = (int)e.OldValue;
            var newMinutesValue = MetroDateTimePicker.MinutesValue;
            MetroDateTimePicker.OnMinutesValueChanged(oldMinutesValue, newMinutesValue);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the MinutesValue property.
        /// </summary>
        /// <param name="oldMinutesValue">Old Value</param>
        /// <param name="newMinutesValue">New Value</param>
        protected virtual void OnMinutesValueChanged(int oldMinutesValue, int newMinutesValue)
        {
            try
            {
                DateValue
                    = new DateTime(
                        DateValue.Year,
                        DateValue.Month,
                        DateValue.Day,
                        DateValue.Hour,
                        MinutesValue,
                        DateValue.Second);

                SetQuickMinutesSelected();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region SecondsValue

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty SecondsValueProperty =
            DependencyProperty.Register(
                "SecondsValue",
                typeof(int),
                typeof(MetroDateTimePicker),
                new FrameworkPropertyMetadata(0, OnSecondsValueChanged));

        /// <summary>
        /// Gets / Sets the SecondsValue that the control is showing
        /// </summary>
        public int SecondsValue
        {
            get
            {
                return (int)GetValue(SecondsValueProperty);
            }
            set
            {
                SetValue(SecondsValueProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the SecondsValue property.
        /// </summary>
        /// <param name="d">MetroDateTimePicker</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnSecondsValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroDateTimePicker = (MetroDateTimePicker)d;
            var oldSecondsValue = (int)e.OldValue;
            var newSecondsValue = MetroDateTimePicker.SecondsValue;
            MetroDateTimePicker.OnSecondsValueChanged(oldSecondsValue, newSecondsValue);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the SecondsValue property.
        /// </summary>
        /// <param name="oldSecondsValue">Old Value</param>
        /// <param name="newSecondsValue">New Value</param>
        protected virtual void OnSecondsValueChanged(int oldSecondsValue, int newSecondsValue)
        {
            try
            {
                DateValue
                    = new DateTime(
                        DateValue.Year,
                        DateValue.Month,
                        DateValue.Day,
                        DateValue.Hour,
                        DateValue.Minute,
                        SecondsValue);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region TimeType

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty TimeTypeProperty =
            DependencyProperty.Register(
                "TimeType",
                typeof(TimeType),
                typeof(MetroDateTimePicker),
                new PropertyMetadata(TimeType.AM, OnTimeTypeChanged));

        /// <summary>
        /// Gets / Sets the TimeType that the control is showing 
        /// </summary>
        public TimeType TimeType
        {
            get
            {
                return (TimeType)GetValue(TimeTypeProperty);
            }
            set
            {
                SetValue(TimeTypeProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the TimeType property.
        /// </summary>
        /// <param name="d">MetroDateTimePicker</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnTimeTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroDateTimePicker = (MetroDateTimePicker)d;
            var oldTimeType = (TimeType)e.OldValue;
            var newTimeType = MetroDateTimePicker.TimeType;
            MetroDateTimePicker.OnTimeTypeChanged(oldTimeType, newTimeType);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the TimeType property.
        /// </summary>
        /// <param name="oldTimeType">Old Value</param>
        /// <param name="newTimeType">New Value</param>
        protected virtual void OnTimeTypeChanged(TimeType oldTimeType, TimeType newTimeType)
        {
            try
            {
                int _hoursValue = HoursValue;

                if ((DateTimePickerType == DateTimePickerType.ShortTime
                    || DateTimePickerType == DateTimePickerType.QuickSelect)
                    && TimeType == TimeType.PM)
                {
                    _hoursValue += 12;
                }

                DateValue
                    = new DateTime(
                        DateValue.Year,
                        DateValue.Month,
                        DateValue.Day,
                        _hoursValue,
                        DateValue.Minute,
                        DateValue.Second);

                SetQuickTimeTypesSelected();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region QuickHoursItems

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty QuickHoursItemsProperty =
            DependencyProperty.Register(
                "QuickHoursItems",
                typeof(List<int>),
                typeof(MetroDateTimePicker),
                new FrameworkPropertyMetadata(SetQuickHoursItems()));

        /// <summary>
        /// Gets / Sets the QuickHoursItems that the control is showing
        /// </summary>
        public List<int> QuickHoursItems
        {
            get
            {
                return (List<int>)GetValue(QuickHoursItemsProperty);
            }
            set
            {
                SetValue(QuickHoursItemsProperty, value);
            }
        }

        #endregion

        #region QuickMinutesItems

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty QuickMinutesItemsProperty =
            DependencyProperty.Register(
                "QuickMinutesItems",
                typeof(List<int>),
                typeof(MetroDateTimePicker),
                new FrameworkPropertyMetadata(SetQuickMinutesItems()));

        /// <summary>
        /// Gets / Sets the QuickMinutesItems that the control is showing
        /// </summary>
        public List<int> QuickMinutesItems
        {
            get
            {
                return (List<int>)GetValue(QuickMinutesItemsProperty);
            }
            set
            {
                SetValue(QuickMinutesItemsProperty, value);
            }
        }

        #endregion

        #region QuickTimeTypesItems

        /// <summary>
        /// Dependency Object for the value of the MetroDateTimePicker Control
        /// </summary>
        public static readonly DependencyProperty QuickTimeTypesItemsProperty =
            DependencyProperty.Register(
                "QuickTimeTypesItems",
                typeof(List<Enum>),
                typeof(MetroDateTimePicker),
                new FrameworkPropertyMetadata(SetQuickTimeTypesItems()));

        /// <summary>
        /// Gets / Sets the QuickMinutesItems that the control is showing
        /// </summary>
        public List<Enum> QuickTimeTypesItems
        {
            get
            {
                return (List<Enum>)GetValue(QuickTimeTypesItemsProperty);
            }
            set
            {
                SetValue(QuickTimeTypesItemsProperty, value);
            }
        }

        #endregion

        #endregion

        #region Time Methods

        private void buttonUpHours_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RefreshHoursValue(true, null);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void buttonDownHours_Click(object sender, RoutedEventArgs e)
        {
            RefreshHoursValue(false, null);
        }

        private void buttonUpMinutes_Click(object sender, RoutedEventArgs e)
        {
            RefreshMinutesValue(true);
        }

        private void buttonDownMinutes_Click(object sender, RoutedEventArgs e)
        {
            RefreshMinutesValue(false);
        }

        private void buttonUpTimeType_Click(object sender, RoutedEventArgs e)
        {
            RefreshTimeTypeValue();
        }

        private void buttonDownTimeType_Click(object sender, RoutedEventArgs e)
        {
            RefreshTimeTypeValue();
        }

        #endregion

        #region QuickSelect Plus Methods

        #region PanelHours Methods

        private void calendarDayButtonHour_1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 1;

                foreach (CalendarDayButton calendarDayButton in stackPanelHours.Children)
                {

                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 2;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 3;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 4;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_5_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 5;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_6_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 6;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_7_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 7;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_8_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 8;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_9_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 9;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_10_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 10;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_11_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 11;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonHour_12_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HoursValue = 12;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region PanelMinutes Methods

        private void calendarDayButtonMinute_10_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MinutesValue = 10;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonMinute_15_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MinutesValue = 15;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonMinute_20_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MinutesValue = 20;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonMinute_30_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MinutesValue = 30;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonMinute_40_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MinutesValue = 40;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonMinute_45_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MinutesValue = 45;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonMinute_50_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MinutesValue = 50;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region PanelTimeTypes Methods

        private void calendarDayButtonTimeType_AM_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TimeType = TimeType.AM;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void calendarDayButtonTimeType_PM_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TimeType = TimeType.PM;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #endregion

        #region QuickSelect Methods

        private static List<int> SetQuickHoursItems()
        {
            List<int> QuickHoursItems = new List<int>();

            try
            {
                for (int hour = 1; hour <= 12; hour++)
                {
                    QuickHoursItems.Add(hour);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return QuickHoursItems;
        }

        private void SetQuickHoursSelected()
        {
            try
            {
                foreach (int quickHoursValue in listBoxQuickHours.Items)
                {
                    if (quickHoursValue == HoursValue)
                    {
                        listBoxQuickHours.SelectedItem = quickHoursValue;
                        break;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void listBoxQuickHours_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (DateTimePickerType == DateTimePickerType.QuickSelect)
                {
                    if (listBoxQuickHours.SelectedItem != null)
                    {
                        var selectedItem = (int)listBoxQuickHours.SelectedItem;

                        HoursValue = selectedItem;
                    }
                    else { HoursValue = 0; }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private static List<int> SetQuickMinutesItems()
        {
            List<int> QuickMinutesItems = new List<int>();

            try
            {
                /// Get a list of QuickMinutes.
                var QuickMinutes = EnumExtensions.GetEnumValues(new QuickMinutes());
                /// Add each one to this collection:
                foreach (QuickMinutes minute in QuickMinutes)
                {
                    QuickMinutesItems.Add((int)minute);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return QuickMinutesItems;
        }

        private void SetQuickMinutesSelected()
        {
            try
            {
                foreach (int quickMinutesValue in listBoxQuickMinutes.Items)
                {
                    if (quickMinutesValue == MinutesValue)
                    {
                        listBoxQuickMinutes.SelectedItem = quickMinutesValue;
                        break;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void listBoxQuickMinutes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (DateTimePickerType == DateTimePickerType.QuickSelect)
                {
                    if (listBoxQuickMinutes.SelectedItem != null)
                    {
                        var selectedItem = (int)listBoxQuickMinutes.SelectedItem;

                        MinutesValue = selectedItem;
                    }
                    else { MinutesValue = 0; }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private static List<Enum> SetQuickTimeTypesItems()
        {
            List<Enum> QuickTimeTypesItems = new List<Enum>();

            try
            {
                QuickTimeTypesItems = EnumExtensions.GetEnumValues(new TimeType()).ToList();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return QuickTimeTypesItems;
        }

        private void SetQuickTimeTypesSelected()
        {
            try
            {
                foreach (TimeType quickTimeType in listBoxQuickTimeTypes.Items)
                {
                    if (quickTimeType == TimeType)
                    {
                        listBoxQuickTimeTypes.SelectedItem = quickTimeType;
                        break;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void listBoxQuickTimeTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (DateTimePickerType == DateTimePickerType.QuickSelect)
                {
                    if (listBoxQuickTimeTypes.SelectedItem != null)
                    {
                        var selectedItem = (TimeType)listBoxQuickTimeTypes.SelectedItem;

                        TimeType = selectedItem;
                    }
                    else { TimeType = TimeType.AM; }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refresh DateTimePickerType and Set Properties
        /// </summary>
        /// <param name="oldDateTimePickerType"></param>
        private void SetMetroDateTimePickerProperties(DateTimePickerType? oldDateTimePickerType)
        {
            try
            {
                gridDate.Visibility = Visibility.Visible;
                gridTime.Visibility = Visibility.Visible;
                gridQuickSelectPlus.Visibility = Visibility.Visible;
                gridQuickSelect.Visibility = Visibility.Visible;
                stackPanelTimeTypes.Visibility = Visibility.Visible;

                buttonUpTimeType.Visibility = Visibility.Visible;
                textBoxTimeType.Visibility = Visibility.Visible;
                buttonDownTimeType.Visibility = Visibility.Visible;

                switch (DateTimePickerType)
                {
                    case DateTimePickerType.ShortTime:
                        {
                            MaxHoursValue = 11;
                            RefreshHoursValue(null, oldDateTimePickerType);

                            gridQuickSelectPlus.Visibility = Visibility.Collapsed;
                            gridQuickSelect.Visibility = Visibility.Collapsed;

                            break;
                        }

                    case DateTimePickerType.LongTime:
                        {
                            MaxHoursValue = 23;
                            RefreshHoursValue(null, oldDateTimePickerType);

                            buttonUpTimeType.Visibility = Visibility.Collapsed;
                            textBoxTimeType.Visibility = Visibility.Collapsed;
                            buttonDownTimeType.Visibility = Visibility.Collapsed;

                            gridQuickSelectPlus.Visibility = Visibility.Collapsed;
                            gridQuickSelect.Visibility = Visibility.Collapsed;

                            break;
                        }

                    case DateTimePickerType.QuickSelect:
                        {
                            MaxHoursValue = 11;
                            RefreshHoursValue(null, oldDateTimePickerType);

                            gridTime.Visibility = Visibility.Collapsed;

                            gridQuickSelectPlus.Visibility = Visibility.Collapsed;

                            break;
                        }

                    case DateTimePickerType.QuickSelectPlus:
                        {
                            MaxHoursValue = 11;
                            RefreshHoursValue(null, oldDateTimePickerType);

                            stackPanelTimeTypes.Visibility = Visibility.Collapsed;

                            gridQuickSelect.Visibility = Visibility.Collapsed;

                            break;
                        }

                    case DateTimePickerType.JustTime:
                        {
                            gridDate.Visibility = Visibility.Collapsed;
                            gridQuickSelectPlus.Visibility = Visibility.Collapsed;
                            gridQuickSelect.Visibility = Visibility.Collapsed;

                            break;
                        }

                    case DateTimePickerType.JustCalender:
                        {
                            gridTime.Visibility = Visibility.Collapsed;
                            gridQuickSelectPlus.Visibility = Visibility.Collapsed;
                            gridQuickSelect.Visibility = Visibility.Collapsed;

                            break;
                        }
                }

                HoursValue = DateValue.Hour;
                MinutesValue = DateValue.Minute;
                SecondsValue = DateValue.Second;

                RefreshHoursValue(null, null);
                SetQuickTimeTypesSelected();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Refresh HoursValue
        /// </summary>
        /// <param name="IsIncrease">Null for changed DateTimePickerType</param>
        private void RefreshHoursValue(bool? IsIncrease, DateTimePickerType? oldDateTimePickerType)
        {
            try
            {
                if (IsIncrease == null && oldDateTimePickerType == null)
                {
                    if ((DateTimePickerType == DateTimePickerType.ShortTime
                        || DateTimePickerType == DateTimePickerType.QuickSelect))
                    {
                        TimeType = TimeType.AM;

                        if (HoursValue > 11)
                        {
                            HoursValue = HoursValue - 12;
                            TimeType = TimeType.PM;
                        }
                    }
                }
                else if (IsIncrease == null && oldDateTimePickerType != null)
                {
                    if (oldDateTimePickerType == DateTimePickerType.LongTime
                        && (DateTimePickerType == DateTimePickerType.ShortTime
                        || DateTimePickerType == DateTimePickerType.QuickSelect))
                    {
                        TimeType = TimeType.AM;

                        if (HoursValue > 11)
                        {
                            HoursValue = HoursValue - 12;
                            TimeType = TimeType.PM;
                        }
                    }
                    else if ((oldDateTimePickerType == DateTimePickerType.ShortTime
                        || oldDateTimePickerType == DateTimePickerType.QuickSelect)
                        && DateTimePickerType == DateTimePickerType.LongTime)
                    {
                        if (TimeType == TimeType.PM)
                        {
                            HoursValue = HoursValue + 12;
                        }
                    }
                }
                else if (IsIncrease != null && (IsIncrease ?? false))
                {
                    if (HoursValue >= MaxHoursValue)
                    { HoursValue = 0; }
                    else { HoursValue++; }
                }
                else if (IsIncrease != null && !(IsIncrease ?? false))
                {
                    if (HoursValue <= 0)
                    { HoursValue = MaxHoursValue; }
                    else { HoursValue--; }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Refresh MinutesValue
        /// </summary>
        /// <param name="IsIncrease"></param>
        private void RefreshMinutesValue(bool IsIncrease)
        {
            try
            {
                if (IsIncrease)
                {
                    if (MinutesValue >= MaxMinutesValue)
                    { MinutesValue = 0; }
                    else { MinutesValue++; }
                }
                else if (!IsIncrease)
                {
                    if (MinutesValue <= 0)
                    { MinutesValue = MaxMinutesValue; }
                    else { MinutesValue--; }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Refresh TimeTypeValue
        /// </summary>
        private void RefreshTimeTypeValue()
        {
            try
            {
                if (TimeType == TimeType.AM)
                { TimeType = TimeType.PM; }
                else { TimeType = TimeType.AM; }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set DefaultValues
        /// </summary>
        private void SetDefaultValues()
        {
            try
            {
                SetMetroDateTimePickerProperties(null);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Selected date to now
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDateTimeNow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                calenderDate.SelectedDate = DateTime.Now;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        /// <summary>
        /// Load this user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroDateTimePickerUC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SetDefaultValues();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
