﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HiVE.BasicModels.MaskedTextBoxs
{
    /// <summary>
    /// Interaction logic for MaskedTextBox.xaml
    /// </summary>
    public partial class MaskedTextBox : UserControl
    {
        public MaskedTextBox()
        {
            InitializeComponent();
        }

        #region Feilds

        private Regex MaskMatchPattern = null;
        private string MaskPattern = string.Empty;

        #endregion

        #region Properties

        /// <summary>
        /// Gets / Sets the value that the control is showing
        /// </summary>
        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        /// <summary>
        /// Gets / Sets the Watermark that the control is showing
        /// </summary>
        public string Watermark
        {
            get
            {
                return (string)GetValue(WatermarkProperty);
            }
            set
            {
                SetValue(WatermarkProperty, value);
            }
        }

        public MaskType Mask
        {
            get
            {
                return (MaskType)GetValue(MaskProperty);
            }
            set
            {
                SetValue(MaskProperty, value);
            }
        }

        /// <summary>
        /// Gets / Sets the Watermark that the control is showing
        /// </summary>
        public string CustomMask
        {
            get
            {
                return (string)GetValue(CustomMaskProperty);
            }
            set
            {
                SetValue(CustomMaskProperty, value);
            }
        }

        #endregion

        #region Dependency Properties

        /// <summary>
        /// Dependency Object for the value of the MaskedTextBox Control
        /// </summary>
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                "Text",
                typeof(string),
                typeof(MaskedTextBox),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency Object for the value of the MaskedTextBox Control
        /// </summary>
        public static readonly DependencyProperty WatermarkProperty =
            DependencyProperty.Register(
                "Watermark",
                typeof(string),
                typeof(MaskedTextBox),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency Object for the value of the MaskedTextBox Control
        /// </summary>
        public static readonly DependencyProperty MaskProperty =
            DependencyProperty.Register(
                "Mask",
                typeof(MaskType),
                typeof(MaskedTextBox),
                new PropertyMetadata(MaskType.None));

        /// <summary>
        /// Dependency Object for the value of the MaskedTextBox Control
        /// </summary>
        public static readonly DependencyProperty CustomMaskProperty =
            DependencyProperty.Register(
                "CustumMask",
                typeof(string),
                typeof(MaskedTextBox),
                new FrameworkPropertyMetadata(string.Empty));

        #endregion

        #region Methods

        /// <summary>
        /// Check mask is matched
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        private bool MaskIsMatch(string stringValue)
        {
            bool isMatch = false;

            try
            {
                if (Mask == MaskType.None)
                { return true; }
                else if (Mask == MaskType.Custom)
                {
                    if (CustomMask.Trim() != "")
                    { MaskPattern = CustomMask.Trim(); }
                    else
                    {
                        return true;
                        //MaskPattern = string.Empty;
                    }
                }
                else
                { MaskPattern = Mask.ToDescriptionString(); }

                MaskMatchPattern = new Regex(MaskPattern);

                isMatch = MaskMatchPattern.IsMatch(stringValue);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return isMatch;
        }

        /// <summary>
        /// Check mask is matched
        /// </summary>
        /// <param name="stringValue"></param>
        /// <param name="Mask"></param>
        /// <param name="CustomMask"></param>
        /// <returns></returns>
        public static bool MaskIsMatch(string stringValue, MaskType Mask, string CustomMask)
        {
            bool isMatch = false;

            try
            {
                string MaskPattern = string.Empty;

                if (Mask == MaskType.None)
                { return true; }
                else if (Mask == MaskType.Custom)
                {
                    if (CustomMask.Trim() != "")
                    { MaskPattern = CustomMask.Trim(); }
                    else
                    {
                        return true;
                        //MaskPattern = string.Empty;
                    }
                }
                else
                { MaskPattern = Mask.ToDescriptionString(); }

                Regex MaskMatchPattern = null;

                MaskMatchPattern = new Regex(MaskPattern);

                isMatch = MaskMatchPattern.IsMatch(stringValue);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return isMatch;
        }

        /// <summary>
        /// Check mask for email, IsValidEmail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool IsValidEmail(string email)
        {
            try
            {
                var mail = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Some mask FormattedPhoneNumber
        /// </summary>
        public string FormattedPhoneNumber
        {
            get
            {
                if (Text == null)
                    return string.Empty;

                switch (Text.Length)
                {
                    case 7:
                        return Regex.Replace(Text, @"(\d{3})(\d{4})", "$1-$2");
                    case 10:
                        return Regex.Replace(Text, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                    case 11:
                        return Regex.Replace(Text, @"(\d{1})(\d{3})(\d{3})(\d{4})", "$1-$2-$3-$4");
                    default:
                        return Text;
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Event Definition Value Change
        /// </summary>
        public static RoutedEvent ValueChangedEvent =
            EventManager.RegisterRoutedEvent(
                "ValueChanged",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(MaskedTextBox));

        /// <summary>
        /// Event fired when value changes
        /// </summary>
        public event RoutedEventHandler ValueChanged
        {
            add { AddHandler(ValueChangedEvent, value); }
            remove { RemoveHandler(ValueChangedEvent, value); }
        }

        /// <summary>
        /// Event Helper Function when Value is changed
        /// </summary>
        protected virtual void OnValueChanged()
        {
            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = ValueChangedEvent;
            RaiseEvent(args);
        }

        /// <summary>
        /// Handles the Loaded event of the MaskedTextBoxUC control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void maskedTextBoxUC_Loaded(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Need to check IsMatch Regex type
        /// In the future
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxValue_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        /// <summary>
        /// Check IsMatch Regex type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxValue_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                var currentTextBox = (TextBox)sender;
                var textValue = currentTextBox.Text.Insert(currentTextBox.CaretIndex, e.Text);

                e.Handled = !MaskIsMatch(textValue);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Reset Text Value if IsMatch Regex type is false
        /// </summary>
        /// <param name="currentTextBox"></param>
        private void ResetText(TextBox currentTextBox)
        {
            try
            {
                //currentTextBox.Text = 0 < MinValue ? MinValue.ToString() : "0";

                currentTextBox.SelectAll();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Check IsMatch Regex type of new value and reset to last value if needs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var currentTextBox = (TextBox)sender;
                if (!MaskIsMatch(currentTextBox.Text)) ResetText(currentTextBox);

                RaiseEvent(new RoutedEventArgs(ValueChangedEvent));
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion
    }
}
