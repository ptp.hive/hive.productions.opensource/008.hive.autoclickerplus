﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using static HiVE.BasicModels.BoxTreeNodes.BoxTreeNodeMethods;

namespace HiVE.BasicModels.BoxTreeNodes
{
    /// <summary>
    /// Represents a collection of BoxTreeNode objects contained within a node or a BcBoxTreeNode control. 
    /// Supports change notification through INotifyCollectionChanged. Implements the non-generic IList to 
    /// provide design-time support.
    /// </summary>  
    public class BoxTreeNodeCollection : IList<BoxTreeNode>, IList, INotifyCollectionChanged
    {
        #region Feilds

        private List<BoxTreeNode> _innerList;
        private BoxTreeNode _node;

        #endregion

        #region Constructor

        /// <summary>
        /// Initialises a new instance of CntBoxTreeNodeCollection using default (empty) values.
        /// </summary>
        public BoxTreeNodeCollection()
        {
            _innerList = new List<BoxTreeNode>();
            _node = new BoxTreeNode();
        }

        /// <summary>
        /// Initalises a new instance of BoxTreeNodeCollection and associates it with the specified BoxTreeNode.
        /// </summary>
        /// <param name="node"></param>
        public BoxTreeNodeCollection(BoxTreeNode node)
        {
            _innerList = new List<BoxTreeNode>();
            _node = node;
        }

        #endregion

        #region Internal\Private Methods and Events

        internal static IEnumerator<BoxTreeNode> GetNodesRecursive(BoxTreeNodeCollection collection, bool reverse)
        {
            if (!reverse)
            {
                for (int i = 0; i < collection.Count; i++)
                {
                    yield return collection[i];
                    IEnumerator<BoxTreeNode> e = GetNodesRecursive(collection[i].Nodes, reverse);
                    while (e.MoveNext()) yield return e.Current;
                }
            }
            else
            {
                for (int i = (collection.Count - 1); i >= 0; i--)
                {
                    IEnumerator<BoxTreeNode> e = GetNodesRecursive(collection[i].Nodes, reverse);
                    while (e.MoveNext()) yield return e.Current;
                    yield return collection[i];
                }
            }
        }

        /// <summary>
        /// Fired when the check state of a node in the collection (or one of its children) changes.
        /// </summary>
        [Browsable(false)]
        internal event EventHandler<CntBoxTreeNodeEventArgs> AfterCheck;

        /// <summary>
        /// Raises the CollectionChanged event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null) CollectionChanged(this, e);
        }

        /// <summary>
        /// Sorts the collection and its entire sub-tree using the specified comparer.
        /// </summary>
        /// <param name="comparer"></param>
        internal void Sort(IComparer<BoxTreeNode> comparer)
        {
            if (comparer == null) comparer = Comparer<BoxTreeNode>.Default;
            SortInternal(comparer);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Recursive helper method for Sort(IComparer&lt;BoxTreeNode&gt;).
        /// </summary>
        /// <param name="comparer"></param>
        private void SortInternal(IComparer<BoxTreeNode> comparer)
        {
            _innerList.Sort(comparer);
            foreach (BoxTreeNode node in _innerList)
            {
                node.Nodes.Sort(comparer);
            }
        }

        /// <summary>
        /// Adds event handlers to the specified node.
        /// </summary>
        /// <param name="item"></param>
        private void AddEventHandlers(BoxTreeNode item)
        {
            item.CheckStateChanged += item_CheckStateChanged;
            item.Nodes.CollectionChanged += CollectionChanged;
            item.Nodes.AfterCheck += AfterCheck;
        }

        /// <summary>
        /// Removes event handlers from the specified node.
        /// </summary>
        /// <param name="item"></param>
        private void RemoveEventHandlers(BoxTreeNode item)
        {
            item.CheckStateChanged -= item_CheckStateChanged;
            item.Nodes.CollectionChanged -= CollectionChanged;
            item.Nodes.AfterCheck -= AfterCheck;
        }

        /// <summary>
        /// Raises the <see cref="AfterCheck"/> event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnAfterCheck(CntBoxTreeNodeEventArgs e)
        {
            if (AfterCheck != null) AfterCheck(this, e);
        }

        /// <summary>
        /// Returns the <see cref="BoxTreeNode"/> that corresponds to the specified path string.
        /// </summary>
        /// <param name="path">The path string.</param>
        /// <param name="pathSeparator">The path separator.</param>
        /// <param name="useNodeNamesForPath">Whether the path is constructed from the name of the node instead of its text.</param>
        /// <returns>The node, or null if the path is empty.</returns>
        internal BoxTreeNode ParsePath(string path, string pathSeparator, bool useNodeNamesForPath)
        {
            BoxTreeNode select = null;

            string[] parts = path.Split(new string[] { pathSeparator }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < parts.Length; i++)
            {
                BoxTreeNodeCollection collection = ((select == null) ? this : select.Nodes);
                if (useNodeNamesForPath)
                {
                    try
                    {
                        select = collection[parts[i]];
                    }
                    catch (KeyNotFoundException ex)
                    {
                        throw new ArgumentException("Invalid path string.", "value", ex);
                    }
                }
                else
                {
                    bool found = false;
                    foreach (BoxTreeNode node in collection)
                    {
                        if (node.Text.Equals(parts[i], StringComparison.InvariantCultureIgnoreCase))
                        {
                            select = node;
                            found = true;
                            break;
                        }
                    }
                    if (!found) throw new ArgumentException("Invalid path string.", "value");
                }
            }

            return select;
        }

        private void item_CheckStateChanged(object sender, EventArgs e)
        {
            OnAfterCheck(new CntBoxTreeNodeEventArgs(sender as BoxTreeNode));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the node with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public BoxTreeNode this[string name]
        {
            get
            {
                foreach (BoxTreeNode o in this)
                {
                    if (Object.Equals(o.Name, name)) return o;
                }

                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Creates a node and adds it to the collection.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public BoxTreeNode Add(string text)
        {
            BoxTreeNode item = new BoxTreeNode(text);
            Add(item);
            return item;
        }

        /// <summary>
        /// Creates a node and adds it to the collection.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public BoxTreeNode Add(string name, string text)
        {
            BoxTreeNode item = new BoxTreeNode(name, text);
            Add(item);
            return item;
        }

        /// <summary>
        /// Adds a range of BoxTreeNode to the collection.
        /// </summary>
        /// <param name="items"></param>
        public void AddRange(IEnumerable<BoxTreeNode> items)
        {
            foreach (BoxTreeNode item in items)
            {
                _innerList.Add(item);
                item.Parent = _node;
                AddEventHandlers(item);
            }
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Adds a range of BoxTreeNode to the collection.
        /// Return the current collection node.
        /// </summary>
        /// <param name="items"></param>
        public IEnumerable<BoxTreeNode> AddRangeNode(IEnumerable<BoxTreeNode> items)
        {
            foreach (BoxTreeNode item in items)
            {
                _innerList.Add(item);
                item.Parent = _node;
                AddEventHandlers(item);
            }
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            return items;
        }

        /// <summary>
        /// Determines whether the collection contains a node with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ContainsKey(string name)
        {
            foreach (BoxTreeNode o in this)
            {
                if (Object.Equals(o.Name, name)) return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the node with the specified name from the collection.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Remove(string name)
        {
            for (int i = 0; i < _innerList.Count; i++)
            {
                if (Object.Equals(_innerList[i].Name, name))
                {
                    BoxTreeNode item = _innerList[i];
                    RemoveEventHandlers(item);
                    _innerList.RemoveAt(i);
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the index of the node with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int IndexOf(string name)
        {
            for (int i = 0; i < _innerList.Count; i++)
            {
                if (Object.Equals(_innerList[i].Name, name)) return i;
            }

            return -1;
        }

        /// <summary>
        /// Returns the <see cref="BoxTreeNode"/> with the specified node text.
        /// </summary>
        /// <param name="text">The text to match.</param>
        /// <param name="comparisonType">The type of string comparison performed.</param>
        /// <param name="recurse">Whether to search recursively through all child nodes.</param>
        /// <returns></returns>
        public BoxTreeNode Find(string text, StringComparison comparisonType, bool recurse)
        {
            IEnumerator<BoxTreeNode> nodes = recurse ? GetNodesRecursive(this, false) : GetEnumerator();

            while (nodes.MoveNext())
            {
                if (nodes.Current.Text.Equals(text, comparisonType))
                {
                    return nodes.Current;
                }
            }

            return null;
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Returns the aggregate check state of this node's children.
        /// </summary>
        /// <param name="Nodes"></param>
        /// <returns></returns>
        public static bool? GetAggregateIsCheck(BoxTreeNodeCollection Nodes)
        {
            bool? state = false;

            bool all = true;
            bool any = false;
            bool chk = false;

            foreach (BoxTreeNode child in Nodes)
            {
                if (child.IsChecked != false) any = true;
                if (child.IsChecked != true) all = false;
                if (child.IsChecked == true) chk = true;
            }

            if (all & chk)
                state = true;
            else if (any)
                state = null;

            return state;
        }

        /// <summary>
        /// Returns the aggregate check state of this node's children.
        /// </summary>
        /// <param name="Nodes"></param>
        /// <returns></returns>
        public static MethodCheckState GetAggregateCheckState(BoxTreeNodeCollection Nodes)
        {
            MethodCheckState state = MethodCheckState.Unchecked;
            bool all = true;
            bool any = false;
            bool chk = false;

            foreach (BoxTreeNode child in Nodes)
            {
                if (child.CheckState != MethodCheckState.Unchecked) any = true;
                if (child.CheckState != MethodCheckState.Checked) all = false;
                if (child.CheckState == MethodCheckState.Checked) chk = true;
            }

            if (all & chk)
                state = MethodCheckState.Checked;
            else if (any)
                state = MethodCheckState.Indeterminate;

            return state;
        }

        /// <summary>
        /// Returns the aggregate check state of all node's and node's children.
        /// </summary>
        /// <param name="Nodes"></param>
        /// <returns></returns>
        public static bool? GetAggregateBoxTreeNodeCollection_IsCheck(BoxTreeNodeCollection Nodes)
        {
            bool? state = false;
            bool? childsState = false;

            bool all = true;
            bool any = false;
            bool chk = false;

            try
            {
                /// Add all root nodes to BoxTreeNode and call for child nodes;
                /// Recursion base case; if given node has no child nodes no more action is taken;
                if (Nodes.Count == 0) { return state; }

                foreach (BoxTreeNode child in Nodes)
                {
                    if (child.IsChecked != false) any = true;
                    if (child.IsChecked != true) all = false;
                    if (child.IsChecked == true) chk = true;

                    if (all && chk)
                        state = true;
                    else if (any)
                    {
                        state = null;
                        return state;
                    }

                    childsState =
                        GetAggregateIsCheck(
                            child.Nodes);

                    if (state != childsState)
                    {
                        state = null;
                        return state;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return state;
        }

        /// <summary>
        /// Returns the aggregate check state of all node's and node's children.
        /// </summary>
        /// <param name="Nodes"></param>
        /// <returns></returns>
        public static MethodCheckState GetAggregateBoxTreeNodeCollection_CheckState(BoxTreeNodeCollection Nodes)
        {
            MethodCheckState state = MethodCheckState.Unchecked;
            MethodCheckState childsState = MethodCheckState.Unchecked;

            bool all = true;
            bool any = false;
            bool chk = false;

            try
            {
                /// Add all root nodes to BoxTreeNode and call for child nodes;
                /// Recursion base case; if given node has no child nodes no more action is taken;
                if (Nodes.Count == 0) { return state; }

                foreach (BoxTreeNode child in Nodes)
                {
                    if (child.IsChecked != false) any = true;
                    if (child.IsChecked != true) all = false;
                    if (child.IsChecked == true) chk = true;

                    if (all && chk)
                        state = MethodCheckState.Checked;
                    else if (any)
                        state = MethodCheckState.Indeterminate;

                    childsState =
                        GetAggregateCheckState(
                            child.Nodes);

                    if (state != childsState)
                    {
                        state = MethodCheckState.Indeterminate;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return state;
        }

        /// <summary>
        /// Get all root nodes;
        /// Recursive method to change IsChecked and call for child nodes of each node;
        /// </summary>
        /// <param name="Nodes"></param>
        /// <param name="isChecked"></param>
        /// <returns></returns>
        public static BoxTreeNodeCollection BindBoxTreeNodeCollection_IsChecked(
            BoxTreeNodeCollection Nodes,
            bool isChecked)
        {
            try
            {
                /// Add all root nodes to BoxTreeNode and call for child nodes;
                /// Recursion base case; if given node has no child nodes no more action is taken;
                if (Nodes.Count == 0) { return Nodes; }

                foreach (BoxTreeNode child in Nodes)
                {
                    child.IsChecked = isChecked;

                    BindBoxTreeNodeCollection_IsChecked(
                        child.Nodes,
                        isChecked);
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return Nodes;
        }

        /// <summary>
        /// Get all checked nodes;
        /// Recursive method to get checked nodes and call for child nodes of each node;
        /// </summary>
        /// <param name="collectionBoxTreeNodeItems"></param>
        /// <param name="isChecked"></param>
        /// <returns></returns>
        public static List<BoxTreeNode> BoxTreeNodeCollection_SelectedItemsNodes(
            BoxTreeNodeCollection collectionBoxTreeNodeItems,
            bool isChecked)
        {
            List<BoxTreeNode> listBoxTreeNodeItems =
                new List<BoxTreeNode>();

            try
            {
                /// Get all root nodes to ItemCollection and call for child nodes;
                /// Recursion base case; if given node has no child nodes no more action is taken;
                if (collectionBoxTreeNodeItems.Count == 0) { return listBoxTreeNodeItems; }

                foreach (BoxTreeNode item in collectionBoxTreeNodeItems)
                {
                    if (item.IsChecked == isChecked)
                    {
                        listBoxTreeNodeItems.Add(item);
                    }

                    listBoxTreeNodeItems.AddRange(
                        BoxTreeNodeCollection_SelectedItemsNodes(
                            item.Nodes,
                            isChecked));
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return listBoxTreeNodeItems;
        }

        /// <summary>
        /// Get all checked nodes;
        /// Recursive method to get checked nodes and call for child nodes of each node;
        /// </summary>
        /// <param name="itemCollectionSource"></param>
        /// <param name="isChecked"></param>
        /// <returns></returns>
        public static List<BoxTreeNode> BoxTreeNodeCollection_SelectedItems(
            System.Windows.Controls.ItemCollection itemCollectionSource,
            bool isChecked)
        {
            List<BoxTreeNode> listBoxTreeNodeItems =
                new List<BoxTreeNode>();

            try
            {
                /// Get all root nodes to ItemCollection and call for child nodes;
                /// Recursion base case; if given node has no child nodes no more action is taken;
                if (itemCollectionSource.Count == 0) { return listBoxTreeNodeItems; }

                foreach (BoxTreeNode item in itemCollectionSource)
                {
                    if (item.IsChecked == isChecked)
                    {
                        listBoxTreeNodeItems.Add(item);
                    }

                    listBoxTreeNodeItems.AddRange(
                        BoxTreeNodeCollection_SelectedItemsNodes(
                            item.Nodes,
                            isChecked));
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return listBoxTreeNodeItems;
        }

        #endregion

        #region ICollection<BoxTreeNode> Members

        /// <summary>
        /// Adds a node to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(BoxTreeNode item)
        {
            _innerList.Add(item);
            item.Parent = _node;
            AddEventHandlers(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Adds a node to the collection.
        /// Return the current node.
        /// </summary>
        /// <param name="item"></param>
        public BoxTreeNode AddNode(BoxTreeNode item)
        {
            _innerList.Add(item);
            item.Parent = _node;
            AddEventHandlers(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            return item;
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            foreach (BoxTreeNode item in _innerList)
            {
                RemoveEventHandlers(item);
            }
            _innerList.Clear();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Determines whether the collection contains the specified node.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(BoxTreeNode item)
        {
            return _innerList.Contains(item);
        }

        /// <summary>
        /// Gets the number of nodes in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return _innerList.Count;
            }
        }

        /// <summary>
        /// Copies all the nodes from the collection to a compatible array.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(BoxTreeNode[] array, int arrayIndex)
        {
            _innerList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Removes the specified node from the collection.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(BoxTreeNode item)
        {
            if (_innerList.Remove(item))
            {
                RemoveEventHandlers(item);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
                return true;
            }

            return false;
        }

        bool ICollection<BoxTreeNode>.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region IEnumerable<BoxTreeNode> Members

        /// <summary>
        /// Returns an enumerator which can be used to cycle through the nodes in the collection (non-recursive).
        /// </summary>
        /// <returns></returns>
        public IEnumerator<BoxTreeNode> GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        #endregion

        #region IList<BoxTreeNode> Members

        /// <summary>
        /// Gets or sets the node at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public BoxTreeNode this[int index]
        {
            get
            {
                return _innerList[index];
            }
            set
            {
                BoxTreeNode oldItem = _innerList[index];
                _innerList[index] = value;
                value.Parent = _node;
                AddEventHandlers(value);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, oldItem));
            }
        }

        /// <summary>
        /// Returns the index of the specified node.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(BoxTreeNode item)
        {
            return _innerList.IndexOf(item);
        }

        /// <summary>
        /// Inserts a node into the collection at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, BoxTreeNode item)
        {
            _innerList.Insert(index, item);
            item.Parent = _node;
            AddEventHandlers(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Removes the node at the specified index from the collection.
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            BoxTreeNode item = _innerList[index];
            RemoveEventHandlers(item);
            _innerList.RemoveAt(index);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
        }

        #endregion

        #region IEnumerable Members (implemented explicitly)

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        #endregion

        #region IList Members (implemented explicitly)

        int IList.Add(object value)
        {
            Add((BoxTreeNode)value);
            return Count - 1;
        }

        bool IList.Contains(object value)
        {
            return Contains((BoxTreeNode)value);
        }

        int IList.IndexOf(object value)
        {
            return IndexOf((BoxTreeNode)value);
        }

        void IList.Insert(int index, object value)
        {
            Insert(index, (BoxTreeNode)value);
        }

        bool System.Collections.IList.IsFixedSize
        {
            get
            {
                return false;
            }
        }

        bool System.Collections.IList.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        void IList.Remove(object value)
        {
            Remove((BoxTreeNode)value);
        }

        object System.Collections.IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = (BoxTreeNode)value;
            }
        }

        #endregion

        #region ICollection Members (implemented explicitly)

        void ICollection.CopyTo(Array array, int index)
        {
            ((ICollection)_innerList).CopyTo(array, index);
        }

        bool ICollection.IsSynchronized
        {
            get
            {
                return ((ICollection)_innerList).IsSynchronized;
            }
        }

        object ICollection.SyncRoot
        {
            get
            {
                return ((ICollection)_innerList).SyncRoot;
            }
        }

        #endregion

        #region INotifyCollectionChanged Members

        /// <summary>
        /// Fired when the collection (sub-tree) changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion
    }
}