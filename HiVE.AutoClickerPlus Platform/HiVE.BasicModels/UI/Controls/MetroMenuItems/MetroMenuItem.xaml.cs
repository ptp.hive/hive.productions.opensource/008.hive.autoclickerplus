﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace HiVE.BasicModels.MetroMenuItems
{
    /// <summary>
    /// Interaction logic for MetroMenuItem.xaml
    /// </summary>
    public partial class MetroMenuItem : MenuItem
    {
        public MetroMenuItem()
        {
            InitializeComponent();

        }

        #region Dependency Properties

        #region ContentStyle

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty ContentStyleProperty =
            DependencyProperty.Register(
                "ContentStyle",
                typeof(Style),
                typeof(MetroMenuItem),
                new FrameworkPropertyMetadata(null, OnContentStyleChanged));

        /// <summary>
        /// Gets / Sets the ContentStyle that the control is showing
        /// </summary>
        public Style ContentStyle
        {
            get
            {
                return (Style)GetValue(ContentStyleProperty);
            }
            set
            {
                SetValue(ContentStyleProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the MetroMenuType property.
        /// </summary>
        /// <param name="d">MetroMenu</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnContentStyleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroMenuItem = (MetroMenuItem)d;
            var oldContentStyle = (Style)e.OldValue;
            var newContentStyle = metroMenuItem.ContentStyle;
            metroMenuItem.OnMetroMenuTypeChanged(oldContentStyle, newContentStyle);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the MetroMenuType property.
        /// </summary>
        /// <param name="oldMetroMenuType">Old Value</param>
        /// <param name="newMetroMenuType">New Value</param>
        protected virtual void OnMetroMenuTypeChanged(Style oldContentStyle, Style newContentStyle)
        {
            SetMetroMenuItemProperties();
        }

        #endregion

        #region ImageSourceUri

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty ImageSourceUriProperty =
            DependencyProperty.Register(
                "ImageSourceUri",
                typeof(string),
                typeof(MetroMenuItem),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Gets / Sets the Image that the control is showing
        /// </summary>
        public string ImageSourceUri
        {
            get
            {
                return (string)GetValue(ImageSourceUriProperty);
            }
            set
            {
                SetValue(ImageSourceUriProperty, value);
            }
        }

        #endregion

        /// <summary>
        /// For the future
        /// </summary>
        #region Placement

        public static readonly DependencyProperty PlacementProperty =
            ContextMenuService.PlacementProperty.AddOwner(
                typeof(MetroMenuItem),
                new FrameworkPropertyMetadata(PlacementMode.Bottom, new PropertyChangedCallback(OnPlacementChanged)));

        /// <summary>
        /// Placement of the Context menu
        /// </summary>
        public PlacementMode Placement
        {
            get { return (PlacementMode)GetValue(PlacementProperty); }
            set { SetValue(PlacementProperty, value); }
        }

        /// <summary>
        /// Placement Property changed callback, pass the value through to the buttons context menu
        /// </summary>
        private static void OnPlacementChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MetroMenuItem s = d as MetroMenuItem;
            if (s == null) return;

            s.EnsureContextMenuIsValid();
            s.ContextMenu.Placement = (PlacementMode)e.NewValue;
        }

        #endregion

        #region MenuPlacement

        /// <summary>
        /// Placement of the Context menu
        /// </summary>
        public PlacementMode MenuPlacement
        {
            get { return (PlacementMode)GetValue(MenuPlacementProperty); }
            set { SetValue(MenuPlacementProperty, value); }
        }

        public static PlacementMode GetMenuPlacement(DependencyObject obj)
        {
            return (PlacementMode)obj.GetValue(MenuPlacementProperty);
        }

        public static void SetMenuPlacement(DependencyObject obj, PlacementMode value)
        {
            obj.SetValue(MenuPlacementProperty, value);
        }

        // Using a DependencyProperty as the backing store for MenuPlacement.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MenuPlacementProperty =
            DependencyProperty.RegisterAttached("MenuPlacement",
            typeof(PlacementMode),
            typeof(MetroMenuItem),
            new FrameworkPropertyMetadata(PlacementMode.Bottom, FrameworkPropertyMetadataOptions.Inherits, new PropertyChangedCallback(OnMenuPlacementChanged)));

        private static void OnMenuPlacementChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var menuItem = o as MenuItem;
            if (menuItem != null)
            {
                if (menuItem.IsLoaded)
                {
                    SetPopupPlacement(menuItem, (PlacementMode)e.NewValue);
                }
                else
                {
                    menuItem.Loaded += new RoutedEventHandler((m, v) => SetPopupPlacement(menuItem, (PlacementMode)e.NewValue));
                }
            }
        }

        private static void SetPopupPlacement(MenuItem menuItem, PlacementMode placementMode)
        {
            Popup popup = menuItem.Template.FindName("PART_Popup", menuItem) as Popup;
            if (popup != null)
            {
                popup.Placement = placementMode;
            }
        }

        #endregion

        /// <summary>
        /// For the future
        /// </summary>
        #region IsContextMenuOpen

        public static readonly DependencyProperty IsContextMenuOpenProperty =
            DependencyProperty.Register(
                "IsContextMenuOpen",
                typeof(bool),
                typeof(MetroMenuItem),
                new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsContextMenuOpenChanged)));

        /// <summary>
        /// Gets or sets the IsContextMenuOpen property. 
        /// </summary>
        public bool IsContextMenuOpen
        {
            get { return (bool)GetValue(IsContextMenuOpenProperty); }
            set { SetValue(IsContextMenuOpenProperty, value); }
        }

        private static void OnIsContextMenuOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MetroMenuItem s = (MetroMenuItem)d;
            s.EnsureContextMenuIsValid();

            if (!s.ContextMenu.HasItems)
                return;

            bool value = (bool)e.NewValue;

            if (value && !s.ContextMenu.IsOpen)
                s.ContextMenu.IsOpen = true;
            else if (!value && s.ContextMenu.IsOpen)
                s.ContextMenu.IsOpen = false;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Refresh MetroMenuType and Set Properties
        /// </summary>
        private void SetMetroMenuItemProperties()
        {
            try
            {
                /// For the future
                return;

                if (ContentStyle == null)
                {
                    if (metroMenuItemUC.Parent != null)
                    {
                        metroMenuItemUC.Style = Application.Current.FindResource(string.Format("{0}", "MetroUI.MenuItemStyle.Dark")) as Style;
                    }
                    else
                    {
                        metroMenuItemUC.Style = Application.Current.FindResource(string.Format("{0}", "MetroUI.MenuParentStyle.Dark")) as Style;
                    }

                    metroMenuItemUC.ItemContainerStyle = Application.Current.FindResource(string.Format("{0}", "MetroUI.MenuItemStyle.Dark")) as Style;
                }
                else
                {
                    metroMenuItemUC.Style = ContentStyle;
                    metroMenuItemUC.ItemContainerStyle = ContentStyle;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Make sure the Context menu is not null
        /// </summary>
        private void EnsureContextMenuIsValid()
        {
            if (this.ContextMenu == null)
            {
                this.ContextMenu = new ContextMenu();
                this.ContextMenu.PlacementTarget = this;
                this.ContextMenu.Placement = Placement;

                this.ContextMenu.Opened += ((sender, routedEventArgs) => IsContextMenuOpen = true);
                this.ContextMenu.Closed += ((sender, routedEventArgs) => IsContextMenuOpen = false);
            }
        }

        #endregion

        /// <summary>
        /// Load this user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroMenuItemUC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
