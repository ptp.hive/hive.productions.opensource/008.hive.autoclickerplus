﻿using HiVE.BasicModels.MetroButtons;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_PasswordValueToPasswordDisplay : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            List<MetroButton> returnValuePasswordDisplay = new List<MetroButton>();

            try
            {
                if ((string)value != null)
                {
                    foreach (char item in (string)value)
                    {
                        MetroButton metroButtonPassword = new MetroButton();
                        metroButtonPassword.MetroButtonType = MetroButtonType.Password;
                        metroButtonPassword.DisplayStyle = MetroButtonDisplayStyle.Image;
                        returnValuePasswordDisplay.Add(metroButtonPassword);
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValuePasswordDisplay;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from PasswordDisplay back to PasswordValue");
        }
    }
}
