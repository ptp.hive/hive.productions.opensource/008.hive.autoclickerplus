﻿using System.Windows.Controls;
using System.Windows.Media;

namespace HiVE.BasicModels.BoxItems
{
    public class BoxMetroTitle
    {
        public byte[] ImageByteArray { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public UserControl UserControlPath { get; set; }
        public string Background { get; set; }

        public BoxMetroTitle() { }

        public BoxMetroTitle(
            byte[] imageByteArray,
            string title,
            string description,
            UserControl userControlPath,
            string background)
        {
            this.ImageByteArray = imageByteArray;
            this.Title = title;
            this.Description = description;
            this.UserControlPath = userControlPath;
            this.Background = background;
        }
    }
}
