﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace HiVE.AutoClickerPlus.UserControls
{
    /// <summary>
    /// Interaction logic for AutoClickerUC.xaml
    /// </summary>
    public partial class AutoClickerUC : UserControl
    {
        public AutoClickerUC()
        {
            InitializeComponent();
        }

        #region Feilds

        private NavigationService oNavigationService = null;
        private WpfAppControlBaseUC oWpfAppControlBaseUC { get; set; } = null;

        #endregion

        #region Dependency Properties

        #region DisplayTitle

        /// <summary>
        /// Dependency Object for the value of the AutoClickerUC Control
        /// </summary>
        public static readonly DependencyProperty DisplayTitleProperty =
            DependencyProperty.Register(
                "DisplayTitle",
                typeof(string),
                typeof(AutoClickerUC),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Gets / Sets the DisplayTitle that the control is showing
        /// </summary>
        public string DisplayTitle
        {
            get
            {
                return (string)GetValue(DisplayTitleProperty);
            }
            set
            {
                SetValue(DisplayTitleProperty, value);
            }
        }

        #endregion

        #endregion

        #region MenuBar Methods

        private void metroButtonOpenNewClicker_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonStartNewClicker_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonCancelClicker_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonOpenApp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenNewWpfAppControlBaseUC();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region Methods

        private void OpenNewWpfAppControlBaseUC()
        {
            try
            {
                gridUserControls.Children.Clear();

                if (oWpfAppControlBaseUC != null)
                {
                    oWpfAppControlBaseUC = null;
                }

                oWpfAppControlBaseUC = new WpfAppControlBaseUC();

                gridUserControls.Children.Add(oWpfAppControlBaseUC);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        /// <summary>
        /// Load this user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autoClickerUC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                oNavigationService =
                    NavigationService.GetNavigationService(this);

                OpenNewWpfAppControlBaseUC();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void autoClickerUC_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
