﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.MetroButtons
{
    public class Converter_ActualSizeToSizeMinusMarginPlusText : IMultiValueConverter
    {
        private const double _commonMargin = 32;
        private const double _specialLightMargin = 4;

        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            double returnValue = 0;

            try
            {
                /// Always test MultiValueConverter inputs for non-null
                /// (to avoid crash bugs for views in the designer)
                if (values[0] is double && values[1] is MetroButtonType && values[2] is bool)
                {
                    double ActualSize = (double)values[0];
                    MetroButtonType MetroButtonType = (MetroButtonType)values[1];
                    bool IsCircle = (bool)values[2];

                    if (MetroButtonType == MetroButtonType.Password ||
                        MetroButtonType == MetroButtonType.Flat ||
                        MetroButtonType == MetroButtonType.Flat_Large ||
                        IsCircle)
                    {
                        returnValue = ActualSize - _specialLightMargin;
                    }
                    else
                    {
                        returnValue = ActualSize - _commonMargin;
                    }
                }

                if (returnValue < 0)
                { returnValue = 0; }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
