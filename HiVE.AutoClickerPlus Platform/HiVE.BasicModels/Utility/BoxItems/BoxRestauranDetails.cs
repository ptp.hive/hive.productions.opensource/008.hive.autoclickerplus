﻿namespace HiVE.BasicModels.BoxItems
{
    public class BoxRestauranDetails
    {
        public int CompanyID { get; set; } = 1;
        public int RestaurantID { get; set; } = 1;
        public string Name { get; set; } = "My Restauran";
        public string Longitude { get; set; } = "Longitude";
        public string Latitude { get; set; } = "Latitude";
    }
}
