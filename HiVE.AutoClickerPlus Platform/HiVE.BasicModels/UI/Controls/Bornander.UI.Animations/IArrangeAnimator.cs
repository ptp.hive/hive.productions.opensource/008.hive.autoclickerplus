﻿using System.Windows;

namespace HiVE.BasicModels.Animations
{
    public interface IArrangeAnimator
    {
        Rect Arrange(double elapsedTime, Point desiredPosition, Size desiredSize, Point currentPosition, Size currentSize, bool fixDeltaWidth, bool fixDeltaHeight);
    }
}
