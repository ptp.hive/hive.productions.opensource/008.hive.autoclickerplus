﻿using HiVE.AutoClickerPlus.Utility.Helpers;
using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Diagnostics;
using System.Windows;

namespace HiVE.AutoClickerPlus
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            try
            {
                if (e.ExtraData != null && e.ExtraData is DateTime)
                {
                    Debug.WriteLine(
                        string.Format(
                            "Navigating to {0} at {1:t}",
                            e.Uri.ToString(),
                            e.ExtraData));
                }
                else
                {
                    Debug.WriteLine(
                        string.Format(
                            "Navigating to {0}",
                            e.Uri.ToString()));
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void Application_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                if (e.ExtraData != null && e.ExtraData is DateTime)
                {
                    Debug.WriteLine(
                        string.Format(
                            "Navigating to {0} at {1:t}",
                            e.Uri.ToString(),
                            e.ExtraData));
                }
                else
                {
                    Debug.WriteLine(
                        string.Format(
                            "Navigating to {0}",
                            e.Uri.ToString()));
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void Application_NavigationFailed(object sender, System.Windows.Navigation.NavigationFailedEventArgs e)
        {
            try
            {
                if (e.Exception is System.Net.WebException)
                {
                    MessageBox.Show(
                        string.Format(
                            "Your attemp to navigate to {0} failed",
                            e.Uri.ToString()));

                    e.Handled = true;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void Application_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                SpecialModels.GetMainSettings();

                if (Currents.CurrentMainSettings.PermissionScaleTransform)
                {
                    Application.Current.MainWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    Application.Current.MainWindow.WindowState = WindowState.Normal;
                    Application.Current.MainWindow.ResizeMode = ResizeMode.CanResize;
                    Application.Current.MainWindow.WindowStyle = WindowStyle.ToolWindow;
                    //Application.Current.MainWindow.WindowStyle = WindowStyle.None;
                    //Application.Current.MainWindow.ResizeMode = ResizeMode.CanResize;

                    Application.Current.MainWindow.Height = Currents.CurrentMainSettings.MainWindowHeight;
                    Application.Current.MainWindow.Width = Currents.CurrentMainSettings.MainWindowWidth;
                }
                else
                {
                    Application.Current.MainWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    Application.Current.MainWindow.WindowState = WindowState.Maximized;
                    //Application.Current.MainWindow.WindowStyle = WindowStyle.None;
                    //Application.Current.MainWindow.ResizeMode = ResizeMode.NoResize;
                    Application.Current.MainWindow.WindowStyle = WindowStyle.SingleBorderWindow;
                    Application.Current.MainWindow.ResizeMode = ResizeMode.CanResize;

                    Application.Current.MainWindow.Height = Currents.CurrentMainSettings.MainWindowHeight;
                    Application.Current.MainWindow.Width = Currents.CurrentMainSettings.MainWindowWidth;
                }

                GeneralModels.CommonMainSettings();
                GeneralModels.ActiveAutoRunStartupStatus(true);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
