﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_DataGridRowBackgroundToBrushes : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                var item = (DataGridRow)value;
                var listView = ItemsControl.ItemsControlFromItemContainer(item) as DataGrid;

                /// Get the index of a DataGridCell;
                int index = listView.ItemContainerGenerator.IndexFromContainer(item);

                if (index % 2 == 0)
                { return Brushes.LightBlue; }
                else
                { return Brushes.Beige; }
            }
            catch { return null; }
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from Brushes back to DataGridRowBackground");
        }
    }
}
