﻿using HiVE.AutoClickerPlus.Utility.Helpers;
using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Controls;

namespace HiVE.AutoClickerPlus.UserControls
{
    /// <summary>
    /// Interaction logic for WpfAppControlBaseUC.xaml
    /// </summary>
    public partial class WpfAppControlBaseUC : UserControl
    {
        public WpfAppControlBaseUC()
        {
            InitializeComponent();

            /// Set main startup settings
            HelperFunctions.ApplicationSettings_WpfAppControl(this);

            /// Scale this page to show in another resolution of display
            HelperFunctions.ScaleTransformAnimated_WpfAppControl(this);

            try
            {
                appControlMain.BeforeTimeoutThreshold =
                    Currents.CurrentMainSettings.BeforeTimeoutThreshold;
                appControlMain.AfterTimeoutThreshold =
                    Currents.CurrentMainSettings.AfterTimeoutThreshold;

                appControlMain.ExeName =
                    Currents.CurrentMainSettings.AppExeName;

                this.Unloaded += new RoutedEventHandler((s, e) => { appControlMain.Dispose(); });
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
