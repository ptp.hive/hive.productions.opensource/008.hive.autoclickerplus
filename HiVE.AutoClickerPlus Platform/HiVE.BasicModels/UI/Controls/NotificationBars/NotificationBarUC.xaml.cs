﻿using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.UI.Controls.NotificationBars.Models;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using static HiVE.BasicModels.BoxItems.BoxSpecialMethods;

namespace HiVE.BasicModels.UI.Controls.NotificationBars
{
    /// <summary>
    /// Interaction logic for NotificationBarUC.xaml
    /// </summary>
    public partial class NotificationBarUC : UserControl
    {
        public NotificationBarUC()
        {
            InitializeComponent();

            Notifications = new List<BoxNotification>();
        }

        #region Feilds

        #endregion

        #region Dependency Properties

        #region Notifications

        /// <summary>
        /// Dependency Object for the value of the NotificationBarUC Control
        /// </summary>
        public static readonly DependencyProperty NotificationsProperty =
            DependencyProperty.Register(
                "Notifications",
                typeof(List<BoxNotification>),
                typeof(NotificationBarUC),
                new FrameworkPropertyMetadata(new List<BoxNotification>()));

        /// <summary>
        /// Gets / Sets the Notifications that the control is showing
        /// Need for create and binding items of list box
        /// </summary>
        public List<BoxNotification> Notifications
        {
            get
            {
                return (List<BoxNotification>)GetValue(NotificationsProperty);
            }
            set
            {
                SetValue(NotificationsProperty, value);
            }
        }

        #endregion

        #region IsShowNotificationBar

        /// <summary>
        /// Dependency Object for the value of the NotificationBarUC Control
        /// </summary>
        public static readonly DependencyProperty IsShowNotificationBarProperty =
            DependencyProperty.Register(
                "IsShowNotificationBar",
                typeof(bool),
                typeof(NotificationBarUC),
                new PropertyMetadata(false));

        /// <summary>
        /// Gets / Sets the IsShowNotificationBar that the control is showing
        /// </summary>
        public bool IsShowNotificationBar
        {
            get
            {
                return (bool)GetValue(IsShowNotificationBarProperty);
            }
            set
            {
                SetValue(IsShowNotificationBarProperty, value);
            }
        }

        #endregion

        #region ContentNotificationSenderType

        /// <summary>
        /// Dependency Object for the value of the NotificationBarUC Control
        /// </summary>
        public static readonly DependencyProperty ContentNotificationSenderTypeProperty =
            DependencyProperty.Register(
                "ContentNotificationSenderType",
                typeof(MethodContentNotificationSenderType),
                typeof(NotificationBarUC),
                new FrameworkPropertyMetadata(MethodContentNotificationSenderType.Null, OnContentNotificationSenderTypeChanged));

        /// <summary>
        /// Gets / Sets the ContentNotificationSenderType that the control is showing
        /// </summary>
        public MethodContentNotificationSenderType ContentNotificationSenderType
        {
            get
            {
                return (MethodContentNotificationSenderType)GetValue(ContentNotificationSenderTypeProperty);
            }
            set
            {
                SetValue(ContentNotificationSenderTypeProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the ContentNotificationSenderType property.
        /// </summary>
        /// <param name="d">NotificationBarUC</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnContentNotificationSenderTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var NotificationBarUC = (NotificationBarUC)d;
            var oldContentNotificationSenderType = (MethodContentNotificationSenderType)e.OldValue;
            var newContentNotificationSenderType = NotificationBarUC.ContentNotificationSenderType;
            NotificationBarUC.OnContentNotificationSenderTypeChanged(oldContentNotificationSenderType, newContentNotificationSenderType);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ContentNotificationSenderType property.
        /// </summary>
        /// <param name="oldContentNotificationSenderType">Old Value</param>
        /// <param name="newContentNotificationSenderType">New Value</param>
        protected virtual void OnContentNotificationSenderTypeChanged(MethodContentNotificationSenderType oldContentNotificationSenderType, MethodContentNotificationSenderType newContentNotificationSenderType)
        {
            try
            {
                RefreshContentNotificationSenderType();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region MaxHeightBar

        /// <summary>
        /// Dependency Object for the value of the NotificationBarUC Control
        /// </summary>
        public static readonly DependencyProperty MaxHeightBarProperty =
            DependencyProperty.Register(
                "MaxHeightBar",
                typeof(decimal),
                typeof(NotificationBarUC),
                new FrameworkPropertyMetadata(decimal.Parse("480")));

        /// <summary>
        /// Gets / Sets the value that the control is showing
        /// Default is 560
        /// </summary>
        public decimal MaxHeightBar
        {
            get
            {
                return (decimal)GetValue(MaxHeightBarProperty);
            }
            set
            {
                SetValue(MaxHeightBarProperty, value);
            }
        }

        #endregion

        #region MaxWidthItem

        /// <summary>
        /// Dependency Object for the value of the NotificationBarUC Control
        /// </summary>
        public static readonly DependencyProperty MaxWidthItemProperty =
            DependencyProperty.Register(
                "MaxWidthItem",
                typeof(decimal),
                typeof(NotificationBarUC),
                new FrameworkPropertyMetadata(decimal.Parse("180")));

        /// <summary>
        /// Gets / Sets the value that the control is showing
        /// Default is 240
        /// </summary>
        public decimal MaxWidthItem
        {
            get
            {
                return (decimal)GetValue(MaxWidthItemProperty);
            }
            set
            {
                SetValue(MaxWidthItemProperty, value);
            }
        }

        #endregion

        #endregion

        #region Methods

        private void RefreshContentNotificationSenderType()
        {
            try
            {
                listBoxNotifications.ItemsSource = null;

                switch (ContentNotificationSenderType)
                {
                    case MethodContentNotificationSenderType.MainPage:
                        {
                            listBoxNotifications.ItemsSource =
                                Notifications;

                            break;
                        }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonOKItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IsShowNotificationBar = false;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonClearItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Notifications == null) { Notifications = new List<BoxNotification>(); }

                Notifications.Clear();
                IsShowNotificationBar = false;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        public void AddNewNotification(BoxNotification Notification)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    if (Notifications == null) { Notifications = new List<BoxNotification>(); }

                    if (Notification != null)
                    {
                        Notifications.Add(Notification);
                    }

                    ContentNotificationSenderType = MethodContentNotificationSenderType.Null;
                    ContentNotificationSenderType = MethodContentNotificationSenderType.MainPage;

                    IsShowNotificationBar = true;
                }));
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        public void AddNewInformationDetails(BoxInformationDetails InformationDetails)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    if (Notifications == null) { Notifications = new List<BoxNotification>(); }

                    if (InformationDetails != null)
                    {
                        BoxNotification oBoxNotification = new BoxNotification();

                        oBoxNotification = SetDefaultNotificationDetails.BoxInformationDetailsToBoxNotification(InformationDetails);

                        Notifications.Add(oBoxNotification);
                    }

                    ContentNotificationSenderType = MethodContentNotificationSenderType.Null;
                    ContentNotificationSenderType = MethodContentNotificationSenderType.MainPage;

                    IsShowNotificationBar = true;
                }));
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        public void RemoveNotification(BoxNotification Notification)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    if (Notifications == null) { Notifications = new List<BoxNotification>(); }

                    if (Notification != null)
                    {
                        Notifications.Remove(Notification);
                    }

                    ContentNotificationSenderType = MethodContentNotificationSenderType.Null;
                    ContentNotificationSenderType = MethodContentNotificationSenderType.MainPage;

                    IsShowNotificationBar = true;
                }));
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        /// <summary>
        /// Load this user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notificationBarUC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
