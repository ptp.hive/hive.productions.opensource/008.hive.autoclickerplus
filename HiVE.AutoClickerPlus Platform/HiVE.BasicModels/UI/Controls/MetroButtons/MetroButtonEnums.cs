﻿using System.ComponentModel;

namespace HiVE.BasicModels.MetroButtons
{
    public enum MetroButtonType
    {
        #region Default Buttons

        [SpecialDescription("")]
        [Description("#00000000")]
        None = 0,                   /// Color.Empty
        [SpecialDescription("")]
        [Description("#FFFFFAFA")]
        Default,                /// 0xFFFFFAFA,         /// Snow
        [SpecialDescription("")]
        [Description("#FF64B4E2")]
        Primary,                /// 0xFF0044CC,         /// RichVein
        [SpecialDescription("")]
        [Description("#FF73C16B")]
        Success,                /// 0xFF008000,         /// Green
        [SpecialDescription("")]
        [Description("#FF008B8B")]
        Info,                   /// 0xFF008B8B,         /// DarkCyan
        [SpecialDescription("")]
        [Description("#FFFFD700")]
        Warning,                /// 0xFFFFD700,         /// Gold
        [SpecialDescription("")]
        [Description("#FFDC143C")]
        Danger,                 /// 0xFFDC143C,         /// Crimson
        [SpecialDescription("")]
        [Description("#FF9400D3")]
        Surprise,               /// 0xFF9400D3,         /// DarkViolet
        [SpecialDescription("")]
        [Description("#FF202B30")]
        Inverse,                /// 0xFF202B30,         /// Dark
        [SpecialDescription("Normal")]
        [Description("#FFAAB2BD")]
        Normal,                 /// 0xFFAAB2BD,         /// MediumGray
        [SpecialDescription("Link")]
        [Description("#FF64B4E2")]
        Link,                   /// 0xFF1E90FF,         /// DodgerBlue          JustText

        #endregion

        #region Common

        [SpecialDescription("OK")]
        [Description("#FF0044CC")]
        OK,
        [SpecialDescription("Cancel")]
        [Description("#FFFFD700")]
        Cancel,
        [SpecialDescription("Abort")]
        [Description("#FFFFD700")]
        Abort,
        [SpecialDescription("Retry")]
        [Description("#FF1E90FF")]
        Retry,
        [SpecialDescription("Ignore")]
        [Description("#FF202B30")]
        Ignore,
        [SpecialDescription("Yes")]
        [Description("#FF0044CC")]
        Yes,
        [SpecialDescription("No")]
        [Description("#FFDC143C")]
        No,

        #endregion

        #region Create/Edit Mode

        [SpecialDescription("New")]
        [Description("#FF0044CC")]
        New,
        [SpecialDescription("Add")]
        [Description("#FF008000")]
        Add,
        [SpecialDescription("Edit")]
        [Description("#FF008B8B")]
        Edit,
        [SpecialDescription("Delete")]
        [Description("#FFDC143C")]
        Delete,
        [SpecialDescription("Save")]
        [Description("#FF008000")]
        Save,
        [SpecialDescription("Update")]
        [Description("#FF008000")]
        Update,
        [SpecialDescription("Refresh")]
        [Description("#FF008B8B")]
        Refresh,
        [SpecialDescription("Submit")]
        [Description("#FF008000")]
        Submit,
        [SpecialDescription("Done")]
        [Description("#FF0044CC")]
        Done,

        #endregion

        #region BackupClient

        [SpecialDescription("Backup")]
        [Description("#FF9400D3")]
        Backup,
        [SpecialDescription("Do Nothing")]
        [Description("#FFAAB2BD")]
        DoNothing,

        #endregion

        #region Connection

        [SpecialDescription("Try To Reconnect")]
        [Description("#FF9400D3")]
        TryToReconnect,
        [SpecialDescription("Try Again ...")]
        [Description("#FFFFFAFA")]
        TryAgain,
        [SpecialDescription("Connect")]
        [Description("#FF1E90FF")]
        Connect,
        [SpecialDescription("Exit")]
        [Description("#FFDC143C")]
        Exit,

        #endregion

        /// AlreadyExists, Download, Upload
        #region FileManager

        [SpecialDescription("Overwrite")]
        [Description("#FFDC143C")]
        Overwrite,
        [SpecialDescription("Overwrite If Older")]
        [Description("#FFAAB2BD")]
        OverwriteIfOlder,
        [SpecialDescription("Skip")]
        [Description("#FF202B30")]
        Skip,
        [SpecialDescription("Rename")]
        [Description("#FF008B8B")]
        Rename,
        [SpecialDescription("Append")]
        [Description("#FFFFFAFA")]
        Append,
        [SpecialDescription("Download")]
        [Description("#FF008000")]
        Download,
        [SpecialDescription("Upload")]
        [Description("#FF0044CC")]
        Upload,

        #endregion

        #region Special

        [SpecialDescription("0")]
        [Description("#FFFFFFFF")]
        Numeric,
        [SpecialDescription("")]
        [Description("#00000000")]
        Password,
        [SpecialDescription("Confirmation")]
        [Description("#FF73C16B")]
        Confirmation,
        [SpecialDescription("Back")]
        [Description("#FFDC143C")]
        Back,
        [SpecialDescription("Remove")]
        [Description("#FFF6CF5A")]
        Remove,
        [SpecialDescription("Clear")]
        [Description("#FFDC143C")]
        Clear,
        [SpecialDescription("Login")]
        [Description("#00000000")]
        Login,
        [SpecialDescription("")]
        [Description("#FFB8860B")]
        Flat,
        [SpecialDescription("")]
        [Description("#FF008B8B")]
        Flat_Large,

        #endregion

        #region Custom by user

        [SpecialDescription("Custom")]
        [Description("#00000000")]
        Custom,

        #endregion
    }

    public enum MetroButtonDisplayStyle
    {
        None,
        Text,
        Image,
        ImageAndText
    }

    /// <summary>
    /// In the feuture
    /// </summary>
    public enum MetroButtonShapeType
    {
        Default,
        Circle,
        Square,
        Rectangle,
        HorizontalRectangle,
        VerticalRectangle
    }

    /// <summary>
    /// In the feuture
    /// </summary>
    public enum MetroButtonSizeType
    {
        Default,
        Normal,
        Small,
        Medium,
        Large,
        XLarge
    }

    /// <summary>
    /// Get Common Colors
    /// </summary>
    internal enum MethodColorsType : uint
    {
        #region Method United Colors

        [Description("#00000000")]
        None,                   /// Color.Empty
        [Description("#FFFFFAFA")]
        Default,                /// 0xFFFFFAFA,         /// Snow
        [Description("#FF0044CC")]
        Primary,                /// 0xFF0044CC,         /// RichVein
        [Description("#FF008000")]
        Success,                /// 0xFF008000,         /// Green
        [Description("#FF008B8B")]
        Info,                   /// 0xFF008B8B,         /// DarkCyan
        [Description("#FFFFD700")]
        Warning,                /// 0xFFFFD700,         /// Gold
        [Description("#FFDC143C")]
        Danger,                 /// 0xFFDC143C,         /// Crimson
        [Description("#FF9400D3")]
        Surprise,               /// 0xFF9400D3,         /// DarkViolet
        [Description("#FF202B30")]
        Inverse,                /// 0xFF202B30,         /// Dark
        [Description("#FFAAB2BD")]
        Normal,                 /// 0xFFAAB2BD,         /// MediumGray
        [Description("#FF1E90FF")]
        Link,                   /// 0xFF1E90FF,         /// DodgerBlue          JustText

        #endregion

        #region Method .NET C# Colors

        [Description("#FFF0F8FF")]
        AliceBlue = 0xFFF0F8FF,
        [Description("#FFFAEBD7")]
        AntiqueWhite = 0xFFFAEBD7,
        [Description("#FF00FFFF")]
        Aqua = 0xFF00FFFF,
        [Description("#FF7FFFD4")]
        Aquamarine = 0xFF7FFFD4,
        [Description("#FFF0FFFF")]
        Azure = 0xFFF0FFFF,
        [Description("#FFF5F5DC")]
        Beige = 0xFFF5F5DC,
        [Description("#FFFFE4C4")]
        Bisque = 0xFFFFE4C4,
        [Description("#FF000000")]
        Black = 0xFF000000,
        [Description("#FFFFEBCD")]
        BlanchedAlmond = 0xFFFFEBCD,
        [Description("#FF0000FF")]
        Blue = 0xFF0000FF,
        [Description("#FF8A2BE2")]
        BlueViolet = 0xFF8A2BE2,
        [Description("#FFA52A2A")]
        Brown = 0xFFA52A2A,
        [Description("#FFDEB887")]
        BurlyWood = 0xFFDEB887,
        [Description("#FF5F9EA0")]
        CadetBlue = 0xFF5F9EA0,
        [Description("#FF7FFF00")]
        Chartreuse = 0xFF7FFF00,
        [Description("#FFD2691E")]
        Chocolate = 0xFFD2691E,
        [Description("#FFFF7F50")]
        Coral = 0xFFFF7F50,
        [Description("#FF6495ED")]
        CornflowerBlue = 0xFF6495ED,
        [Description("#FFFFF8DC")]
        Cornsilk = 0xFFFFF8DC,
        [Description("#FFDC143C")]
        Crimson = 0xFFDC143C,
        [Description("#FF00FFFF")]
        Cyan = 0xFF00FFFF,
        [Description("#FF00008B")]
        DarkBlue = 0xFF00008B,
        [Description("#FF008B8B")]
        DarkCyan = 0xFF008B8B,
        [Description("#FFB8860B")]
        DarkGoldenRod = 0xFFB8860B,
        [Description("#FFA9A9A9")]
        DarkGray = 0xFFA9A9A9,
        [Description("#FF006400")]
        DarkGreen = 0xFF006400,
        [Description("#FFBDB76B")]
        DarkKhaki = 0xFFBDB76B,
        [Description("#FF8B008B")]
        DarkMagenta = 0xFF8B008B,
        [Description("#FF556B2F")]
        DarkOliveGreen = 0xFF556B2F,
        [Description("#FFFF8C00")]
        DarkOrange = 0xFFFF8C00,
        [Description("#FF9932CC")]
        DarkOrchid = 0xFF9932CC,
        [Description("#FF8B0000")]
        DarkRed = 0xFF8B0000,
        [Description("#FFE9967A")]
        DarkSalmon = 0xFFE9967A,
        [Description("#FF8FBC8F")]
        DarkSeaGreen = 0xFF8FBC8F,
        [Description("#FF483D8B")]
        DarkSlateBlue = 0xFF483D8B,
        [Description("#FF2F4F4F")]
        DarkSlateGray = 0xFF2F4F4F,
        [Description("#FF00CED1")]
        DarkTurquoise = 0xFF00CED1,
        [Description("#FF9400D3")]
        DarkViolet = 0xFF9400D3,
        [Description("#FFFF1493")]
        DeepPink = 0xFFFF1493,
        [Description("#FF00BFFF")]
        DeepSkyBlue = 0xFF00BFFF,
        [Description("#FF696969")]
        DimGray = 0xFF696969,
        [Description("#FF1E90FF")]
        DodgerBlue = 0xFF1E90FF,
        [Description("#FFB22222")]
        FireBrick = 0xFFB22222,
        [Description("#FFFFFAF0")]
        FloralWhite = 0xFFFFFAF0,
        [Description("#FF228B22")]
        ForestGreen = 0xFF228B22,
        [Description("#FFFF00FF")]
        Fuchsia = 0xFFFF00FF,
        [Description("#FFDCDCDC")]
        Gainsboro = 0xFFDCDCDC,
        [Description("#FFF8F8FF")]
        GhostWhite = 0xFFF8F8FF,
        [Description("#FFFFD700")]
        Gold = 0xFFFFD700,
        [Description("#FFDAA520")]
        GoldenRod = 0xFFDAA520,
        [Description("#FF808080")]
        Gray = 0xFF808080,
        [Description("#FF008000")]
        Green = 0xFF008000,
        [Description("#FFADFF2F")]
        GreenYellow = 0xFFADFF2F,
        [Description("#FFF0FFF0")]
        HoneyDew = 0xFFF0FFF0,
        [Description("#FFFF69B4")]
        HotPink = 0xFFFF69B4,
        [Description("#FFCD5C5C")]
        IndianRed = 0xFFCD5C5C,
        [Description("#FF4B0082")]
        Indigo = 0xFF4B0082,
        [Description("#FFFFFFF0")]
        Ivory = 0xFFFFFFF0,
        [Description("#FFF0E68C")]
        Khaki = 0xFFF0E68C,
        [Description("#FFE6E6FA")]
        Lavender = 0xFFE6E6FA,
        [Description("#FFFFF0F5")]
        LavenderBlush = 0xFFFFF0F5,
        [Description("#FF7CFC00")]
        LawnGreen = 0xFF7CFC00,
        [Description("#FFFFFACD")]
        LemonChiffon = 0xFFFFFACD,
        [Description("#FFADD8E6")]
        LightBlue = 0xFFADD8E6,
        [Description("#FFF08080")]
        LightCoral = 0xFFF08080,
        [Description("#FFE0FFFF")]
        LightCyan = 0xFFE0FFFF,
        [Description("#FFFAFAD2")]
        LightGoldenRodYellow = 0xFFFAFAD2,
        [Description("#FFD3D3D3")]
        LightGray = 0xFFD3D3D3,
        [Description("#FF90EE90")]
        LightGreen = 0xFF90EE90,
        [Description("#FFFFB6C1")]
        LightPink = 0xFFFFB6C1,
        [Description("#FFFFA07A")]
        LightSalmon = 0xFFFFA07A,
        [Description("#FF20B2AA")]
        LightSeaGreen = 0xFF20B2AA,
        [Description("#FF87CEFA")]
        LightSkyBlue = 0xFF87CEFA,
        [Description("#FF778899")]
        LightSlateGray = 0xFF778899,
        [Description("#FFB0C4DE")]
        LightSteelBlue = 0xFFB0C4DE,
        [Description("#FFFFFFE0")]
        LightYellow = 0xFFFFFFE0,
        [Description("#FF00FF00")]
        Lime = 0xFF00FF00,
        [Description("#FF32CD32")]
        LimeGreen = 0xFF32CD32,
        [Description("#FFFAF0E6")]
        Linen = 0xFFFAF0E6,
        [Description("#FFFF00FF")]
        Magenta = 0xFFFF00FF,
        [Description("#FF800000")]
        Maroon = 0xFF800000,
        [Description("#FF66CDAA")]
        MediumAquaMarine = 0xFF66CDAA,
        [Description("#FF0000CD")]
        MediumBlue = 0xFF0000CD,
        [Description("#FFBA55D3")]
        MediumOrchid = 0xFFBA55D3,
        [Description("#FF9370DB")]
        MediumPurple = 0xFF9370DB,
        [Description("#FF3CB371")]
        MediumSeaGreen = 0xFF3CB371,
        [Description("#FF7B68EE")]
        MediumSlateBlue = 0xFF7B68EE,
        [Description("#FF00FA9A")]
        MediumSpringGreen = 0xFF00FA9A,
        [Description("#FF48D1CC")]
        MediumTurquoise = 0xFF48D1CC,
        [Description("#FFC71585")]
        MediumVioletRed = 0xFFC71585,
        [Description("#FF191970")]
        MidnightBlue = 0xFF191970,
        [Description("#FFF5FFFA")]
        MintCream = 0xFFF5FFFA,
        [Description("#FFFFE4E1")]
        MistyRose = 0xFFFFE4E1,
        [Description("#FFFFE4B5")]
        Moccasin = 0xFFFFE4B5,
        [Description("#FFFFDEAD")]
        NavajoWhite = 0xFFFFDEAD,
        [Description("#FF000080")]
        Navy = 0xFF000080,
        [Description("#FFFDF5E6")]
        OldLace = 0xFFFDF5E6,
        [Description("#FF808000")]
        Olive = 0xFF808000,
        [Description("#FF6B8E23")]
        OliveDrab = 0xFF6B8E23,
        [Description("#FFFFA500")]
        Orange = 0xFFFFA500,
        [Description("#FFFF4500")]
        OrangeRed = 0xFFFF4500,
        [Description("#FFDA70D6")]
        Orchid = 0xFFDA70D6,
        [Description("#FFEEE8AA")]
        PaleGoldenRod = 0xFFEEE8AA,
        [Description("#FF98FB98")]
        PaleGreen = 0xFF98FB98,
        [Description("#FFAFEEEE")]
        PaleTurquoise = 0xFFAFEEEE,
        [Description("#FFDB7093")]
        PaleVioletRed = 0xFFDB7093,
        [Description("#FFFFEFD5")]
        PapayaWhip = 0xFFFFEFD5,
        [Description("#FFFFDAB9")]
        PeachPuff = 0xFFFFDAB9,
        [Description("#FFCD853F")]
        Peru = 0xFFCD853F,
        [Description("#FFFFC0CB")]
        Pink = 0xFFFFC0CB,
        [Description("#FFDDA0DD")]
        Plum = 0xFFDDA0DD,
        [Description("#FFB0E0E6")]
        PowderBlue = 0xFFB0E0E6,
        [Description("#FF800080")]
        Purple = 0xFF800080,
        [Description("#FF663399")]
        RebeccaPurple = 0xFF663399,
        [Description("#FFFF0000")]
        Red = 0xFFFF0000,
        [Description("#FFBC8F8F")]
        RosyBrown = 0xFFBC8F8F,
        [Description("#FF4169E1")]
        RoyalBlue = 0xFF4169E1,
        [Description("#FF8B4513")]
        SaddleBrown = 0xFF8B4513,
        [Description("#FFFA8072")]
        Salmon = 0xFFFA8072,
        [Description("#FFF4A460")]
        SandyBrown = 0xFFF4A460,
        [Description("#FF2E8B57")]
        SeaGreen = 0xFF2E8B57,
        [Description("#FFFFF5EE")]
        SeaShell = 0xFFFFF5EE,
        [Description("#FFA0522D")]
        Sienna = 0xFFA0522D,
        [Description("#FFC0C0C0")]
        Silver = 0xFFC0C0C0,
        [Description("#FF87CEEB")]
        SkyBlue = 0xFF87CEEB,
        [Description("#FF6A5ACD")]
        SlateBlue = 0xFF6A5ACD,
        [Description("#FF708090")]
        SlateGray = 0xFF708090,
        [Description("#FFFFFAFA")]
        Snow = 0xFFFFFAFA,
        [Description("#FF00FF7F")]
        SpringGreen = 0xFF00FF7F,
        [Description("#FF4682B4")]
        SteelBlue = 0xFF4682B4,
        [Description("#FFD2B48C")]
        Tan = 0xFFD2B48C,
        [Description("#FF008080")]
        Teal = 0xFF008080,
        [Description("#FFD8BFD8")]
        Thistle = 0xFFD8BFD8,
        [Description("#FFFF6347")]
        Tomato = 0xFFFF6347,
        [Description("#00000000")]
        Transparent,
        [Description("#FF40E0D0")]
        Turquoise = 0xFF40E0D0,
        [Description("#FFEE82EE")]
        Violet = 0xFFEE82EE,
        [Description("#FFF5DEB3")]
        Wheat = 0xFFF5DEB3,
        [Description("#FFFFFFFF")]
        White = 0xFFFFFFFF,
        [Description("#FFF5F5F5")]
        WhiteSmoke = 0xFFF5F5F5,
        [Description("#FFFFFF00")]
        Yellow = 0xFFFFFF00,
        [Description("#FF9ACD32")]
        YellowGreen = 0xFF9ACD32,

        #endregion

        #region Method Custom Colors

        [Description("#FFE9573F")]
        BitterSweet = 0xFFE9573F,
        [Description("#FF4A89DC")]
        BlueJeans = 0xFF4A89DC,
        [Description("#FF0247FE")]
        BlueRYB = 0xFF0247FE,
        [Description("#FF202B30")]
        Dark = 0xFF202B30,
        [Description("#FF1F262A")]
        DarkGunmetal = 0xFF1F262A,
        [Description("#FFDA4453")]
        Grapefruit = 0xFFDA4453,
        [Description("#FF8CC152")]
        Grass = 0xFF8CC152,
        [Description("#FF0088CC")]
        HelloSky = 0xFF0088CC,
        [Description("#FFBD362F")]
        HotMayDay = 0xFFBD362F,
        [Description("#FFAAB2BD")]
        MediumGray = 0xFFAAB2BD,
        [Description("#FF37BC9B")]
        Mint = 0xFF37BC9B,
        [Description("#FFD770AD")]
        PinkRose = 0xFFD770AD,
        [Description("#FF26133C")]
        RaisinBlack = 0xFF26133C,
        [Description("#FF0044CC")]
        RichVein = 0xFF0044CC,
        [Description("#FFF5F5F5")]
        Smoke = 0xFFF5F5F5,
        [Description("#FF989898")]
        SpanishGray = 0xFF989898,
        [Description("#FFF6BB42")]
        Sunflower = 0xFFF6BB42,

        #endregion
    }
}
