﻿using System;
using System.IO;

namespace HiVE.BasicModels.Helpers
{
    public class DateTimeFormats
    {
        public const string ISO_DATETIME_FORMAT = "yyyy-MM-ddTHH^mm^ss";
        public const string ISO_DATETIME_FORMAT_Post = "yyyy-MM-ddTHH:mm:ss";
        public const string ISO_DATETIME_FORMAT_Show = "yyyy-MM-dd HH:mm:ss";
        public const string ISO_DATE_FORMAT_Show = "yyyy-MM-dd";
        public const string ISO_DATE_FORMAT_ShowShort = "yyyy-MM-dd hh:mm tt";
    }

    public class Logging
    {
        public static void logActoin(string txt, bool showTime)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            if (showTime == true)
                Console.WriteLine(System.DateTime.Now.ToString() + ":" + txt);
            else
                Console.WriteLine(txt);
        }

        public static void logPrint(string txt)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(System.DateTime.Now.ToString() + ":" + txt);
        }

        public static void logErrorForUser(string txt, bool showTime)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            if (showTime == true)
                Console.WriteLine(System.DateTime.Now.ToString() + ":" + txt);
            else
                Console.WriteLine(txt);
        }

        public static void logError(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            string txt = string.Empty;
            txt += ex.Message;
            if (ex.InnerException != null)
                txt += "\nInnerException:" + ex.InnerException.Message;

            using (StreamWriter writer = new StreamWriter(new FileStream("Errors.txt", FileMode.Append)))
            {
                writer.WriteLine("{0}{1}{2}{3}", "Error ", DateTime.Now, " ", txt);
            }

            /// logActoin to file
            //Console.WriteLine(System.DateTime.Now.ToString() + ":" + txt);
        }

        public static void logEmail(string txt)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(System.DateTime.Now.ToString() + ":" + txt);
        }
    }
}
