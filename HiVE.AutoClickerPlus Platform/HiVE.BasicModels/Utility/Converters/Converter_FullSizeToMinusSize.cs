﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_FullSizeToMinusSize : IMultiValueConverter
    {
        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            double returnValue = 0;

            try
            {
                /// Always test MultiValueConverter inputs for non-null
                /// (to avoid crash bugs for views in the designer)
                if (values[0] is double && values[1] is double)
                {
                    double fullSize = (double)values[0];
                    double minusSize = (double)values[1];

                    returnValue = fullSize - minusSize;

                    if (returnValue < 0) { returnValue = 0; }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
