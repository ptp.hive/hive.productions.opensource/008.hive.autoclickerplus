﻿using HiVE.BasicModels.Animations.Animators;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace HiVE.BasicModels.Animations.Panels
{
    public class AnimatedWrapPanel : WrapPanel
    {
        private DispatcherTimer animationTimer;
        private DateTime lastArrange = DateTime.MinValue;

        public IArrangeAnimator Animator { get; set; }

        public AnimatedWrapPanel()
            : this(new FractionDistanceAnimator(0.4), TimeSpan.FromSeconds(0.01))
        {
        }

        public AnimatedWrapPanel(IArrangeAnimator animator, TimeSpan animationInterval)
        {
            animationTimer = AnimationBase.CreateAnimationTimer(this, animationInterval);
            Animator = animator;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            finalSize = base.ArrangeOverride(finalSize);

            AnimationBase.Arrange(Math.Max(0, (DateTime.Now - lastArrange).TotalSeconds), this, Children, Animator);
            lastArrange = DateTime.Now;

            return finalSize;
        }
    }
}
