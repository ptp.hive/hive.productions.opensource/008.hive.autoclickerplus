﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace HiVE.BasicModels.PageTransitions
{
    public class FrameTransition : Frame
    {
        public FrameTransition()
            : base()
        {
            // watch for navigations
            Navigating += OnNavigating;
        }

        #region Feilds

        private bool _allowDirectNavigation = false;
        private ContentPresenter _contentPresenter = null;
        private NavigatingCancelEventArgs _navArgs = null;
        private TransitionType _currentTransitionType;

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty TransitionTypeProperty =
            DependencyProperty.Register(
                "TransitionType",
                typeof(TransitionType),
                typeof(FrameTransition),
                new PropertyMetadata(TransitionType.Fade));

        public TransitionType TransitionType
        {
            get
            {
                return (TransitionType)GetValue(TransitionTypeProperty);
            }
            set
            {
                SetValue(TransitionTypeProperty, value);
            }
        }

        #endregion

        #region Methods

        public override void OnApplyTemplate()
        {
            try
            {
                // get a reference to the frame's content presenter
                // this is the element we will fade in and out
                _contentPresenter = GetTemplateChild("PART_FrameCP") as ContentPresenter;
                base.OnApplyTemplate();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        protected void OnNavigating(object sender, NavigatingCancelEventArgs e)
        {
            try
            {
                // if we did not internally initiate the navigation:
                //   1. cancel the navigation,
                //   2. cache the target,
                //   3. disable hittesting during the fade, and
                //   4. fade out the current content
                if (Content != null && !_allowDirectNavigation && _contentPresenter != null)
                {
                    e.Cancel = true;
                    _navArgs = e;
                    _contentPresenter.IsHitTestVisible = false;

                    _currentTransitionType = TransitionType;

                    if (_currentTransitionType == TransitionType.Default)
                    {
                        _currentTransitionType = BasicMethods.CurrentMainSettings.BasicDefaultTransitionType;

                        if (_currentTransitionType == TransitionType.Default)
                        {
                            _currentTransitionType = TransitionType.Fade;
                        }
                    }

                    Storyboard hidePage = (Application.Current.FindResource(string.Format("{0}Out", _currentTransitionType.ToString())) as Storyboard).Clone();

                    hidePage.Completed += FadeOutCompleted;

                    hidePage.Begin(_contentPresenter);
                }
                _allowDirectNavigation = false;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void FadeOutCompleted(object sender, EventArgs e)
        {
            try
            {
                // after the fade out
                //   1. re-enable hittesting
                //   2. initiate the delayed navigation
                //   3. invoke the FadeIn animation at Loaded priority

                if ((sender as ClockGroup) != null)
                { (sender as ClockGroup).Completed -= FadeOutCompleted; }

                if (_contentPresenter != null)
                {
                    _contentPresenter.IsHitTestVisible = true;

                    _allowDirectNavigation = true;
                    switch (_navArgs.NavigationMode)
                    {
                        case NavigationMode.New:
                            if (_navArgs.Uri == null)
                            {
                                NavigationService.Navigate(_navArgs.Content);
                            }
                            else
                            {
                                NavigationService.Navigate(_navArgs.Uri);
                            }
                            break;

                        case NavigationMode.Back:
                            NavigationService.GoBack();
                            break;

                        case NavigationMode.Forward:
                            NavigationService.GoForward();
                            break;

                        case NavigationMode.Refresh:
                            NavigationService.Refresh();
                            break;
                    }

                    Dispatcher.BeginInvoke(DispatcherPriority.Loaded,
                        (ThreadStart)delegate ()
                        {
                            _currentTransitionType = TransitionType;

                            if (_currentTransitionType == TransitionType.Default)
                            {
                                _currentTransitionType = BasicMethods.CurrentMainSettings.BasicDefaultTransitionType;

                                if (_currentTransitionType == TransitionType.Default)
                                {
                                    _currentTransitionType = TransitionType.Fade;
                                }
                            }

                            Storyboard showNewPage = (Application.Current.FindResource(string.Format("{0}In", _currentTransitionType.ToString())) as Storyboard).Clone();

                            showNewPage.Begin(_contentPresenter);
                        });
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion
    }
}
