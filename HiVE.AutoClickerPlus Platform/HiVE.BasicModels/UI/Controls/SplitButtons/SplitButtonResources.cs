﻿using System.Windows;

namespace HiVE.BasicModels.UI.Controls.SplitButtons
{
    /// <summary>
    /// Class used for the ComponentResourceKey
    /// </summary>
    public class SplitButtonResources
    {
        public static ComponentResourceKey VistaSplitButtonStyleKey
        {
            get { return new ComponentResourceKey(typeof(SplitButtonResources), "vistaSplitButtonStyle"); }
        }
    }
}
