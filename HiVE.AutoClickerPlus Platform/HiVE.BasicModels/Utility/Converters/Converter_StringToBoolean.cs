﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_StringToBoolean : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            bool? returnValue = null;

            try
            {
                if (value != null)
                {
                    switch (((string)value).ToUpper())
                    {
                        case ("1"):
                        case ("TRUE"):
                            {
                                returnValue = true;
                                break;
                            }
                        case ("0"):
                        case ("FALSE"):
                            {
                                returnValue = false;
                                break;
                            }
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                return System.Convert.ToBoolean(value);
            }
            catch (Exception)
            {
                return value;
            }
        }
    }
}
