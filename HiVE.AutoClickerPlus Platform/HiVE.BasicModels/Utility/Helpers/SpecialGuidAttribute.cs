﻿namespace System.ComponentModel
{
    using System;

    //[AttributeUsage(AttributeTargets.All)]
    //[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class SpecialGuidAttribute : Attribute
    {
        public static readonly SpecialGuidAttribute Default = new SpecialGuidAttribute();
        private string specialGuid;

        public SpecialGuidAttribute() : this(string.Empty)
        {
        }

        public SpecialGuidAttribute(string especialGuid)
        {
            this.specialGuid = especialGuid;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
            {
                return true;
            }
            SpecialGuidAttribute attribute = obj as SpecialGuidAttribute;
            return ((attribute != null) && (attribute.SpecialGuid == this.SpecialGuid));
        }

        public override int GetHashCode()
        {
            return this.SpecialGuid.GetHashCode();
        }

        public override bool IsDefaultAttribute()
        {
            return this.Equals(Default);
        }

        public virtual string SpecialGuid
        {
            get
            {
                return this.SpecialGuidValue;
            }
        }

        protected string SpecialGuidValue
        {
            get
            {
                return this.specialGuid;
            }
            set
            {
                this.specialGuid = value;
            }
        }
    }
}