﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace HiVE.BasicModels.MetroNumericInputs
{
    /// <summary>
    /// Interaction logic for MetroNumericInput.xaml
    /// </summary>
    public partial class MetroNumericInput : UserControl
    {
        public MetroNumericInput()
        {
            InitializeComponent();
        }

        #region Feilds

        private const char DecimalSeperator = '.';
        private const int MaxResultValueLength = 10;
        private bool IsCancelAppendResultValue { get; set; } = false;

        #endregion

        #region Dependency Properties

        #region ResultValue

        /// <summary>
        /// Dependency Object for the value of the MetroNumericInput Control
        /// </summary>
        public static readonly DependencyProperty ResultValueProperty =
            DependencyProperty.Register(
                "ResultValue",
                typeof(string),
                typeof(MetroNumericInput),
                new FrameworkPropertyMetadata(string.Empty, OnResultValueChanged));

        /// <summary>
        /// Gets / Sets the ResultValue that the control is showing
        /// </summary>
        public string ResultValue
        {
            get
            {
                return (string)GetValue(ResultValueProperty);
            }
            set
            {
                SetValue(ResultValueProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the ResultValue property.
        /// </summary>
        /// <param name="d">MetroNumericInput</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnResultValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroNumericInput = (MetroNumericInput)d;
            var oldResultValue = (string)e.OldValue;
            var newResultValue = MetroNumericInput.ResultValue;
            MetroNumericInput.OnResultValueChanged(oldResultValue, newResultValue);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ResultValue property.
        /// </summary>
        /// <param name="oldResultValue">Old Value</param>
        /// <param name="newResultValue">New Value</param>
        protected virtual void OnResultValueChanged(string oldResultValue, string newResultValue)
        {
            try
            {
                RefreshResultValue(oldResultValue, newResultValue);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region FormatStringValue

        /// <summary>
        /// Dependency Object for the value of the DialerPlusUC Control
        /// </summary>
        public static readonly DependencyProperty FormatStringValueProperty =
            DependencyProperty.Register(
                "FormatStringValue",
                typeof(string),
                typeof(MetroNumericInput),
                new FrameworkPropertyMetadata("N2"));

        /// <summary>
        /// Gets / Sets the value that the control is showing
        /// </summary>
        public string FormatStringValue
        {
            get
            {
                return (string)GetValue(FormatStringValueProperty);
            }
            set
            {
                SetValue(FormatStringValueProperty, value);
            }
        }

        #endregion

        #region Increment

        /// <summary>
        /// Dependency Object for the value of the MetroNumericInput Control
        /// </summary>
        public static readonly DependencyProperty IncrementProperty =
            DependencyProperty.Register(
                "Increment",
                typeof(decimal),
                typeof(MetroNumericInput),
                new FrameworkPropertyMetadata(decimal.Parse("1")));

        /// <summary>
        /// Gets / Sets the value that the control is showing
        /// </summary>
        public decimal Increment
        {
            get
            {
                return (decimal)GetValue(IncrementProperty);
            }
            set
            {
                SetValue(IncrementProperty, value);
            }
        }

        #endregion

        #region IsDecimal

        /// <summary>
        /// Dependency Object for the value of the MetroNumericInput Control
        /// </summary>
        public static readonly DependencyProperty IsDecimalProperty =
            DependencyProperty.Register(
                "IsDecimal",
                typeof(bool),
                typeof(MetroNumericInput),
                new FrameworkPropertyMetadata(true));

        /// <summary>
        /// Gets / Sets the value that the control is showing
        /// </summary>
        public bool IsDecimal
        {
            get
            {
                return (bool)GetValue(IsDecimalProperty);
            }
            set
            {
                SetValue(IsDecimalProperty, value);
            }
        }

        #endregion

        #endregion

        #region NumericPanel

        private void metroButtonNumeric_0_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (0).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (1).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (2).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (3).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (4).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_5_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (5).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_6_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (6).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_7_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (7).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_8_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (8).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_9_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (9).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_Back_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ResultValue.Length > 0)
                { ResultValue = ResultValue.Remove(ResultValue.Length - 1, 1); }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_Clear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = string.Empty;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void metroButtonNumeric_DecimalSeperator_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = ResultValue.Insert(ResultValue.Length, (DecimalSeperator).ToString());
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refresh ResultValue
        /// </summary>
        /// <param name="oldResultValue"></param>
        /// <param name="newResultValue"></param>
        public void RefreshResultValue(string oldResultValue, string newResultValue)
        {
            try
            {
                if (!IsCancelAppendResultValue)
                {
                    /// Check for just one decimal seperator
                    if (newResultValue.Length > 0
                        && newResultValue.Where(i => i == DecimalSeperator).Count() > 1)
                    {
                        IsCancelAppendResultValue = true;

                        ResultValue = oldResultValue;

                        IsCancelAppendResultValue = false;
                    }

                    /// Check for is have decimal seperator and special Decimal Digits
                    if (newResultValue.Length > 0
                        && newResultValue.Where(i => i == DecimalSeperator).Count() == 1)
                    {
                        var splitResultValue = newResultValue.Split(DecimalSeperator);

                        if (splitResultValue != null && splitResultValue.Count() == 2)
                        {
                            if (splitResultValue[1].Count() > BasicMethods.CurrentMainSettings.DecimalDigits)
                            {
                                IsCancelAppendResultValue = true;

                                ResultValue = oldResultValue;

                                IsCancelAppendResultValue = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void buttonMines_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((decimal.Parse(ResultValue) - Increment) > 0)
                {
                    ResultValue = (decimal.Parse(ResultValue) - Increment).ToString();
                }
                else
                {
                    ResultValue = (0).ToString();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        private void buttonPlus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultValue = (decimal.Parse(ResultValue) + Increment).ToString();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        /// <summary>
        /// Load this user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroNumericInput_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
