﻿using System;
using static HiVE.BasicModels.BoxItems.BoxSpecialMethods;

namespace HiVE.BasicModels.BoxItems
{
    public class BoxInformationDetails
    {
        public string Caption { get; set; } = string.Empty;
        public string MessageInformation { get; set; } = string.Empty;
        public string ResultDescription { get; set; } = string.Empty;
        public string Result { get; set; } = string.Empty;
        public string ButtonFinalResult { get; set; } = string.Empty;
        public string ButtonFailureResult { get; set; } = string.Empty;
        public bool IsToUpper { get; set; } = true;
        public bool IsSuccessfullyInfo { get; set; } = true;
        public MethodContentPageSenderType ContentPageSenderTypeSource { get; set; }
        public Uri GoBackSource { get; set; }
    }
}
