﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace HiVE.BasicModels.AppActivityTimers
{
    public class AppActivityTimer
    {
        #region Events - OnActive, OnInactive, OnTimePassed

        public event System.Windows.Input.PreProcessInputEventHandler OnActive;
        public event EventHandler OnInactive;
        public event EventHandler OnTimePassed;

        #endregion

        #region TimePassed

        /// <summary>
        /// Time in milliseconds to fire the OnTimePassed event.
        /// Plain timer, for example going off every 30 secs
        /// Default is TimeSpan.FromMilliseconds(30 * 1000);
        /// </summary>
        public TimeSpan TimePassedThreshold { get; private set; } = TimeSpan.FromMilliseconds(30 * 1000);

        private DispatcherTimer _timePassedTimer;

        #endregion

        #region Inactivity

        /// <summary>
        /// Time in milliseconds to be idle before firing the OnInactivity event.
        /// How long to wait for no activity before firing OnInactive event - for example 5 minutes
        /// Default is TimeSpan.FromMilliseconds(5 * 60 * 1000);
        /// </summary>
        public TimeSpan InactivityThreshold { get; private set; } = TimeSpan.FromMilliseconds(5 * 60 * 1000);

        private DispatcherTimer _inactivityTimer;
        private Point _inactiveMousePosition = new Point(0, 0);

        /// <summary>
        /// Does a change in mouse position count as activity?
        /// Does mouse movement count as activity?
        /// Default is True;
        /// </summary>
        public bool IsWillMonitorMousePosition { get; private set; } = true;

        #endregion

        #region Constructor

        /// <summary>
        /// Timers for activity, inactivity and time passed.
        /// </summary>
        /// <param name="TimePassedInMS">Time in milliseconds to fire the OnTimePassed event.</param>
        /// <param name="IdleTimeInMS">Time in milliseconds to be idle before firing the OnInactivity event.</param>
        /// <param name="WillMonitorMousePosition">Does a change in mouse position count as activity?</param>
        public AppActivityTimer(int TimePassedInMS, int IdleTimeInMS, bool WillMonitorMousePosition)
        {
            try
            {
                IsWillMonitorMousePosition = WillMonitorMousePosition;
                System.Windows.Input.InputManager.Current.PreProcessInput += new System.Windows.Input.PreProcessInputEventHandler(OnActivity);

                /// Time Passed Timer
                _timePassedTimer = new DispatcherTimer();
                TimePassedThreshold = TimeSpan.FromMilliseconds(TimePassedInMS);
                /// Start the time passed timer
                _timePassedTimer.Tick += new EventHandler(OnTimePassedHandler);
                _timePassedTimer.Interval = TimePassedThreshold;
                _timePassedTimer.IsEnabled = true;

                /// Inactivity Timer
                _inactivityTimer = new DispatcherTimer();
                InactivityThreshold = TimeSpan.FromMilliseconds(IdleTimeInMS);
                /// Start the inactivity timer
                _inactivityTimer.Tick += new EventHandler(OnInactivity);
                _inactivityTimer.Interval = InactivityThreshold;
                _inactivityTimer.IsEnabled = true;
            }
            catch (Exception) { }
        }

        #endregion

        #region OnActivity

        /// <summary>
        /// Activity detected (key press, mouse move, etc) - close your slide show, if it is open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnActivity(object sender, System.Windows.Input.PreProcessInputEventArgs e)
        {
            try
            {
                System.Windows.Input.InputEventArgs inputEventArgs = e.StagingItem.Input;
                if (inputEventArgs is System.Windows.Input.MouseEventArgs || inputEventArgs is System.Windows.Input.KeyboardEventArgs)
                {
                    if (inputEventArgs is System.Windows.Input.MouseEventArgs)
                    {
                        System.Windows.Input.MouseEventArgs mea = inputEventArgs as System.Windows.Input.MouseEventArgs;
                        /// no button is pressed and the position is still the same as the application became inactive
                        if (mea.LeftButton == System.Windows.Input.MouseButtonState.Released &&
                            mea.RightButton == System.Windows.Input.MouseButtonState.Released &&
                            mea.MiddleButton == System.Windows.Input.MouseButtonState.Released &&
                            mea.XButton1 == System.Windows.Input.MouseButtonState.Released &&
                            mea.XButton2 == System.Windows.Input.MouseButtonState.Released &&
                            (IsWillMonitorMousePosition == false ||
                                (IsWillMonitorMousePosition == true && _inactiveMousePosition == mea.GetPosition(Application.Current.MainWindow)))
                            )
                            return;
                    }

                    /// Reset idle timer
                    _inactivityTimer.IsEnabled = false;
                    _inactivityTimer.Stop();
                    _inactivityTimer.IsEnabled = true;
                    _inactivityTimer.Start();
                    if (OnActive != null)
                        OnActive(sender, e);
                }
            }
            catch (Exception) { }
        }

        #endregion

        #region OnInactivity

        /// <summary>
        /// App is inactive - activate your slide show - new full screen window or whatever
        /// FYI - The last input was at:
        /// DateTime idleStartTime = DateTime.Now.Subtract(AppActivityTimer.InactivityThreshold);
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnInactivity(object sender, EventArgs e)
        {
            try
            {
                /// Fires when app has gone idle
                _inactiveMousePosition = System.Windows.Input.Mouse.GetPosition(Application.Current.MainWindow);
                _inactivityTimer.Stop();
                if (OnInactive != null)
                    OnInactive(sender, e);
            }
            catch (Exception) { }
        }

        #endregion

        #region OnTimePassedHandler

        /// <summary>
        /// Regular timer went off
        /// DateTime timePassedInMS = DateTime.Now.Subtract(AppActivityTimer.TimePassedThreshold);
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTimePassedHandler(object sender, EventArgs e)
        {
            try
            {
                if (OnTimePassed != null)
                    OnTimePassed(sender, e);
            }
            catch (Exception) { }
        }

        #endregion
    }
}
