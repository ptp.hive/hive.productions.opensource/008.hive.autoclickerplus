﻿using HiVE.AutoClickerPlus.Utility.Helpers;
using HiVE.BasicModels.AppActivityTimers;
using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace HiVE.AutoClickerPlus
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        public MainPage()
        {
            SpecialModels.GetMainSettings();
            Currents.CurrentInformationDetails = new BoxInformationDetails();

            SetAppActivityTimerDetails();

            InitializeComponent();
            Currents.CurrentFrameTransition = this.frameTransitionControl;

            DispatcherTimerInProgress_LME_PTPHiVEPOSEntities = new DispatcherTimer();
        }

        #region AppActivityTimer Methods

        /// <summary>
        /// In app, this is a member variable of the main window
        /// </summary>
        private AppActivityTimer AppActivityTimer;

        /// <summary>
        /// Set AppActivityTimer Details for this application
        /// </summary>
        private void SetAppActivityTimerDetails()
        {
            try
            {
                AppActivityTimer = new AppActivityTimer(
                    /// Time in milliseconds to fire the OnTimePassed event.
                    /// Plain timer, for example going off every 30 secs
                    /// Default is TimeSpan.FromMilliseconds(30 * 1000);
                    Currents.CurrentMainSettings.TimePassedThreshold,

                    /// Time in milliseconds to be idle before firing the OnInactivity event.
                    /// How long to wait for no activity before firing OnInactive event - for example 5 minutes
                    /// Default is TimeSpan.FromMilliseconds(5 * 60 * 1000);
                    Currents.CurrentMainSettings.InactivityThreshold,

                    /// Does a change in mouse position count as activity?
                    /// Does mouse movement count as activity?
                    /// Default is True;
                    Currents.CurrentMainSettings.IsWillMonitorMousePosition);

                AppActivityTimer.OnInactive += new EventHandler(AppActivityTimer_OnInactive);
                AppActivityTimer.OnActive += new PreProcessInputEventHandler(AppActivityTimer_OnActive);
                AppActivityTimer.OnTimePassed += new EventHandler(AppActivityTimer_OnTimePassed);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set AppActivityTimerUC Details for this application
        /// </summary>
        private void SetAppActivityTimerUCDetails()
        {
            try
            {
                appActivityTimerUC.SetAppActivityTimerUCDetails(
                    /// Gets / Sets the TimePassedThreshold that the control is showing
                    /// Time in milliseconds to fire the OnTimePassed event.
                    /// Plain timer, for example going off every 30 secs
                    /// Default is TimeSpan.FromMilliseconds(30 * 1000);
                    Currents.CurrentMainSettings.TimePassedThreshold,

                    /// Gets / Sets the NavigateUri that the control is showing
                    new Uri("UI/Pages/StartPage.xaml", UriKind.Relative));
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Regular timer went off
        /// DateTime timePassedInMS = DateTime.Now.Subtract(AppActivityTimer.TimePassedThreshold);
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AppActivityTimer_OnTimePassed(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Activity detected (key press, mouse move, etc) - close your slide show, if it is open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AppActivityTimer_OnActive(object sender, PreProcessInputEventArgs e)
        {
            try
            {
                appActivityTimerUC.StopCountdown();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// App is inactive - activate your slide show - new full screen window or whatever
        /// FYI - The last input was at:
        /// DateTime idleStartTime = DateTime.Now.Subtract(AppActivityTimer.InactivityThreshold);
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AppActivityTimer_OnInactive(object sender, EventArgs e)
        {
            try
            {
                if (!appActivityTimerUC.IsStartAppActivityPage)
                {
                    appActivityTimerUC.StartCountdown();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region MultiThreaded Functions - Refresh InProgress LME_PTPHiVEPOSEntities

        #region Feilds - InProgressLME_PTPHiVEPOSEntities

        /// <summary>
        /// Get/Set status of timer
        /// If Reset is True, then go to default value for feilds and methods this timer
        /// </summary>
        private bool StatusInProgress_LME_PTPHiVEPOSEntities_IsReset = false;
        /// <summary>
        /// Main DispatcherTimer for this MultiThreaded Functions to check stopwatch time in special tick
        /// </summary>
        private DispatcherTimer DispatcherTimerInProgress_LME_PTPHiVEPOSEntities { get; set; }

        /// <summary>
        /// Main thread for refresh InProgressLME_PTPHiVEPOSEntities Functions
        /// </summary>
        private Thread threadTimerInProgress_LME_PTPHiVEPOSEntities;
        /// <summary>
        /// Main thread start for refresh InProgressLME_PTPHiVEPOSEntities Functions
        /// </summary>
        private ThreadStart threadStartTimerInProgress_LME_PTPHiVEPOSEntities;

        /// <summary>
        /// Need to get this objects from server and then set to Dependency Propertie of another thread in Dispatcher.Invoke state
        /// </summary>
        //private LME_PTPHiVEPOSEntities InProgressLME_PTPHiVEPOSEntities { get; set; }

        #endregion

        #region NextAllowedRunFunction Methods - InProgressLME_PTPHiVEPOSEntities

        /// <summary>
        /// No allowed when properties and fields use in another method!
        /// </summary>
        public bool IsUsedPropertiesDetailsInProgress_LME_PTPHiVEPOSEntities { get; set; } = false;
        /// <summary>
        /// TimePassed NextAllowedRunFunction datetime Threshold
        /// </summary>
        public DateTime? NextAllowedRunFunction_RefreshInProgressLME_PTPHiVEPOSEntities;

        /// <summary>
        /// IsNext Allowed RunFunction limited by TimePassed NextAllowedRunFunction datetime Threshold
        /// Also if IsUsedPropertiesDetailsInProgress_LME_PTPHiVEPOSEntities==False
        /// </summary>
        /// <returns></returns>
        private bool IsNextAllowedRunFunction_RefreshInProgressLME_PTPHiVEPOSEntities()
        {
            try
            {
                if ((NextAllowedRunFunction_RefreshInProgressLME_PTPHiVEPOSEntities != null && DateTime.UtcNow < NextAllowedRunFunction_RefreshInProgressLME_PTPHiVEPOSEntities)
                    || (IsUsedPropertiesDetailsInProgress_LME_PTPHiVEPOSEntities))
                {
                    return false;
                }
                NextAllowedRunFunction_RefreshInProgressLME_PTPHiVEPOSEntities =
                    DateTime.UtcNow +
                    TimeSpan.FromMilliseconds(Currents.CurrentMainSettings.TimePassedNextAllowedRunFunctionThreshold_RefreshInProgressLME_PTPHiVEPOSEntities);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return true;
        }

        #endregion

        #region Status - InProgressLME_PTPHiVEPOSEntities

        /// <summary>
        /// Set Default values for InProgressLME_PTPHiVEPOSEntities feilds and functions of this  timer 
        /// </summary>
        private void TimerResetInProgress_LME_PTPHiVEPOSEntities()
        {
            try
            {
                NextAllowedRunFunction_RefreshInProgressLME_PTPHiVEPOSEntities = null;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Start this timer
        /// </summary>
        private void TimerStartInProgress_LME_PTPHiVEPOSEntities()
        {
            try
            {
                if (StatusInProgress_LME_PTPHiVEPOSEntities_IsReset)
                {
                    TimerResetInProgress_LME_PTPHiVEPOSEntities();
                    StatusInProgress_LME_PTPHiVEPOSEntities_IsReset = false;
                }

                DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.IsEnabled = true;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Counter of this timer
        /// Check state in special period and do something if IsNextAllowedRunFunction_RefreshInProgressLME_PTPHiVEPOSEntities==True
        /// Update InProgressLME_PTPHiVEPOSEntities from server in special thread of main application
        /// </summary>
        private void TimerCounterInProgress_LME_PTPHiVEPOSEntities()
        {
            try
            {
                if (!IsNextAllowedRunFunction_RefreshInProgressLME_PTPHiVEPOSEntities()) { return; }

                //RefreshListBoxInProgressLME_PTPHiVEPOSEntities(true);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Do something works in a period in special thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DispatcherTimerInProgress_LME_PTPHiVEPOSEntities_Tick(object sender, EventArgs e)
        {
            try
            {
                threadStartTimerInProgress_LME_PTPHiVEPOSEntities = new ThreadStart(TimerCounterInProgress_LME_PTPHiVEPOSEntities);
                threadTimerInProgress_LME_PTPHiVEPOSEntities = new Thread(threadStartTimerInProgress_LME_PTPHiVEPOSEntities);
                threadTimerInProgress_LME_PTPHiVEPOSEntities.Priority = ThreadPriority.AboveNormal;
                //
                threadTimerInProgress_LME_PTPHiVEPOSEntities.Start();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Stop this timer
        /// </summary>
        private void TimerStopInProgress_LME_PTPHiVEPOSEntities()
        {
            try
            {
                DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.IsEnabled = false;
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Lap this timer and get current status
        /// </summary>
        private void TimerLapInProgress_LME_PTPHiVEPOSEntities()
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    string stringStatusProcess = string.Empty;

                    if (DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.IsEnabled)
                    {
                        stringStatusProcess = "Started";
                    }
                    else
                    {
                        stringStatusProcess = "Stoped";
                    }
                }));
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region Functions - InProgressLME_PTPHiVEPOSEntities

        /// <summary>
        /// Set Details of MultiThreaded InProgressLME_PTPHiVEPOSEntities Functions
        /// </summary>
        /// <param name="IsForceStart"></param>
        private void SetMultiThreadedFunctionsInProgress_LME_PTPHiVEPOSEntitiesDetails(bool IsForceStart)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {

                }));

                DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.Interval =
                    TimeSpan.FromMilliseconds(Currents.CurrentMainSettings.TimePassedDispatcherTimerIntervalThreshold);

                DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.Tick += new EventHandler(this.DispatcherTimerInProgress_LME_PTPHiVEPOSEntities_Tick);

                DispatcherTimerInProgress_LME_PTPHiVEPOSEntities_EnabledChanged(DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.IsEnabled);
                StatusInProgress_LME_PTPHiVEPOSEntities_IsReset = true;

                if (IsForceStart)
                {
                    DispatcherTimerStartStopInProgress_LME_PTPHiVEPOSEntities();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Change enabled if timer and set special status for each state
        /// </summary>
        /// <param name="IsEnabledTimer"></param>
        /// <returns></returns>
        private bool DispatcherTimerInProgress_LME_PTPHiVEPOSEntities_EnabledChanged(bool IsEnabledTimer)
        {
            try
            {
                Dispatcher.Invoke(new Action(delegate
                {
                    //checkBoxMilliseconds.IsEnabled = !isEnabled;
                }));

                if (IsEnabledTimer)
                {
                    Dispatcher.Invoke(new Action(delegate
                    {
                        //buttonResetLap.Content = "Lap";
                    }));
                }
                else
                {
                    Dispatcher.Invoke(new Action(delegate
                    {
                        //buttonResetLap.Content = "Reset";
                    }));
                }
            }
            catch { return false; }

            return true;
        }

        /// <summary>
        /// Change Start/Stop InProgressLME_PTPHiVEPOSEntities
        /// </summary>
        private void DispatcherTimerStartStopInProgress_LME_PTPHiVEPOSEntities()
        {
            try
            {
                if (!DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.IsEnabled)
                {
                    TimerStartInProgress_LME_PTPHiVEPOSEntities();

                    DispatcherTimerInProgress_LME_PTPHiVEPOSEntities_EnabledChanged(DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.IsEnabled);
                    TimerLapInProgress_LME_PTPHiVEPOSEntities();
                }
                else
                {
                    TimerStopInProgress_LME_PTPHiVEPOSEntities();

                    DispatcherTimerInProgress_LME_PTPHiVEPOSEntities_EnabledChanged(DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.IsEnabled);
                    TimerLapInProgress_LME_PTPHiVEPOSEntities();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Reset/Lap InProgressLME_PTPHiVEPOSEntities
        /// </summary>
        private void DispatcherTimerResetLapInProgress_LME_PTPHiVEPOSEntities()
        {
            try
            {
                if (DispatcherTimerInProgress_LME_PTPHiVEPOSEntities.IsEnabled)
                {
                    TimerLapInProgress_LME_PTPHiVEPOSEntities();
                }
                else
                {
                    TimerResetInProgress_LME_PTPHiVEPOSEntities();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region Methods - InProgressLME_PTPHiVEPOSEntities
        
        #endregion

        #endregion

        #region MultiThreaded Functions Methods
        
        #endregion

        /// <summary>
        /// Load the main page of this application
        /// Application start page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainPage_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Currents.CurrentMainPage = this;

                SetMultiThreadedFunctionsInProgress_LME_PTPHiVEPOSEntitiesDetails(true);
                
                SetAppActivityTimerUCDetails();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Unload the main page of this application
        /// Application stop page
        /// Shutdown all children thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainPage_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DispatcherTimerStartStopInProgress_LME_PTPHiVEPOSEntities();
                this.Dispatcher.InvokeShutdown();
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
