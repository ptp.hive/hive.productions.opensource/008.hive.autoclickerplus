﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_ToDateTimeNow : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            DateTime returnValue = DateTime.Now;

            try
            {
                returnValue.ToString(DateTimeFormats.ISO_DATETIME_FORMAT_Show);
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from DateTimeNow back to Null");
        }
    }
}
