﻿using System.ComponentModel;

namespace HiVE.BasicModels.BoxItems
{
    /// <summary>
    /// BoxSpecialMethods for this [HiVE.AutoClickerPlus] PTPHiVE project.
    /// </summary>
    public static class BoxSpecialMethods
    {
        /// <summary>
        /// Get Special ProductInformation
        /// </summary>
        public enum MethodProductInformation
        {
            [SpecialDescription("HiVE.AutoClickerPlus")]
            [Description("HiVE.AutoClickerPlus")]
            ProductName,
            [SpecialDescription("http://www.HiVE.AutoClickerPlus.com/")]
            [Description("www.HiVE.AutoClickerPlus.com")]
            ProductWebSite,
            [SpecialDescription("http://HiVE.HiVE.AutoClickerPlus@gmail.com/")]
            [Description("HiVE.AutoClickerPlus@gmail.com")]
            ProductEmail,
            [SpecialDescription("http://www.PTPHiVE.com/HiVE.AutoClickerPlus/")]
            [Description("www.PTPHiVE.com/HiVE.AutoClickerPlus")]
            ProductLink,
        }

        /// <summary>
        /// Define our collection of list items details
        /// Used for DropDownControls
        /// </summary>
        public enum MethodNodeListItemDetails
        {
            Group,
            Value,
            Display,
            ToolTip
        }

        /// <summary>
        /// Get Default ContentType
        /// </summary>
        public enum MethodContentType
        {
            [SpecialGuid("c402da30-defb-4094-b886-2e8d04279492")]
            [SpecialDescription("Main Settings")]
            [Description("MainSettings.xml")]
            MainSettings = 0,

            [SpecialGuid("5dfe0eb3-cb5d-422f-802b-5a0570594265")]
            [SpecialDescription("GetInfo")]
            [Description("GetInfo.xml")]
            GetInfo,

            [SpecialGuid("8ac0b781-300a-422f-99a0-d79bdbb4e03b")]
            [SpecialDescription("Restauran Details")]
            [Description("Restauran.xml")]
            Restauran,

            [SpecialGuid("15c5ff85-bc42-4bb8-b60e-f712c36e8714")]
            [SpecialDescription("Menus")]
            [Description("Menus.xml")]
            Menus,

            [SpecialGuid("2236d739-de94-4ea4-bb88-02f206734cfd")]
            [SpecialDescription("Content Photos Paths")]
            [Description("ContentPhotosPaths.xml")]
            ContentPhotosPaths,
        }

        /// <summary>
        /// Get Special Status Type
        /// </summary>
        internal enum MethodStatusType : int
        {
            /// <summary>
            /// Just for Veiws
            /// </summary>
            [Description("IsFaild")]
            IsFaild = -1,

            [Description("Null")]
            Null = 0,
            [Description("IsActive")]
            IsActive = 1,
            [Description("IsDelete")]
            IsDeleted = 2,
            [Description("IsDisabled")]
            IsDisabled = 3,
            [Description("IsInactive")]
            IsInactive = 4,

            /// <summary>
            /// Just for Veiws
            /// </summary>
            [Description("IsNoFilter")]
            IsNoFilter = 101,
            /// <summary>
            /// Just for Veiws
            /// </summary>
            [Description("IsBack")]
            IsBack = 102,
        }

        /// <summary>
        /// Get Common Special Colors for menu sections
        /// </summary>
        public enum MethodMenuSectionsColors : uint
        {
            #region Menu Section Item United Colors

            [Description("#f3c500")]
            MenuSectionItem_01,
            [Description("#f3ba23")]
            MenuSectionItem_02,
            [Description("#eecb27")]
            MenuSectionItem_03,
            [Description("#f3a622")]
            MenuSectionItem_04,
            [Description("#e79146")]
            MenuSectionItem_05,

            #endregion

            #region Method United Colors

            //[Description("#AB002B00")]
            //None,                   /// Color.Empty
            //[Description("#FFBF5AFA")]
            //Default,                /// 0xFFFFFAFA,         /// Snow
            [Description("#FF0044CC")]
            Primary,                /// 0xFF0044CC,         /// RichVein
            [Description("#FF008000")]
            Success,                /// 0xFF008000,         /// Green
            [Description("#FF008B8B")]
            Info,                   /// 0xFF008B8B,         /// DarkCyan
            [Description("#FFFFD700")]
            Warning,                /// 0xFFFFD700,         /// Gold
            [Description("#FFDC143C")]
            Danger,                 /// 0xFFDC143C,         /// Crimson
            [Description("#FF9400D3")]
            Surprise,               /// 0xFF9400D3,         /// DarkViolet
            [Description("#FF202B30")]
            Inverse,                /// 0xFF202B30,         /// Dark
            [Description("#FFAAB2BD")]
            Normal,                 /// 0xFFAAB2BD,         /// MediumGray
            [Description("#FF1E90FF")]
            Link                    /// 0xFF1E90FF,         /// DodgerBlue          JustText

            #endregion
        }

        /// <summary>
        /// Get Content Page Sender Type
        /// </summary>
        public enum MethodContentPageSenderType
        {
            /// <summary>
            /// For hold order and send to server
            /// OrderDetailsPageUC
            /// </summary>
            SubmitOrderPageUC = 0,

            /// <summary>
            /// For Checkout and receipt order
            /// </summary>
            CheckOutPageUC,

            JoinPage,

            /// <summary>
            /// For remove OrderDetail CardItem
            /// </summary>
            OrderDetailsCardItemUC_RemoveItem,

            /// <summary>
            /// For remove Address Item from Customer Item
            /// </summary>
            CustomerAddressDetailsCardItemUC_RemoveAddressItem,
            /// <summary>
            /// For remove Customer Item
            /// </summary>
            CustomerAddressDetailsCardItemUC_RemoveCustomerItem,
        }

        /// <summary>
        /// Get Content Discount Sender Type
        /// </summary>
        public enum MethodContentDiscountSenderType
        {
            /// <summary>
            /// Just for refresh datas
            /// </summary>
            Null = 0,

            Order,
            OrderDetail,
            Receipt
        }

        /// <summary>
        /// Get Content User Sender Type
        /// </summary>
        public enum MethodContentClockUserSenderType
        {
            /// <summary>
            /// Just for refresh datas
            /// </summary>
            Null = 0,

            ClockInUsers,
        }

        /// <summary>
        /// Get Content Notification Sender Type
        /// </summary>
        public enum MethodContentNotificationSenderType
        {
            /// <summary>
            /// Just for refresh datas
            /// </summary>
            Null = 0,

            MainPage,
        }

        /// <summary>
        /// Get Content AddressPlus Sender Type
        /// </summary>
        public enum MethodContentAddressPlusSenderType
        {
            /// <summary>
            /// Just for refresh datas
            /// </summary>
            Null = 0,

            AddCustomer,
            AddAddress,
            EditAddress
        }

        /// <summary>
        /// Get Content Show Sender Type - OrderPage
        /// </summary>
        public enum ContentShowSenderType_OrderPage
        {
            OrderJoinPageUC = 0,
            OrderDetailsPageUC,
            OrderQuestionAnswerUC,
            CheckOutPageUC,

            DiscountSelectedItemsUC,

            SubmitOrderPageUC,
            InformationPageUC,
        }

        /// <summary>
        /// Get Content Show Sender Type - CheckOutPage
        /// </summary>
        public enum ContentShowSenderType_CheckOutPage
        {
            CheckOutPageUC = 0,
            DiscountSelectedItemsUC,
            InformationPageUC,
        }

        /// <summary>
        /// Get Content ReceiptPayment Sender Type
        /// </summary>
        public enum MethodContentReceiptPaymentSenderType
        {
            Null = 0,
            AmountWithTip,
            TipValue,
        }

        #region DataManagement

        /// <summary>
        /// Get ContentPhotosPaths for Import data from excel to main database
        /// </summary>
        public enum MethodContentPhotosPathsParameters
        {
            [SpecialDescription("None")]
            [Description("هیچ کدام")]
            None = 0,

            [SpecialDescription("Content Image Type")]
            [Description("نوع محتوای تصویر")]
            ContentImageType,

            [SpecialDescription("Content Image Name")]
            [Description("نام محتوای تصویر")]
            ContentImageName,

            [SpecialDescription("Content Photo Path")]
            [Description("مسیر حقیقی تصویر محتوا")]
            ContentPhotoPath,

            [SpecialDescription("Content Photo Name")]
            [Description("نام تصویر محتوا")]
            ContentPhotoName,

            [SpecialDescription("Content Ids")]
            [Description("شناسه محتوا‌ها")]
            ContentIds,

            [SpecialDescription("Content Name")]
            [Description("نام محتوا")]
            ContentName,
        }

        /// <summary>
        /// Get QuestionsCategories for Import data from excel to main database
        /// </summary>
        public enum MethodQuestionsCategoriesParameters
        {
            [SpecialDescription("None")]
            [Description("هیچ کدام")]
            None = 0,

            [SpecialDescription("Step Type")]
            [Description("نوع مرحله")]
            StepType,

            [SpecialDescription("Step Name")]
            [Description("نام مرحله")]
            StepName,

            [SpecialDescription("Step Description")]
            [Description("توضیحات مرحله")]
            StepDescription,

            [SpecialDescription("Question Id")]
            [Description("شنایه سوال")]
            QuestionId,

            [SpecialDescription("Question Name")]
            [Description("نام سوال")]
            QuestionName,

            [SpecialDescription("Question Answers Type")]
            [Description("نوع جواب‌های سوال")]
            QuestionAnswersType,

            [SpecialDescription("Sub Question Type")]
            [Description("نوع زیر سوال")]
            SubQuestionType,

            [SpecialDescription("Sub Question Name")]
            [Description("نام زیر سوال")]
            SubQuestionName,

            [SpecialDescription("Answer Ids")]
            [Description("شناسه جواب‌ها")]
            AnswerIds,

            [SpecialDescription("Answer Names")]
            [Description("نام جواب‌ها")]
            AnswerNames,
        }

        /// <summary>
        /// Get DefaultAnswersCategories for Import data from excel to main database
        /// </summary>
        public enum MethodDefaultAnswersCategoriesParameters
        {
            [SpecialDescription("None")]
            [Description("هیچ کدام")]
            None = 0,

            [SpecialDescription("Food Id")]
            [Description("شناسه غذا")]
            FoodId,

            [SpecialDescription("Food Name")]
            [Description("نام غذا")]
            FoodName,

            [SpecialDescription("FoodSize Ids")]
            [Description("شناسه‌های سایز‌ها")]
            FoodSizeIds,

            [SpecialDescription("FoodSize Names")]
            [Description("نام‌های سایز‌ها")]
            FoodSizeNames,

            [SpecialDescription("Answer Ids")]
            [Description("شناسه جواب‌ها")]
            AnswerIds,

            [SpecialDescription("Answer Names")]
            [Description("نام جواب‌ها")]
            AnswerNames,

            [SpecialDescription("Answers Tags")]
            [Description("تگ جواب‌ها")]
            AnswersTags,
        }

        /// <summary>
        /// Get ConverterType for Serializer Object from a format to another format
        /// </summary>
        public enum MethodConverterType
        {
            [SpecialDescription("XML To Json")]
            [Description("*.xml")]
            XmlToJson,

            [SpecialDescription("Json To XML")]
            [Description("*.json")]
            JsonToXml,
        }

        #endregion

        /// <summary>
        /// Get Content MenuItem Type
        /// </summary>
        public enum MethodMenuItemType
        {
            [SpecialDescription("The Admin Management")]
            [Description("Admin")]
            Admin,
            [SpecialDescription("The Reservation Management")]
            [Description("Reservation")]
            Reservation,
            [SpecialDescription("The Company Management")]
            [Description("Company")]
            Company,
            [SpecialDescription("The ExcelSheet Management")]
            [Description("ExcelSheet")]
            ExcelSheet,
            [SpecialDescription("The Floor Management")]
            [Description("Floor")]
            Floor,
            [SpecialDescription("The HierarchyModifier Management")]
            [Description("Assign Modifier to Hierarchy")]
            HierarchyModifier,
            [SpecialDescription("The Hierarchy Management")]
            [Description("Hierarchy")]
            Hierarchy,
            [SpecialDescription("The Login Management")]
            [Description("Login")]
            Login,
            [SpecialDescription("The Management Page")]
            [Description("Management Page")]
            ManagementPage,
            [SpecialDescription("The MenuItemFlag Management")]
            [Description("MenuItemFlag")]
            MenuItemFlag,
            [SpecialDescription("The MenuItemHierarchy Management")]
            [Description("Assign Hierarchy to MenuItem")]
            MenuItemHierarchy,
            [SpecialDescription("The MenuItemMenuItemFlag Management")]
            [Description("Assign MenuItemFlag To MenuItem")]
            MenuItemMenuItemFlag,
            [SpecialDescription("The MenuItemTax Management")]
            [Description("Assign Tax To MenuItem")]
            MenuItemTax,
            [SpecialDescription("The MenuItem Management")]
            [Description("MenuItem")]
            MenuItem,
            [SpecialDescription("The MenuSection Management")]
            [Description("MenuSection")]
            MenuSection,
            [SpecialDescription("The Menu Management")]
            [Description("Menu")]
            Menu,
            [SpecialDescription("The Modifier Management")]
            [Description("Modifier")]
            Modifier,
            [SpecialDescription("The Restaurant Management")]
            [Description("Restaurant")]
            Restaurant,
            [SpecialDescription("The SerializerObject Management")]
            [Description("Serializer Object")]
            SerializerObject,
            [SpecialDescription("The Table Management")]
            [Description("Table")]
            Table,
            [SpecialDescription("The Tax Management")]
            [Description("Tax")]
            Tax,
            [SpecialDescription("The UserGroup Management")]
            [Description("UserGroup")]
            UserGroup,
            [SpecialDescription("The User Management")]
            [Description("User")]
            User,
        }
    }
}
