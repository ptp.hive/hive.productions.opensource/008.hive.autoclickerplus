﻿namespace HiVE.BasicModels.BoxItems
{
    public class BoxContentPhotosPathsParametersIndexs
    {
        public int? ContentImageType { get; set; }
        public int? ContentImageName { get; set; }

        public int? ContentPhotoPath { get; set; }
        public int? ContentPhotoName { get; set; }

        public int? ContentIds { get; set; }
        public int? ContentName { get; set; }

        public BoxContentPhotosPathsParametersIndexs() { }

        public BoxContentPhotosPathsParametersIndexs(
            int? contentImageType,
            int? contentImageName,

            int? contentPhotoPath,
            int? contentPhotoName,

            int? contentIds,
            int? contentName)
        {
            this.ContentImageType = contentImageType;
            this.ContentImageName = contentImageName;

            this.ContentPhotoPath = contentPhotoPath;
            this.ContentPhotoName = contentPhotoName;

            this.ContentIds = contentIds;
            this.ContentName = contentName;
        }
    }
}
