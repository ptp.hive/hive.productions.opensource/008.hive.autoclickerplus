﻿using System.Windows;
using System.Windows.Controls;

namespace HiVE.BasicModels.Helpers
{
    public static class Extensions
    {
        public static void ScrollIntoView(
            this ItemsControl control,
            object item)
        {
            FrameworkElement framework =
                control.ItemContainerGenerator.ContainerFromItem(item)
                as FrameworkElement;
            if (framework == null) { return; }
            framework.BringIntoView();
        }

        public static void ScrollIntoView(this ItemsControl control)
        {
            int count = control.Items.Count;
            if (count == 0) { return; }
            object item = control.Items[count - 1];
            control.ScrollIntoView(item);
        }
    }
}
