﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.MetroMenus
{
    public class Converter_ActualSizeToSizeMinusMargin : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            double returnValue = 0;

            try
            {
                if (value != null)
                { returnValue = ((double)value - 32); }

                if (returnValue < 0)
                { returnValue = 0; }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from SizeMinusMargin back to ActualSize");
        }
    }
}
