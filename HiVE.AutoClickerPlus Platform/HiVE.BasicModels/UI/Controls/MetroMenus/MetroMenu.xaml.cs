﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace HiVE.BasicModels.MetroMenus
{
    /// <summary>
    /// Interaction logic for MetroMenu.xaml
    /// </summary>
    //[ContentProperty("MenuItems")]
    public partial class MetroMenu : Menu
    {
        public MetroMenu()
        {
            InitializeComponent();
        }

        #region Properties

        #endregion

        #region Dependency Properties

        #region MetroMenuType

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty MetroMenuTypeProperty =
            DependencyProperty.Register(
                "MetroMenuType",
                typeof(MetroMenuType),
                typeof(MetroMenu),
                new PropertyMetadata(MetroMenuType.Default, OnMetroMenuTypeChanged));

        /// <summary>
        /// Gets / Sets the MetroMenuType that the control is showing 
        /// </summary>
        public MetroMenuType MetroMenuType
        {
            get
            {
                return (MetroMenuType)GetValue(MetroMenuTypeProperty);
            }
            set
            {
                SetValue(MetroMenuTypeProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the MetroMenuType property.
        /// </summary>
        /// <param name="d">MetroMenu</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnMetroMenuTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroMenu = (MetroMenu)d;
            var oldMetroMenuType = (MetroMenuType)e.OldValue;
            var newMetroMenuType = metroMenu.MetroMenuType;
            metroMenu.OnMetroMenuTypeChanged(oldMetroMenuType, newMetroMenuType);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the MetroMenuType property.
        /// </summary>
        /// <param name="oldMetroMenuType">Old Value</param>
        /// <param name="newMetroMenuType">New Value</param>
        protected virtual void OnMetroMenuTypeChanged(MetroMenuType oldMetroMenuType, MetroMenuType newMetroMenuType)
        {
            SetMetroMenuProperties();
        }

        #endregion

        #region ContentStyle

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty ContentStyleProperty =
            DependencyProperty.Register(
                "ContentStyle",
                typeof(Style),
                typeof(MetroMenu),
                new FrameworkPropertyMetadata(new Style()));

        /// <summary>
        /// Gets / Sets the ContentStyle that the control is showing
        /// </summary>
        internal Style ContentStyle
        {
            get
            {
                return (Style)GetValue(ContentStyleProperty);
            }
            set
            {
                SetValue(ContentStyleProperty, value);
            }
        }

        #endregion

        #region MenuItems

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty MenuItemsProperty =
            DependencyProperty.Register(
                "MenuItems",
                typeof(ObservableCollection<DependencyObject>),
                typeof(MetroMenu),
                new FrameworkPropertyMetadata(new ObservableCollection<DependencyObject>()));

        /// <summary>
        /// Gets / Sets the MenuItems that the control is showing
        /// </summary>
        public ObservableCollection<DependencyObject> MenuItems
        {
            get
            {
                return (ObservableCollection<DependencyObject>)GetValue(MenuItemsProperty);
            }
            set
            {
                SetValue(MenuItemsProperty, value);
            }
        }

        #endregion

        #region IsDefaultProperties

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty IsDefaultPropertiesProperty =
            DependencyProperty.Register(
                "IsDefaultProperties",
                typeof(bool),
                typeof(MetroMenu),
                new FrameworkPropertyMetadata(true, OnIsDefaultPropertiesChanged));

        /// <summary>
        /// Gets / Sets the IsDefaultProperties that the control is showing
        /// </summary>
        public bool IsDefaultProperties
        {
            get
            {
                return (bool)GetValue(IsDefaultPropertiesProperty);
            }
            set
            {
                SetValue(IsDefaultPropertiesProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the IsDefaultProperties property.
        /// </summary>
        /// <param name="d">MetroMenu</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnIsDefaultPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroMenu = (MetroMenu)d;
            var oldIsDefaultProperties = (bool)e.OldValue;
            var newIsDefaultProperties = metroMenu.IsDefaultProperties;
            metroMenu.OnIsDefaultPropertiesChanged(oldIsDefaultProperties, newIsDefaultProperties);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the IsDefaultProperties property.
        /// </summary>
        /// <param name="oldIsDefaultProperties">Old Value</param>
        /// <param name="newIsDefaultProperties">New Value</param>
        protected virtual void OnIsDefaultPropertiesChanged(bool oldIsDefaultProperties, bool newIsDefaultProperties)
        {
            SetMetroMenuProperties();
        }

        #endregion

        #region ImageSourceUri

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty ImageSourceUriProperty =
            DependencyProperty.Register(
                "ImageSourceUri",
                typeof(string),
                typeof(MetroMenu),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Gets / Sets the Image that the control is showing
        /// </summary>
        internal string ImageSourceUri
        {
            get
            {
                return (string)GetValue(ImageSourceUriProperty);
            }
            set
            {
                SetValue(ImageSourceUriProperty, value);
            }
        }

        #endregion

        #region CustomImageSourceUri

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty CustomImageSourceUriProperty =
            DependencyProperty.Register(
                "CustomImageSourceUri",
                typeof(string),
                typeof(MetroMenu),
                new FrameworkPropertyMetadata(string.Empty, OnCustomImageSourceUriChanged));

        /// <summary>
        /// Gets / Sets the Custom Image that the control is showing
        /// </summary>
        public string CustomImageSourceUri
        {
            get
            {
                return (string)GetValue(CustomImageSourceUriProperty);
            }
            set
            {
                SetValue(CustomImageSourceUriProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the CustomImageSourceUri property.
        /// </summary>
        /// <param name="d">MetroMenu</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnCustomImageSourceUriChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroMenu = (MetroMenu)d;
            var oldCustomImageSourceUri = (string)e.OldValue;
            var newCustomImageSourceUri = metroMenu.CustomImageSourceUri;
            metroMenu.OnCustomImageSourceUriChanged(oldCustomImageSourceUri, newCustomImageSourceUri);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the CustomImageSourceUri property.
        /// </summary>
        /// <param name="oldCustomImageSourceUri">Old Value</param>
        /// <param name="newCustomImageSourceUri">New Value</param>
        protected virtual void OnCustomImageSourceUriChanged(string oldCustomImageSourceUri, string newCustomImageSourceUri)
        {
            SetCustomImageSourceUri(newCustomImageSourceUri ?? string.Empty);
        }

        #endregion

        #region ContentValue

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty ContentValueProperty =
            DependencyProperty.Register(
                "ContentValue",
                typeof(string),
                typeof(MetroMenu),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Gets / Sets the ContentValue that the control is showing
        /// </summary>
        internal string ContentValue
        {
            get
            {
                return (string)GetValue(ContentValueProperty);
            }
            set
            {
                SetValue(ContentValueProperty, value);
            }
        }

        #endregion

        #region CustomContent

        /// <summary>
        /// Dependency Object for the value of the MetroMenu Control
        /// </summary>
        public static readonly DependencyProperty CustomContentProperty =
            DependencyProperty.Register(
                "CustomContent",
                typeof(string),
                typeof(MetroMenu),
                new FrameworkPropertyMetadata(string.Empty, OnCustomContentChanged));

        /// <summary>
        /// Gets / Sets the CustomContent that the control is showing
        /// </summary>
        public string CustomContent
        {
            get
            {
                return (string)GetValue(CustomContentProperty);
            }
            set
            {
                SetValue(CustomContentProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the CustomContent property.
        /// </summary>
        /// <param name="d">MetroMenu</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnCustomContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroMenu = (MetroMenu)d;
            var oldCustomContent = (string)e.OldValue;
            var newCustomContent = metroMenu.CustomContent;
            metroMenu.OnCustomContentChanged(oldCustomContent, newCustomContent);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the CustomContent property.
        /// </summary>
        /// <param name="oldCustomContent">Old Value</param>
        /// <param name="newCustomContent">New Value</param>
        protected virtual void OnCustomContentChanged(string oldCustomContent, string newCustomContent)
        {
            SetCustomContentValue(newCustomContent ?? string.Empty);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Refresh MetroMenuType and Set Properties
        /// </summary>
        private void SetMetroMenuProperties()
        {
            try
            {
                if (IsDefaultProperties
                    && MetroMenuType != MetroMenuType.Custom)
                {
                    this.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Danger.ToDescriptionString()));
                    this.Background = (SolidColorBrush)(new BrushConverter().ConvertFromString(MetroMenuType.ToDescriptionString()));

                    this.ContentValue = MetroMenuType.ToSpecialDescriptionString().Trim();

                    if (Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.None.ToDescriptionString())) &&
                        Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Default.ToDescriptionString())) &&
                        Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Warning.ToDescriptionString())) &&
                        Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Normal.ToDescriptionString())))
                    {
                        this.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Default.ToDescriptionString()));
                    }

                    //ContentStyle = Application.Current.FindResource(string.Format("{0}", "MetroUI.MenuStyle")) as Style;

                    switch (MetroMenuType)
                    {
                        #region Default Menus

                        #endregion

                        #region Common

                        #endregion

                        #region Create/Edit Mode

                        #endregion

                        #region BackupClient

                        #endregion

                        #region Connection

                        #endregion

                        #region FileManager

                        #endregion

                        #region Special

                        case MetroMenuType.Password:
                            {
                                ImageSourceUri =
                                    (new Uri("pack://application:,,,/HiVE.BasicModels;component/MetroMenus/Images/" + "Password.png")).ToString();

                                //ContentStyle = Application.Current.FindResource(string.Format("{0}", "MetroUI.MenuStyle.PasswordDisplay")) as Style;

                                break;
                            }

                        case MetroMenuType.Confirmation:
                            {
                                ImageSourceUri =
                                    (new Uri("pack://application:,,,/HiVE.BasicModels;component/MetroMenus/Images/" + "Confirmation.png")).ToString();

                                break;
                            }

                        case MetroMenuType.Back:
                            {
                                ImageSourceUri =
                                    (new Uri("pack://application:,,,/HiVE.BasicModels;component/MetroMenus/Images/" + "Back.png")).ToString();

                                break;
                            }

                            #endregion
                    }
                }

                if (CustomContent != "")
                {
                    ContentValue = CustomContent.Trim();
                }

                if (CustomImageSourceUri != "")
                {
                    ImageSourceUri = CustomImageSourceUri.Trim();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Custom ImageSourceUri
        /// If IsEmpty, then use of default common value
        /// </summary>
        /// <param name="customImageSourceUri"></param>
        private void SetCustomImageSourceUri(string customImageSourceUri)
        {
            try
            {
                if (customImageSourceUri != "")
                {
                    ImageSourceUri = customImageSourceUri.Trim();
                }
                else
                {
                    SetMetroMenuProperties();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Custom ContentValue
        /// If IsEmpty, then use of default common value
        /// </summary>
        /// <param name="customContentValue"></param>
        private void SetCustomContentValue(string customContentValue)
        {
            try
            {
                if (customContentValue != "")
                {
                    ContentValue = customContentValue.Trim();
                }
                else
                {
                    SetMetroMenuProperties();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        /// <summary>
        /// Load this user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroMenuUC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
