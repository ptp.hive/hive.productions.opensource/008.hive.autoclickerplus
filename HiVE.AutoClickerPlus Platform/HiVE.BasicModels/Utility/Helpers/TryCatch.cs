﻿using HiVE.BasicModels.Helpers;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace HiVE.BasicModels.Utility.Helpers
{
    public static class TryCatch
    {
        #region TryCatch Methods

        /// <summary>
        /// CatchExceptionMessageError
        /// IsShow Message Error
        /// If false, so no show error of function in message
        /// Just show friendly message error if IsShowMessageError==True and IsShowNotFriendlyMessageError==True
        /// Default = True
        /// </summary>
        /// <param name="Exception"></param>
        /// <returns></returns>
        private static bool CatchExceptionMessageError(Exception Exception)
        {
            try
            {
                if (Exception != null)
                {
                    if (BasicMethods.CurrentMainSettings.IsShowMessageError)
                    {
                        Debug.Print("Error: " + Exception.Message);
                        MessageBox.Show(Exception.Message);
                        if (BasicMethods.CurrentMainSettings.IsShowNotFriendlyMessageError)
                        {
                            if (Exception.InnerException != null)
                            {
                                Debug.Print("Error: " + Exception.InnerException.Message);
                                MessageBox.Show(Exception.InnerException.Message);
                            }
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }

        /// <summary>
        /// CatchExceptionMessageError
        /// IsAllow Full Permission Show Message Error
        /// If false, so no show error of function in message with limited permission
        /// Just show friendly message error if IsShowMessageError==True and IsShowNotFriendlyMessageError==True
        /// </summary>
        /// <param name="Exception"></param>
        /// <param name="IsNeedFullPermissionShowMessageError">Default = False</param>
        /// <returns></returns>
        private static bool CatchExceptionMessageError(Exception Exception, bool IsNeedFullPermissionShowMessageError)
        {
            try
            {
                if (Exception != null)
                {
                    if (BasicMethods.CurrentMainSettings.IsShowMessageError && BasicMethods.CurrentMainSettings.IsAllowFullPermissionShowMessageError)
                    {
                        Debug.Print("Error: " + Exception.Message);
                        MessageBox.Show(Exception.Message);
                        if (BasicMethods.CurrentMainSettings.IsShowNotFriendlyMessageError)
                        {
                            if (Exception.InnerException != null)
                            {
                                Debug.Print("Error: " + Exception.InnerException.Message);
                                MessageBox.Show(Exception.InnerException.Message);
                            }
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }

        /// <summary>
        /// Get CatchExceptionMessageError
        /// IsShow Message Error
        /// If false, so no show error of function in message
        /// Just show friendly message error if IsShowMessageError==True and IsShowNotFriendlyMessageError==True
        /// Default = True
        /// </summary>
        /// <param name="Exception"></param>
        /// <returns></returns>
        public static bool GetCEM_Error(Exception Exception)
        {
            return CatchExceptionMessageError(Exception);
        }

        /// <summary>
        /// Get CatchExceptionMessageError
        /// IsAllow Full Permission Show Message Error
        /// If false, so no show error of function in message with limited permission
        /// Just show friendly message error if IsShowMessageError==True and IsShowNotFriendlyMessageError==True
        /// </summary>
        /// <param name="Exception"></param>
        /// <param name="IsNeedFullPermissionShowMessageError">Default = False</param>
        /// <returns></returns>
        public static bool GetCEM_Error(Exception Exception, bool IsNeedFullPermissionShowMessageError)
        {
            return CatchExceptionMessageError(Exception, IsNeedFullPermissionShowMessageError);
        }

        #endregion

        #region TryCatch Box Methods

        public enum TcMethodCommonMessage
        {
            [Description("\n- خطای x_")]
            BeginningErrorMessage,
            [Description("\n\n- متاسفیم.")]
            ApologyMessage
        }

        public enum TcMethodGeneralErrorMessage
        {
            [SpecialDescription("Logic Error!")]
            [Description("خطای منطقی.")]
            LogicError,
            [SpecialDescription("An unexpected error!")]
            [Description("یک خطای غیر منتظره!")]
            UnexpectedError,

            [SpecialDescription("Please close all message boxes opened by this application, before using this window!")]
            [Description("لطفا تمام جعبه پیام باز شده توسط این نرم افزار را ببندید، قبل از استفاده از این پنجره!")]
            ShowBlockingDialog,
            [SpecialDescription("Could not extract default icon for MessageBox Window!")]
            [Description("نماد پیش فرض برای پیام جعبه پنجره استخراج نشده!")]
            CouldNotExtractIcon,

            [Description("خطا در بارگیری برنامه.")]
            LoadingWindowError,
            [Description("خطا در بارگیری برنامه.")]
            LoadingUserControlError,

            [SpecialDescription("Error loading input file path!")]
            [Description("خطا در بارگیری مسیر فایل ورودی.")]
            LoadingInputFilePathError,

            [Description("خطا در ایجاد پایگاه‌داده.")]
            ErrorModelCreatingDatabase,
            [Description("خطا در مقداردهی اولیه به اطلاعات پایگاه‌داده.")]
            ErrorSeedInitialValue,
            [Description("خطا در برقراری ارتباط با پایگاه‌داده.")]
            ErrorConnectionDatabase,
            [Description("خطا در بارگیری اطلاعات صندوق.")]
            ErrorLoadInformationsDatabase,

            [SpecialDescription("There is no {0} with this {1}!")]
            [Description("{0} با این {1} پیدا نشد!")]
            ErrorNotFoundWithThis_2
        }

        public enum TcMethodTypeTemplate
        {
            IsOthers = 0,
            IsWindow,
            IsForm,
            IsPage,
            IsUserControl,
            IsClass
        }

        public enum TcMethodFollowingFunction
        {
            FollowingFunction1 = 1,
            FollowingFunction2,
            FollowingFunction3,
            FollowingFunction4,
            FollowingFunction5,
            FollowingFunction6,
            FollowingFunction7,
            FollowingFunction8,
            FollowingFunction9,
            FollowingFunction_Others,
        }

        public enum TcMethodAssemblyProduct
        {
            /// Common
            HiVE_BasicModels = 0,
            HiVE_Prerequisites,
            HiVE_DatabaseContext,
            HiVE_Startup,
            HiVE_Login,
            HiVE_MainProject,
            /// HiVE_MainProject
            HiVE_Assistant_Accountants,

            /// Others

        }

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ All Assembly Products

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_BasicModels

        public enum TcMethodTypeTemplate_HiVE_BasicModels_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_BasicModels_IsClass
        {
            #region isClass

            BasicAssemblyAttributeAccessors

            #endregion

            #region isConverter

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_Prerequisites

        public enum TcMethodTypeTemplate_HiVE_Prerequisites_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_Prerequisites_IsClass
        {
            #region isClass

            FontOperation,
            RunPrerequisites

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_DatabaseContext

        public enum TcMethodTypeTemplate_HiVE_DatabaseContext_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_DatabaseContext_IsClass
        {
            #region isClass

            #region isDb_Context

            DatabaseContext,
            DatabaseContextInitializer,
            isMigrations_Configuration,

            #endregion

            #region isDb_oDataSet

            is_oBasic_Syntax,
            is_oAccount,
            is_oCategory,
            is_oContentType,
            is_oFactor,
            is_oFundDetail,
            is_oPicture,
            is_oPropertyDefinition,
            is_oTransaction,

            #endregion

            #region isDb_Tables

            #endregion

            #region isDb_Triggers

            #endregion

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_Startup

        public enum TcMethodTypeTemplate_HiVE_Startup_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsWindow
        {
            MainWindow = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsForm
        {
            MainForm = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsPage
        {
            MainPage = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsUserControl
        {
            #region isUserControl

            #endregion
        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsClass
        {
            #region Models

            AssemblyAttributeAccessors,
            SpecialModels

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_Login

        public enum TcMethodTypeTemplate_HiVE_Login_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsWindow
        {
            MainWindow = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsForm
        {
            MainForm = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsPage
        {
            MainPage = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsUserControl
        {
            #region isUserControl

            #endregion
        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsClass
        {
            #region Models

            AssemblyAttributeAccessors,
            SpecialModels

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_MainProject

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsWindow
        {
            MainWindow = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsForm
        {
            MainForm = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsPage
        {
            MainPage = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsUserControl
        {
            #region isUserControl

            AboutUC

            #endregion
        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsClass
        {
            #region Models

            AssemblyAttributeAccessors,
            SpecialModels

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_Assistant_Accountants

        public enum TcMethodTypeTemplate_HiVE_Assistant_Accountants_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_Assistant_Accountants_IsWindow
        {
            MainWindow = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Assistant_Accountants_IsForm
        {
            MainForm = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Assistant_Accountants_IsPage
        {
            MainPage = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Assistant_Accountants_IsUserControl
        {
            #region isUserControl

            AboutUC,
            AccountUC,
            ActivatedUC,
            CategoryUC,
            DatabaseUC,
            MainMenuUC,
            ReportsUC,
            SettingUC,
            TransactionsUC

            #endregion
        }

        public enum TcMethodTypeTemplate_HiVE_Assistant_Accountants_IsClass
        {
            #region Models

            AssemblyAttributeAccessors,
            MetroTitleItems,
            SpecialModels

            #endregion
        }

        #endregion

        #endregion /TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ All Assembly Products

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ All Assembly Products

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_BasicModels

        #region TcMethodClassFunctions_HiVE_BasicModels_IsClass

        #region TcMethodClassFunctions_HiVE_BasicModels_IsClass_isClass

        public enum TcMethodClassFunctions_HiVE_BasicModels_IsClass_BasicAssemblyAttributeAccessors
        {

        }

        #endregion

        #region TcMethodClassFunctions_HiVE_BasicModels_IsClass_isConverter

        public enum TcMethodClassFunctions_HiVE_BasicModels_IsClass_Converter_BitmapSourceToByteArray
        {

        }

        #endregion

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_Prerequisites

        #region TcMethodClassFunctions_HiVE_Prerequisites_IsClass

        public enum TcMethodClassFunctions_HiVE_Prerequisites_IsClass_FontOperation
        {

        }

        public enum TcMethodClassFunctions_HiVE_Prerequisites_IsClass_RunPrerequisites
        {

        }

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_DatabaseContext

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass_isDb_Context

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_DatabaseContext
        {
            DatabaseContext,
            OnModelCreating,
            DbModelBuilder_Configurations_Tables
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_DatabaseContextInitializer
        {
            Seed,
            SeedInitialValue_Tables
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isMigrations_Configuration
        {
            Seed
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass_isDb_oDataSet

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oBasic_Syntax
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others

        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oAccount
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others
            IsHaveAdminDatabase,
            UnselectedLastCashier,
            UpdateFirstAccountToCashier,
            ChoicedCashierAccount,
            LoadCashierAccount,
            LoadAllIsActiveRowsIncludePicture,
            IsFundHaveCashier
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oCategory
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others
            LoadRowsWithSpecialParentId,
            LoadIsActiveRowsWithSpecialParentId,
            IsValidationParentId,
            LoadOrAddRowItemWithSpecialTitle,
            LoadOrAddRowItemWithSpecialValue
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oContentType
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others
            LoadAllIsActiveIsEditableRowsIncludeAllDetails,
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oFactor
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others
            LoadCountFactors,
            LoadCountIncomes,
            LoadCountCosts,
            LoadCountTransfers,
            LoadSumQuantityFactors,
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oFundDetail
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others
            AddFundCreationDate,
            AddFundDeliveryDate,
            UpdateFundDeliveryDate,
            LoadFundCreationDate,
            LoadFundDeliveryDate
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oPicture
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others
            DeleteRowItemWithSpecialId_Read,
            LoadIsDefaultRowsIncludeAllDetails,
            LoadIsDefaultIsActiveRowsIncludeAllDetails
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oPropertyDefinition
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others

        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oTransaction
        {
            /// Common
            InitialValue,
            AddDefaultRows,
            LoadAllRowsIncludeAllDetails,
            LoadAllIsActiveRowsIncludeAllDetails,
            AddNewRowItem,
            UpdateRowItem,
            DeleteRowItem,
            LoadFirstRowItem,
            LoadLastInsertRowItem,
            LoadLastUpdateRowItem,
            LoadRowItemWithSpecialId,
            LoadCountRows,
            LoadCountIsActiveRows,

            /// Others
            AddNewRowItemWithSpecialDetails,
            LoadSumQuantityFactorsByTransactions
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass_isDb_Tables

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_Account
        {

        }

        #endregion

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass_isDb_Triggers

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_Triggers_Configuration
        {

        }

        #endregion

        #endregion

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_Startup

        #region TcMethodClassFunctions_HiVE_Startup_IsWindow

        public enum TcMethodClassFunctions_HiVE_Startup_IsWindow_MainWindow
        {
            ThisWindow_Loaded,
            DisplayBasicWindowDetails
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_Startup_IsClass

        public enum TcMethodClassFunctions_Startup_IsClass_AssemblyAttributeAccessors
        {

        }

        public enum TcMethodClassFunctions_Startup_IsClass_SpecialModels
        {

        }

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_Login

        #region TcMethodClassFunctions_HiVE_Login_IsWindow

        public enum TcMethodClassFunctions_HiVE_Login_IsWindow_MainWindow
        {
            ThisWindow_Loaded,
            DisplayBasicWindowDetails
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_Login_IsClass

        public enum TcMethodClassFunctions_Login_IsClass_AssemblyAttributeAccessors
        {

        }

        public enum TcMethodClassFunctions_Login_IsClass_SpecialModels
        {

        }

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_MainProject

        #region TcMethodClassFunctions_HiVE_MainProject_IsWindow

        public enum TcMethodClassFunctions_HiVE_MainProject_IsWindow_MainWindow
        {
            ThisWindow_Loaded,
            DisplayBasicWindowDetails
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_MainProject_IsUserControl

        public enum TcMethodClassFunctions_HiVE_MainProject_IsUserControl_AboutUC
        {
            ThisUserControl_Loaded = 0
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_MainProject_IsClass

        public enum TcMethodClassFunctions_HiVE_MainProject_IsClass_AssemblyAttributeAccessors
        {

        }

        public enum TcMethodClassFunctions_HiVE_MainProject_IsClass_SpecialModels
        {

        }

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_Assistant_Accountants

        #region TcMethodClassFunctions_HiVE_Assistant_Accountants_IsWindow

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsWindow_MainWindow
        {
            ThisWindow_Loaded,
            DisplayBasicWindowDetails
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_AboutUC
        {
            ThisUserControl_Loaded = 0
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_AccountUC
        {
            ThisUserControl_Loaded = 0,
            buttonAccountEdit_Click,
            buttonAccountDelete_Click,
            buttonAccountIsCashier_Click,
            buttonOK_Click,
            IsValidationSave_Account,
            buttonSelectCustomPicture_Click
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_ActivatedUC
        {
            ThisUserControl_Loaded = 0
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_CategoryUC
        {
            ThisUserControl_Loaded = 0,
            AddNodeCollectionCategories_AllCategoriesType,
            RefreshTreeViewAllCategoriesType,
            IsValidationSave_Category,
            buttonOK_Click,
            buttonEdit_Click,
            buttonDelete_Click,
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_DatabaseUC
        {
            ThisUserControl_Loaded = 0,
            DisplayDatabaseDetails
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_MainMenuUC
        {
            ThisUserControl_Loaded = 0
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_ReportsUC
        {
            ThisUserControl_Loaded = 0,
            AddNodeCollectionCategories,
            gridReportDetails_Loaded,
            RefreshDataGridFactors,
            buttonDelete_Click
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_SettingUC
        {
            ThisUserControl_Loaded = 0
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsUserControl_TransactionsUC
        {
            ThisUserControl_Loaded = 0,
            AddNodeCollectionCategories,
            IsErrorCreateNewTransaction,
            buttonOK_Click
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_Assistant_Accountants_IsClass

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsClass_AssemblyAttributeAccessors
        {

        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsClass_MetroTitleItems
        {
            DisplayMainMenu,
            DisplayDatabaseDetails,
        }

        public enum TcMethodClassFunctions_HiVE_Assistant_Accountants_IsClass_SpecialModels
        {

        }

        #endregion

        #endregion

        #endregion /TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ All Assembly Products

        #endregion /TryCatch Box Methods
    }
}
