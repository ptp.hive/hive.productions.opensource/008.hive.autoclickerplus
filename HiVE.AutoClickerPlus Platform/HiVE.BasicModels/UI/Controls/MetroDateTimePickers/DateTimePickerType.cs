﻿using System.ComponentModel;

namespace HiVE.BasicModels.MetroDateTimePickers
{
    public enum DateTimePickerType
    {
        [Description("Short")]
        ShortTime,
        [Description("Long")]
        LongTime,

        [Description("QuickSelect")]
        QuickSelect,
        [Description("QuickSelectPlus")]
        QuickSelectPlus,

        [Description("Time")]
        JustTime,
        [Description("Calender")]
        JustCalender,
    }

    public enum TimeType
    {
        [Description("AM")]
        AM,
        [Description("PM")]
        PM
    }

    public enum QuickMinutes
    {
        Minute_10 = 10,
        Minute_15 = 15,
        Minute_20 = 20,
        Minute_30 = 30,
        Minute_40 = 40,
        Minute_45 = 45,
        Minute_50 = 50,
    }
}
