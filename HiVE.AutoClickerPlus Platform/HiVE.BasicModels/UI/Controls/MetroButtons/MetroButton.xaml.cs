﻿using HiVE.BasicModels.Helpers;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace HiVE.BasicModels.MetroButtons
{
    /// <summary>
    /// Interaction logic for MetroButton.xaml
    /// </summary>
    public partial class MetroButton : Button
    {
        public MetroButton()
        {
            InitializeComponent();
        }

        #region Feilds

        private string CircleStyle = ".Circle";
        private string SpecialStyle = string.Empty;
        private string SpecialImageSourceUri = string.Empty;

        #endregion

        #region Dependency Properties

        #region MetroButtonType

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty MetroButtonTypeProperty =
            DependencyProperty.Register(
                "MetroButtonType",
                typeof(MetroButtonType),
                typeof(MetroButton),
                new PropertyMetadata(MetroButtonType.Default, OnMetroButtonTypeChanged));

        /// <summary>
        /// Gets / Sets the MetroButtonType that the control is showing 
        /// </summary>
        public MetroButtonType MetroButtonType
        {
            get
            {
                return (MetroButtonType)GetValue(MetroButtonTypeProperty);
            }
            set
            {
                SetValue(MetroButtonTypeProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the MetroButtonType property.
        /// </summary>
        /// <param name="d">MetroButton</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnMetroButtonTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroButton = (MetroButton)d;
            var oldMetroButtonType = (MetroButtonType)e.OldValue;
            var newMetroButtonType = metroButton.MetroButtonType;
            metroButton.OnMetroButtonTypeChanged(oldMetroButtonType, newMetroButtonType);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the MetroButtonType property.
        /// </summary>
        /// <param name="oldMetroButtonType">Old Value</param>
        /// <param name="newMetroButtonType">New Value</param>
        protected virtual void OnMetroButtonTypeChanged(MetroButtonType oldMetroButtonType, MetroButtonType newMetroButtonType)
        {
            SetMetroButtonProperties();
        }

        #endregion

        #region ContentStyle

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty ContentStyleProperty =
            DependencyProperty.Register(
                "ContentStyle",
                typeof(Style),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(new Style()));

        /// <summary>
        /// Gets / Sets the ContentStyle that the control is showing
        /// </summary>
        internal Style ContentStyle
        {
            get
            {
                return (Style)GetValue(ContentStyleProperty);
            }
            set
            {
                SetValue(ContentStyleProperty, value);
            }
        }

        #endregion

        #region DisplayStyle

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty DisplayStyleProperty =
            DependencyProperty.Register(
                "DisplayStyle",
                typeof(MetroButtonDisplayStyle),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(MetroButtonDisplayStyle.Text));

        /// <summary>
        /// Gets / Sets the DisplayStyle that the control is showing
        /// </summary>
        public MetroButtonDisplayStyle DisplayStyle
        {
            get
            {
                return (MetroButtonDisplayStyle)GetValue(DisplayStyleProperty);
            }
            set
            {
                SetValue(DisplayStyleProperty, value);
            }
        }

        #endregion

        #region IsDefaultProperties

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty IsDefaultPropertiesProperty =
            DependencyProperty.Register(
                "IsDefaultProperties",
                typeof(bool),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(true, OnIsDefaultPropertiesChanged));

        /// <summary>
        /// Gets / Sets the IsDefaultProperties that the control is showing
        /// </summary>
        public bool IsDefaultProperties
        {
            get
            {
                return (bool)GetValue(IsDefaultPropertiesProperty);
            }
            set
            {
                SetValue(IsDefaultPropertiesProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the IsDefaultProperties property.
        /// </summary>
        /// <param name="d">MetroButton</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnIsDefaultPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroButton = (MetroButton)d;
            var oldIsDefaultProperties = (bool)e.OldValue;
            var newIsDefaultProperties = metroButton.IsDefaultProperties;
            metroButton.OnIsDefaultPropertiesChanged(oldIsDefaultProperties, newIsDefaultProperties);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the IsDefaultProperties property.
        /// </summary>
        /// <param name="oldIsDefaultProperties">Old Value</param>
        /// <param name="newIsDefaultProperties">New Value</param>
        protected virtual void OnIsDefaultPropertiesChanged(bool oldIsDefaultProperties, bool newIsDefaultProperties)
        {
            SetMetroButtonProperties();
        }

        #endregion

        #region IsCircle

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty IsCircleProperty =
            DependencyProperty.Register(
                "IsCircle",
                typeof(bool),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(false, OnIsCircleChanged));

        /// <summary>
        /// Gets / Sets the IsCircle that the control is showing
        /// </summary>
        public bool IsCircle
        {
            get
            {
                return (bool)GetValue(IsCircleProperty);
            }
            set
            {
                SetValue(IsCircleProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the IsCircle property.
        /// </summary>
        /// <param name="d">MetroButton</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnIsCircleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroButton = (MetroButton)d;
            var oldIsCircle = (bool)e.OldValue;
            var newIsCircle = metroButton.IsCircle;
            metroButton.OnIsCircleChanged(oldIsCircle, newIsCircle);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the IsCircle property.
        /// </summary>
        /// <param name="oldIsCircle">Old Value</param>
        /// <param name="newIsCircle">New Value</param>
        protected virtual void OnIsCircleChanged(bool oldIsCircle, bool newIsCircle)
        {
            SetMetroButtonProperties();
        }

        #endregion

        #region IsTransparent

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty IsTransparentProperty =
            DependencyProperty.Register(
                "IsTransparent",
                typeof(bool),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(false, OnIsTransparentChanged));

        /// <summary>
        /// Gets / Sets the IsTransparent that the control is showing
        /// </summary>
        public bool IsTransparent
        {
            get
            {
                return (bool)GetValue(IsTransparentProperty);
            }
            set
            {
                SetValue(IsTransparentProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the IsTransparent property.
        /// </summary>
        /// <param name="d">MetroButton</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnIsTransparentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroButton = (MetroButton)d;
            var oldIsTransparent = (bool)e.OldValue;
            var newIsTransparent = metroButton.IsTransparent;
            metroButton.OnIsTransparentChanged(oldIsTransparent, newIsTransparent);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the IsTransparent property.
        /// </summary>
        /// <param name="oldIsTransparent">Old Value</param>
        /// <param name="newIsTransparent">New Value</param>
        protected virtual void OnIsTransparentChanged(bool oldIsTransparent, bool newTransparent)
        {
            SetMetroButtonProperties();
        }

        #endregion

        #region ImageSourceUri

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty ImageSourceUriProperty =
            DependencyProperty.Register(
                "ImageSourceUri",
                typeof(string),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Gets / Sets the Image that the control is showing
        /// </summary>
        internal string ImageSourceUri
        {
            get
            {
                return (string)GetValue(ImageSourceUriProperty);
            }
            set
            {
                SetValue(ImageSourceUriProperty, value);
            }
        }

        #endregion

        #region CustomImageSourceUri

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty CustomImageSourceUriProperty =
            DependencyProperty.Register(
                "CustomImageSourceUri",
                typeof(string),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(string.Empty, OnCustomImageSourceUriChanged));

        /// <summary>
        /// Gets / Sets the Custom Image that the control is showing
        /// </summary>
        public string CustomImageSourceUri
        {
            get
            {
                return (string)GetValue(CustomImageSourceUriProperty);
            }
            set
            {
                SetValue(CustomImageSourceUriProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the CustomImageSourceUri property.
        /// </summary>
        /// <param name="d">MetroButton</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnCustomImageSourceUriChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroButton = (MetroButton)d;
            var oldCustomImageSourceUri = (string)e.OldValue;
            var newCustomImageSourceUri = metroButton.CustomImageSourceUri;
            metroButton.OnCustomImageSourceUriChanged(oldCustomImageSourceUri, newCustomImageSourceUri);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the CustomImageSourceUri property.
        /// </summary>
        /// <param name="oldCustomImageSourceUri">Old Value</param>
        /// <param name="newCustomImageSourceUri">New Value</param>
        protected virtual void OnCustomImageSourceUriChanged(string oldCustomImageSourceUri, string newCustomImageSourceUri)
        {
            SetCustomImageSourceUri(newCustomImageSourceUri ?? string.Empty);
        }

        #endregion

        #region ContentValue

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty ContentValueProperty =
            DependencyProperty.Register(
                "ContentValue",
                typeof(string),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Gets / Sets the ContentValue that the control is showing
        /// </summary>
        internal string ContentValue
        {
            get
            {
                return (string)GetValue(ContentValueProperty);
            }
            set
            {
                SetValue(ContentValueProperty, value);
            }
        }

        #endregion

        #region CustomContent

        /// <summary>
        /// Dependency Object for the value of the MetroButton Control
        /// </summary>
        public static readonly DependencyProperty CustomContentProperty =
            DependencyProperty.Register(
                "CustomContent",
                typeof(string),
                typeof(MetroButton),
                new FrameworkPropertyMetadata(string.Empty, OnCustomContentChanged));

        /// <summary>
        /// Gets / Sets the CustomContent that the control is showing
        /// </summary>
        public string CustomContent
        {
            get
            {
                return (string)GetValue(CustomContentProperty);
            }
            set
            {
                SetValue(CustomContentProperty, value);
            }
        }

        /// <summary>
        /// Handles changes to the CustomContent property.
        /// </summary>
        /// <param name="d">MetroButton</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnCustomContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var metroButton = (MetroButton)d;
            var oldCustomContent = (string)e.OldValue;
            var newCustomContent = metroButton.CustomContent;
            metroButton.OnCustomContentChanged(oldCustomContent, newCustomContent);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the CustomContent property.
        /// </summary>
        /// <param name="oldCustomContent">Old Value</param>
        /// <param name="newCustomContent">New Value</param>
        protected virtual void OnCustomContentChanged(string oldCustomContent, string newCustomContent)
        {
            SetCustomContentValue(newCustomContent ?? string.Empty);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Refresh MetroButtonType and Set Properties
        /// </summary>
        private void SetMetroButtonProperties()
        {
            try
            {
                if (IsDefaultProperties
                    && MetroButtonType != MetroButtonType.Custom)
                {
                    this.ContentValue = MetroButtonType.ToSpecialDescriptionString().Trim();

                    if (!IsTransparent)
                    {
                        this.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Danger.ToDescriptionString()));
                        this.Background = (SolidColorBrush)(new BrushConverter().ConvertFromString(MetroButtonType.ToDescriptionString()));

                        if (Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.None.ToDescriptionString())) &&
                            Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Default.ToDescriptionString())) &&
                            Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Warning.ToDescriptionString())) &&
                            Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Normal.ToDescriptionString())) &&
                            Background != (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Snow.ToDescriptionString())))
                        {
                            this.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Default.ToDescriptionString()));
                        }
                    }
                    else
                    {
                        this.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString("#FF525963"));
                        this.Background = (SolidColorBrush)(new BrushConverter().ConvertFromString(MethodColorsType.Transparent.ToDescriptionString()));
                    }

                    if (IsCircle)
                    { CircleStyle = ".Circle"; }
                    else { CircleStyle = string.Empty; }

                    SpecialStyle = string.Empty;
                    SpecialImageSourceUri = string.Empty;
                    ImageSourceUri = string.Empty;

                    switch (MetroButtonType)
                    {
                        #region Default Buttons

                        #endregion

                        #region Common

                        #endregion

                        #region Create/Edit Mode

                        case MetroButtonType.Refresh:
                            {
                                ImageSourceUri =
                                    (new Uri("pack://application:,,,/HiVE.BasicModels;component/MetroButtons/Images/" + "Refresh.png")).ToString();

                                break;
                            }

                        #endregion

                        #region BackupClient

                        #endregion

                        #region Connection

                        #endregion

                        #region FileManager

                        #endregion

                        #region Special

                        case MetroButtonType.Numeric:
                            {
                                this.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString("#FF424242"));

                                break;
                            }

                        case MetroButtonType.Password:
                            {
                                SpecialStyle = ".PasswordDisplay";
                                SpecialImageSourceUri = "Password.png";

                                break;
                            }

                        case MetroButtonType.Confirmation:
                            {
                                SpecialImageSourceUri = "Confirmation.png";

                                break;
                            }

                        case MetroButtonType.Back:
                            {
                                SpecialImageSourceUri = "Back.png";

                                break;
                            }

                        case MetroButtonType.Remove:
                            {
                                SpecialImageSourceUri = "Remove.png";

                                break;
                            }

                        case MetroButtonType.Clear:
                            {
                                SpecialImageSourceUri = "Clear.png";

                                break;
                            }

                        case MetroButtonType.Flat:
                            {
                                SpecialStyle = ".Flat";

                                break;
                            }

                        case MetroButtonType.Flat_Large:
                            {
                                SpecialStyle = ".Flat.Large";

                                break;
                            }

                            #endregion
                    }
                }

                if (SpecialImageSourceUri.Trim() != "")
                {
                    ImageSourceUri =
                        (new Uri(
                            string.Format(
                                "{0}{1}",
                                "pack://application:,,,/HiVE.BasicModels;component/MetroButtons/Images/",
                                SpecialImageSourceUri))).ToString();
                }

                ContentStyle =
                    Application.Current.FindResource(
                        string.Format(
                            "{0}{1}{2}",
                            "MetroUI.ButtonStyle",
                            SpecialStyle,
                            CircleStyle)) as Style;

                if (CustomContent != "")
                {
                    ContentValue = CustomContent.Trim();
                }

                if (CustomImageSourceUri != "")
                {
                    ImageSourceUri = CustomImageSourceUri.Trim();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Custom ImageSourceUri
        /// If IsEmpty, then use of default common value
        /// </summary>
        /// <param name="customImageSourceUri"></param>
        private void SetCustomImageSourceUri(string customImageSourceUri)
        {
            try
            {
                if (customImageSourceUri != "")
                {
                    ImageSourceUri = customImageSourceUri.Trim();
                }
                else
                {
                    SetMetroButtonProperties();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Custom ContentValue
        /// If IsEmpty, then use of default common value
        /// </summary>
        /// <param name="customContentValue"></param>
        private void SetCustomContentValue(string customContentValue)
        {
            try
            {
                if (customContentValue != "")
                {
                    ContentValue = customContentValue.Trim();
                }
                else
                {
                    SetMetroButtonProperties();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        /// <summary>
        /// Load this user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroButtonUC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
