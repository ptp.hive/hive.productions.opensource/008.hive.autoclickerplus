﻿namespace HiVE.BasicModels.BoxItems
{
    public class BoxColorDetails
    {
        public string Name { get; set; }
        public string HashCode { get; set; }

        public BoxColorDetails() { }

        public BoxColorDetails(
            string name,
            string hashCode)
        {
            this.Name = name;
            this.HashCode = hashCode;
        }
    }
}
