﻿using HiVE.BasicModels.PageTransitions;

namespace HiVE.BasicModels.BoxItems
{
    public class BoxMainSettings
    {
        #region MainWindow.Display

        /// <summary>
        /// Get Update Basic Toggle the scale between 1.0 and X.
        /// Permission to FrameworkElement ScaleTransform!
        /// Default is True for Test
        /// </summary>
        public bool PermissionScaleTransform { get; set; } = false;

        /// <summary>
        /// Get Update Basic Toggle the scale between 1.0 and X.
        /// Scale Display from 1920*1080 to 1280*720 = 0.667
        /// Scale Display from 1920*1080 to 720*405 = 0.375
        /// Scale Display from 1920*1080 to 640*350 = 0.324
        /// Scale Display from 1024*768 to 854*640 = 0.833
        /// Scale Display from 1024*768 to 816*640 = 0.833
        /// Scale Display from 1024*768 to 960*720 = 0.937
        /// </summary>
        public double MainWindowHeight { get; set; } = 720;

        /// <summary>
        /// Get Update Basic Toggle the scale between 1.0 and X.
        /// Scale Display from 1920*1080 to 1280*720 = 0.667
        /// Scale Display from 1920*1080 to 720*405 = 0.375
        /// Scale Display from 1920*1080 to 640*350 = 0.324
        /// Scale Display from 1024*768 to 854*640 = 0.833
        /// Scale Display from 1024*768 to 816*640 = 0.833
        /// Scale Display from 1024*768 to 960*720 = 0.937
        /// </summary>
        public double MainWindowWidth { get; set; } = 1280;

        /// <summary>
        /// Get Update Basic Toggle the scale between 1.0 and X.
        /// Scale Display from 1920*1080 to 1280*720 = 0.667
        /// Scale Display from 1920*1080 to 720*405 = 0.375
        /// Scale Display from 1920*1080 to 640*350 = 0.324
        /// Scale Display from 1024*768 to 854*640 = 0.833
        /// Scale Display from 1024*768 to 816*640 = 0.833
        /// Scale Display from 1024*768 to 960*720 = 0.937
        /// </summary>
        public double ScaleTransformAnimation { get; set; } = 1;

        /// <summary>
        /// Get or set Basic Default TransitionType
        /// Default is TransitionType.Fade
        /// </summary>
        public TransitionType BasicDefaultTransitionType { get; set; } = TransitionType.Fade;

        /// <summary>
        /// Time in milliseconds to fire the OnTimePassed DispatcherTimerInterval event.
        /// Plain timer, for example going off every 1 secs
        /// Default is TimeSpan.FromMilliseconds(1 * 1000);
        /// </summary>
        public int TimePassedDispatcherTimerIntervalThreshold { get; set; } = (1 * 1000);

        /// <summary>
        /// Time in milliseconds to fire the OnTimePassed NextAllowedClick event.
        /// Plain timer, for example going off every 3 secs
        /// Default is TimeSpan.FromMilliseconds(3 * 1000);
        /// </summary>
        public int TimePassedNextAllowedClickThreshold { get; set; } = (3 * 1000);

        /// <summary>
        /// IsShow Message Error
        /// If false, so no show error of function in message
        /// Default = True
        /// </summary>
        public bool IsShowMessageError { get; set; } = true;
        /// <summary>
        /// Just show friendly message error if IsShowMessageError==True and this is True
        /// </summary>
        public bool IsShowNotFriendlyMessageError { get; set; } = true;
        /// <summary>
        /// IsAllow Full Permission Show Message Error
        /// If false, so no show error of function in message with limited permission
        /// Default = False
        /// </summary>
        public bool IsAllowFullPermissionShowMessageError { get; set; } = false;

        #endregion

        #region AutoRunStartup

        /// <summary>
        /// Permission for change status of auto run startup of this applicaion
        /// Default = False
        /// </summary>
        public bool IsAutoRunStartupSetPermission = true;

        /// <summary>
        /// Set and change status of auto run startup of this applicaion if is have permission for change status
        /// Default = True
        /// </summary>
        public bool IsEnabledAutoRunStartup = true;

        #endregion

        #region AppActivityTimer

        /// <summary>
        /// Time in milliseconds to fire the OnTimePassed event.
        /// Plain timer, for example going off every 30 secs
        /// Default is TimeSpan.FromMilliseconds(30 * 1000);
        /// </summary>
        public int TimePassedThreshold { get; set; } = (30 * 1000);

        /// <summary>
        /// Time in milliseconds to be idle before firing the OnInactivity event.
        /// How long to wait for no activity before firing OnInactive event - for example 5 minutes
        /// Default is TimeSpan.FromMilliseconds(5 * 60 * 1000);
        /// </summary>
        public int InactivityThreshold { get; set; } = (5 * 60 * 1000);

        /// <summary>
        /// Does a change in mouse position count as activity?
        /// Does mouse movement count as activity?
        /// Default is False;
        /// </summary>
        public bool IsWillMonitorMousePosition { get; set; } = false;

        #endregion

        #region MainWindow.Display.WpfAppControl

        /// <summary>
        /// Get Update Basic Toggle the scale between 1.0 and X.
        /// Permission to FrameworkElement ScaleTransform!
        /// Default is True for better show in display
        /// </summary>
        public bool PermissionScaleTransform_WpfAppControl { get; set; } = true;

        /// <summary>
        /// Get Update Basic Toggle the scale between 1.0 and X.
        /// Scale Display from 1920*1080 to 1280*720 = 0.667
        /// Scale Display from 1920*1080 to 720*405 = 0.375
        /// Scale Display from 1920*1080 to 640*350 = 0.324
        /// Scale Display from 1024*768 to 854*640 = 0.833
        /// Scale Display from 1024*768 to 816*640 = 0.833
        /// Scale Display from 1280*720 to 1140*640 = 0.889
        /// Scale Display from 1280*720 to 1067*600 = 0.833
        /// </summary>
        public double MainWindowHeight_WpfAppControl { get; set; } = 600;

        /// <summary>
        /// Get Update Basic Toggle the scale between 1.0 and X.
        /// Scale Display from 1920*1080 to 1280*720 = 0.667
        /// Scale Display from 1920*1080 to 720*405 = 0.375
        /// Scale Display from 1920*1080 to 640*350 = 0.324
        /// Scale Display from 1024*768 to 854*640 = 0.833
        /// Scale Display from 1024*768 to 816*640 = 0.833
        /// Scale Display from 1280*720 to 1140*640 = 0.889
        /// Scale Display from 1280*720 to 1067*600 = 0.833
        /// </summary>
        public double MainWindowWidth_WpfAppControl { get; set; } = 1067;

        /// <summary>
        /// Get Update Basic Toggle the scale between 1.0 and X.
        /// Scale Display from 1920*1080 to 1280*720 = 0.667
        /// Scale Display from 1920*1080 to 720*405 = 0.375
        /// Scale Display from 1920*1080 to 640*350 = 0.324
        /// Scale Display from 1024*768 to 854*640 = 0.833
        /// Scale Display from 1024*768 to 816*640 = 0.833
        /// Scale Display from 1280*720 to 1140*640 = 0.889
        /// Scale Display from 1280*720 to 1067*600 = 0.833
        /// </summary>
        public double ScaleTransformAnimation_WpfAppControl { get; set; } = 1;

        /// <summary>
        /// Time in milliseconds to fire and launch exe.
        /// Before WaitForInputIdle
        /// Wait for process to be created and enter idle condition
        /// Need to launch the exe and set to parrent
        /// A sleep of current thread
        /// Default is TimeSpan.FromMilliseconds(1 * 1000);
        /// </summary>
        public int BeforeTimeoutThreshold { get; set; } = (1 * 1000);

        /// <summary>
        /// Time in milliseconds to fire and launch exe.
        /// After WaitForInputIdle
        /// Wait for process to be created and enter idle condition
        /// Need to launch the exe and set to parrent
        /// A sleep of current thread
        /// Default is TimeSpan.FromMilliseconds(0 * 1000);
        /// </summary>
        public int AfterTimeoutThreshold { get; set; } = (0 * 1000);

        /// <summary>
        /// Name of applicaion exe to open in WpfAppControl
        /// </summary>
        public string AppExeName { get; set; } = "Notepad.exe";

        #endregion

        #region GooglePlacesAPI Settings

        /// <summary>
        /// To get address and other details
        /// Default = AIzaSyA4SNXJ4-PG-_6G7UMh205cSpUrOQiliz8
        /// </summary>
        public string GoogleAPIKey { get; set; } = "GoogleAPIKey";
        /// <summary>
        /// Default = 43.654454, -79.380721
        /// </summary>
        public string BiasLocation { get; set; } = "BiasLocation";
        /// <summary>
        /// Default = 5000
        /// </summary>
        public string BiasRadius { get; set; } = "5000";
        /// <summary>
        /// Default = ChIJ29RmSa80K4gRQGpGvCRwDW0
        /// </summary>
        public string BasePlaceId { get; set; } = "BasePlaceId";

        #endregion

        #region DataManagement

        /// <summary>
        /// Get Admin Permission
        /// Permission to do some all functions with no limited!
        /// Default is False
        /// If is true, then can login Without user and password
        /// </summary>
        public bool IsAdminPermission { get; set; } = false;

        /// <summary>
        /// Get OR Set Number of Decimal Places To Use in Currency/Numeric Values
        /// Default = 2
        /// </summary>
        public int DecimalDigits { get; set; } = 2;

        /// <summary>
        /// Get Limited Data OR Functions
        /// Permission to not do some long functions!
        /// Default is True for Test
        /// </summary>
        public bool IsLimitedProductTypeLicensed { get; set; } = false;

        /// <summary>
        /// Permission to allow use of  LMEContext or not in some functions!
        /// Default is True
        /// </summary>
        public bool IsLMEContextPermission { get; set; } = true;

        /// <summary>
        /// Get Refresh Basic Directory for extra files
        /// Permission to update extra datas
        /// </summary>
        public bool RefreshBasicExtraDataDirectory { get; set; } = true;

        /// <summary>
        /// Get Update Basic Directory for extra files
        /// Forced to update extra datas
        /// </summary>
        public bool UpdateBasicExtraDataDirectory { get; set; } = false;

        #endregion

        #region SpecialMainSettings

        /// <summary>
        /// Time in milliseconds to fire the OnTimePassed NextAllowedRunFunction event.
        /// Plain timer, for example going off every 120 secs
        /// Default is TimeSpan.FromMilliseconds(120 * 1000);
        /// </summary>
        public int TimePassedNextAllowedRunFunctionThreshold_RefreshInProgressLME_PTPHiVEPOSEntities { get; set; } = (120 * 1000);

        /// <summary>
        /// Time in milliseconds to fire the OnTimePassed NextAllowedRunFunction event.
        /// Plain timer, for example going off every 15 secs
        /// Default is TimeSpan.FromMilliseconds(15 * 1000);
        /// </summary>
        public int TimePassedNextAllowedRunFunctionThreshold_RefreshInProgressCartOrders { get; set; } = (15 * 1000);

        #endregion
    }
}
