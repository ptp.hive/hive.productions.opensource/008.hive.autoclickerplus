﻿using System;
using static HiVE.BasicModels.BoxItems.BoxSpecialMethods;

namespace HiVE.BasicModels.BoxItems
{
    public class BoxNotification
    {
        public string Caption { get; set; } = string.Empty;
        public string MessageInformation { get; set; } = string.Empty;
        public string ResultDescription { get; set; } = string.Empty;
        public string Result { get; set; } = string.Empty;
        public bool IsRead { get; set; } = false;
        public DateTime InsertTime { get; set; } = DateTime.Now;
        public bool IsToUpper { get; set; } = true;
        public bool IsSuccessfullyInfo { get; set; } = true;
        public MethodContentPageSenderType ContentPageSenderTypeSource { get; set; }
        public Uri GoBackSource { get; set; }
    }
}
