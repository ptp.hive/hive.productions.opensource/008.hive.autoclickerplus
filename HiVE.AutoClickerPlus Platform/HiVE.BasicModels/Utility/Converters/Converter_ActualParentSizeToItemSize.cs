﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_ActualParentSizeToItemSize : IMultiValueConverter
    {
        private const double _commonMargin = 32;
        private const double _specialPasswordMargin = 4;
        private const int margin = 2;

        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            double returnValue = 0;

            try
            {
                /// Always test MultiValueConverter inputs for non-null
                /// (to avoid crash bugs for views in the designer)
                if (values[0] is double && values[1] is int)
                {
                    double ActualSize = (double)values[0];
                    int Count = (int)values[1];

                    returnValue = ((ActualSize - ((int)Count * margin)) / (int)ActualSize);

                    if (returnValue < 0)
                    { returnValue = 0; }
                }

                if (returnValue < 0)
                { returnValue = 0; }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
