﻿using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.Utility.Helpers;
using System;
using static HiVE.BasicModels.BoxItems.BoxSpecialMethods;

namespace HiVE.BasicModels.UI.Controls.NotificationBars.Models
{
    public class SetDefaultNotificationDetails
    {
        #region Feilds

        public const string StartCartOrderCaption = "#";

        #endregion

        #region Common Methods

        /// <summary>
        /// Refresh InformationDetails
        /// Set ToUpper charcters if IsToUpper==True
        /// </summary>
        public static void RefreshInformationDetails(BoxInformationDetails InformationDetails)
        {
            try
            {
                if (InformationDetails != null && InformationDetails.IsToUpper)
                {
                    InformationDetails.Caption = InformationDetails.Caption.ToUpper();
                    InformationDetails.MessageInformation = InformationDetails.MessageInformation.ToUpper();
                    InformationDetails.ResultDescription = InformationDetails.ResultDescription.ToUpper();
                    InformationDetails.Result = InformationDetails.Result.ToUpper();
                    InformationDetails.ButtonFinalResult = InformationDetails.ButtonFinalResult.ToUpper();
                    InformationDetails.ButtonFailureResult = InformationDetails.ButtonFailureResult.ToUpper();
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        public static BoxNotification BoxInformationDetailsToBoxNotification(BoxInformationDetails InformationDetails)
        {
            BoxNotification oBoxNotification = null;

            try
            {
                if (InformationDetails != null)
                {
                    RefreshInformationDetails(InformationDetails);

                    oBoxNotification = new BoxNotification();

                    oBoxNotification.Caption = InformationDetails.Caption;
                    oBoxNotification.MessageInformation = InformationDetails.MessageInformation;
                    oBoxNotification.ResultDescription = InformationDetails.ResultDescription;
                    oBoxNotification.Result = InformationDetails.Result;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return oBoxNotification;
        }

        #endregion

        #region Set Default Information Details - CartOrder

        /// <summary>
        /// Set Default Hold CartOrder Information Details
        /// </summary>
        /// <param name="InformationDetails"></param>
        public static void HoldCartOrder(BoxInformationDetails InformationDetails)
        {
            try
            {
                InformationDetails.ContentPageSenderTypeSource = MethodContentPageSenderType.SubmitOrderPageUC;
                InformationDetails.IsToUpper = false;

                if (InformationDetails.IsSuccessfullyInfo)
                {
                    //GetLMEContext.Instance.IsActiveDataContext_CartOrderDetails = false;

                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Submitted Successfully!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Number:";

                    InformationDetails.IsSuccessfullyInfo = true;
                    InformationDetails.ButtonFinalResult = "OK! GOT IT.";
                    InformationDetails.ButtonFailureResult = string.Empty;
                }
                else
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Submitted Failure!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Failed!";

                    InformationDetails.IsSuccessfullyInfo = false;
                    InformationDetails.ButtonFinalResult = "TRY AGAIN!";
                    InformationDetails.ButtonFailureResult = "CANCEL ORDER";
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Default Print CartOrder Information Details
        /// </summary>
        /// <param name="InformationDetails"></param>
        public static void PrintCartOrder(BoxInformationDetails InformationDetails)
        {
            try
            {
                InformationDetails.ContentPageSenderTypeSource = MethodContentPageSenderType.CheckOutPageUC;
                InformationDetails.IsToUpper = false;

                if (InformationDetails.IsSuccessfullyInfo)
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Printed Successfully!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Number:";

                    InformationDetails.IsSuccessfullyInfo = true;
                    InformationDetails.ButtonFinalResult = "OK! GOT IT.";
                    InformationDetails.ButtonFailureResult = string.Empty;
                }
                else
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Printer Failure!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Printer Failed!";

                    InformationDetails.IsSuccessfullyInfo = false;
                    InformationDetails.ButtonFinalResult = "TRY AGAIN!";
                    InformationDetails.ButtonFailureResult = "CANCEL ORDER";
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Default CheckOut CartOrder Information Details
        /// </summary>
        /// <param name="InformationDetails"></param>
        public static void CheckOutCartOrder(BoxInformationDetails InformationDetails)
        {
            try
            {
                InformationDetails.ContentPageSenderTypeSource = MethodContentPageSenderType.CheckOutPageUC;
                InformationDetails.IsToUpper = false;

                if (InformationDetails.IsSuccessfullyInfo)
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Checkout Successfully!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Number:";

                    InformationDetails.IsSuccessfullyInfo = true;
                    InformationDetails.ButtonFinalResult = "OK! GOT IT.";
                    InformationDetails.ButtonFailureResult = string.Empty;
                }
                else
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Checkout Failure!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Failed!";

                    InformationDetails.IsSuccessfullyInfo = false;
                    InformationDetails.ButtonFinalResult = "TRY AGAIN!";
                    InformationDetails.ButtonFailureResult = "CANCEL ORDER";
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Default PrintPlusCheckOut CartOrder Information Details
        /// </summary>
        /// <param name="InformationDetails"></param>
        public static void PrintPlusCheckOutCartOrder(BoxInformationDetails InformationDetails)
        {
            try
            {
                InformationDetails.ContentPageSenderTypeSource = MethodContentPageSenderType.CheckOutPageUC;
                InformationDetails.IsToUpper = false;

                if (InformationDetails.IsSuccessfullyInfo)
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Print and Checkout Successfully!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Number:";

                    InformationDetails.IsSuccessfullyInfo = true;
                    InformationDetails.ButtonFinalResult = "OK! GOT IT.";
                    InformationDetails.ButtonFailureResult = string.Empty;
                }
                else
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Print and Checkout Failure!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Failed!";

                    InformationDetails.IsSuccessfullyInfo = false;
                    InformationDetails.ButtonFinalResult = "TRY AGAIN!";
                    InformationDetails.ButtonFailureResult = "CANCEL ORDER";
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Default Get CartOrder Rdf Information Details
        /// </summary>
        /// <param name="InformationDetails"></param>
        public static void GetCartOrderRdf(BoxInformationDetails InformationDetails)
        {
            try
            {
                InformationDetails.ContentPageSenderTypeSource = MethodContentPageSenderType.SubmitOrderPageUC;
                InformationDetails.IsToUpper = false;

                if (InformationDetails.IsSuccessfullyInfo)
                {
                    //GetLMEContext.Instance.IsActiveDataContext_CartOrderDetails = false;

                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Get CartOrder Rdf Successfully!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Number:";

                    InformationDetails.IsSuccessfullyInfo = true;
                    InformationDetails.ButtonFinalResult = "OK! GOT IT.";
                    InformationDetails.ButtonFailureResult = string.Empty;
                }
                else
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Get CartOrder Rdf Failure!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Failed!";

                    InformationDetails.IsSuccessfullyInfo = false;
                    InformationDetails.ButtonFinalResult = "TRY AGAIN!";
                    InformationDetails.ButtonFailureResult = "CANCEL ORDER";
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        /// <summary>
        /// Set Default Get CartOrder Rdf Information Details
        /// </summary>
        /// <param name="InformationDetails"></param>
        public static void CancelCartOrder(BoxInformationDetails InformationDetails)
        {
            try
            {
                InformationDetails.ContentPageSenderTypeSource = MethodContentPageSenderType.SubmitOrderPageUC;
                InformationDetails.IsToUpper = false;

                if (InformationDetails.IsSuccessfullyInfo)
                {
                    //GetLMEContext.Instance.IsActiveDataContext_CartOrderDetails = false;

                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Cancel Order Successfully!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Number:";

                    InformationDetails.IsSuccessfullyInfo = true;
                    InformationDetails.ButtonFinalResult = "OK! GOT IT.";
                    InformationDetails.ButtonFailureResult = string.Empty;
                }
                else
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            InformationDetails.Caption);
                    InformationDetails.Result = "Cancel Order Failure!";
                    //InformationDetails.MessageInformation = SelectedAddress.AddressDetail;
                    InformationDetails.ResultDescription = "Order Failed!";

                    InformationDetails.IsSuccessfullyInfo = false;
                    InformationDetails.ButtonFinalResult = "TRY AGAIN!";
                    InformationDetails.ButtonFailureResult = "CANCEL ORDER";
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion

        #region Set Default Information Details - CartOrder

        /// <summary>
        /// Set Default Get BasicData Information Details
        /// </summary>
        /// <param name="InformationDetails"></param>
        public static void GetBasicData(BoxInformationDetails InformationDetails)
        {
            try
            {
                InformationDetails.ContentPageSenderTypeSource = MethodContentPageSenderType.SubmitOrderPageUC;
                InformationDetails.IsToUpper = false;

                if (InformationDetails.IsSuccessfullyInfo)
                {
                    //GetLMEContext.Instance.IsActiveDataContext_CartOrderDetails = false;

                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            "Root");
                    InformationDetails.Result = "Get Basic Data Successfully!";
                    InformationDetails.MessageInformation = "Get Basic Data Just For First Time";
                    InformationDetails.ResultDescription = "Basic Data:";

                    InformationDetails.IsSuccessfullyInfo = true;
                    InformationDetails.ButtonFinalResult = "OK! GOT IT.";
                    InformationDetails.ButtonFailureResult = string.Empty;
                }
                else
                {
                    //InformationDetails.Caption = "#" + CartOrder.Rdf;
                    InformationDetails.Caption =
                        string.Format(
                            "{0}{1}",
                            StartCartOrderCaption,
                            "Root");
                    InformationDetails.Result = "Get Basic Data Failure!";
                    InformationDetails.MessageInformation = "Get Basic Data Just For First Time";
                    InformationDetails.ResultDescription = "Get Data Failed!";

                    InformationDetails.IsSuccessfullyInfo = false;
                    InformationDetails.ButtonFinalResult = "TRY AGAIN!";
                    InformationDetails.ButtonFailureResult = "CANCEL";
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }

        #endregion
    }
}
