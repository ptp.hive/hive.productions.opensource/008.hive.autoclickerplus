﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.MaskedTextBoxs
{
    public class Converter_CountZeroToVisibility : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Visible;

            try
            {
                if (value != null)
                {
                    if ((int)value > 0)
                    {
                        returnValue = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from NotVisibility back to CountZero");
        }
    }
}
