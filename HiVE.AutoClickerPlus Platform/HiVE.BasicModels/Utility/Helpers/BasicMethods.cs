﻿using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Reflection;
using System.Windows;
using static HiVE.BasicModels.BoxItems.BoxBasicMethods;

namespace HiVE.BasicModels.Helpers
{
    public static class BasicMethods
    {
        #region Common Basic Methods

        /// <summary>
        /// Get or set Basic Culture
        /// Default = MethodCulture.fa_IR
        /// </summary>
        private static MethodCulture _basicCulture = MethodCulture.en_US;

        /// <summary>
        /// Get or set Basic Culture
        /// Default = MethodCulture.fa_IR
        /// </summary>
        public static MethodCulture BasicCulture
        {
            get { return (_basicCulture); }
            set { _basicCulture = value; }
        }

        /// <summary>
        /// Get or set Basic FlowDirection
        /// Default = System.Windows.FlowDirection.LeftToRight
        /// </summary>
        private static System.Windows.FlowDirection _basicFlowDirection = System.Windows.FlowDirection.LeftToRight;

        /// <summary>
        /// Get or set Basic FlowDirection
        /// Default = System.Windows.FlowDirection.LeftToRight
        /// </summary>
        public static System.Windows.FlowDirection BasicFlowDirection
        {
            get { return (_basicFlowDirection); }
            set { _basicFlowDirection = value; }
        }

        /// <summary>
        /// Get or set Basic Show Friendly Message
        /// Default = true
        /// </summary>
        private static bool _isShowFriendlyMessage = true;

        /// <summary>
        /// Get or set Basic Show Friendly Message
        /// Default = true
        /// </summary>
        public static bool IsShowFriendlyMessage
        {
            get { return _isShowFriendlyMessage; }
            set { value = _isShowFriendlyMessage; }
        }

        /// <summary>
        /// Get or set Basic MainMenuContent
        /// Default = string.Empty;
        /// </summary>
        private static string _basicMainMenuContent = string.Empty;

        /// <summary>
        /// Get or set Basic MainMenuContent
        /// Default = string.Empty;
        /// </summary>
        public static string BasicMainMenuContent
        {
            get
            {
                try
                {
                    if (BasicCulture == MethodCulture.fa_IR)
                    {
                        _basicMainMenuContent = "منوی اصلی";
                    }
                    /// <summary>
                    /// if(culture.ToUpper() == BasicCulture.Default.ToString().ToUpper())
                    /// OR if(culture.ToUpper() == BasicCulture.en_US.ToString().ToUpper())
                    /// OR if(culture.ToUpper() == BasicCulture.Custom.ToString().ToUpper())
                    /// OR another case
                    /// </summary>
                    else
                    {
                        _basicMainMenuContent = "Main Menu";
                    }
                }
                catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

                return _basicMainMenuContent;
            }
            set
            {
                _basicMainMenuContent = value;
            }
        }

        /// <summary>
        /// Set Basic MainMenuContent
        /// Default = string.Empty;
        /// </summary>
        public static string SetBasicMainMenuContent(MethodCulture culture)
        {
            try
            {
                if (culture == MethodCulture.fa_IR)
                {
                    _basicMainMenuContent = "منوی اصلی";
                }
                /// <summary>
                /// if(culture.ToUpper() == BasicCulture.Default.ToString().ToUpper())
                /// OR if(culture.ToUpper() == BasicCulture.en_US.ToString().ToUpper())
                /// OR if(culture.ToUpper() == BasicCulture.Custom.ToString().ToUpper())
                /// OR another case
                /// </summary>
                else
                {
                    _basicMainMenuContent = "Main Menu";
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return _basicMainMenuContent;
        }

        /// <summary>
        /// Get or set Basic TrialMode
        /// Default = true
        /// </summary>
        private static bool _isTrialMode = true;

        /// <summary>
        /// Get or set Basic TrialMode
        /// Default = true
        /// </summary>
        public static bool IsTrialMode
        {
            get { return _isTrialMode; }
            set { value = _isTrialMode; }
        }

        /// <summary>
        /// Get or set Basic ApplicationRestart
        /// Default = true
        /// </summary>
        private static bool _isApplicationRestart = true;

        /// <summary>
        /// Get or set Basic ApplicationRestart
        /// Default = true
        /// </summary>
        public static bool IsApplicationRestart
        {
            get { return _isApplicationRestart; }
            set { value = _isApplicationRestart; }
        }

        /// <summary>
        /// Get or set Basic Db_ReservationRecords
        /// Default = 100
        /// </summary>
        private static int _db_ReservationRecords = 100;

        /// <summary>
        /// Get or set Basic Db_ReservationRecords
        /// Default = 100
        /// </summary>
        public static int Db_ReservationRecords
        {
            get { return _db_ReservationRecords; }
            set { value = _db_ReservationRecords; }
        }

        /// <summary>
        /// Get or set Basic Db_ReservationUserRecords
        /// Default = 1,000,000,000,000
        /// Default = 1,00,00,00,00,00,00 for 6 level
        /// </summary>
        private static long _db_ReservationUserRecords = System.Convert.ToInt64(System.Math.Pow(Db_ReservationRecords, 6));

        /// <summary>
        /// Get or set Basic Db_ReservationUserRecords
        /// Default = 1,000,000,000,000
        /// Default = 1,00,00,00,00,00,00 for 6 level
        /// </summary>
        public static long Db_ReservationUserRecords
        {
            get { return _db_ReservationUserRecords; }
            set { value = _db_ReservationUserRecords; }
        }

        #endregion

        #region Special BasicMethods

        public static BoxMainSettings CurrentMainSettings { get; set; }

        /// <summary>
        /// Get or set Basic DefaultPath
        /// </summary>
        private static string _basicDefaultPath =
            string.Format(
                "{0}\\{1}\\{2} {3}",
                Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                BasicAssemblyAttributeAccessors.EntryAssemblyCompany,
                BasicAssemblyAttributeAccessors.EntryAssemblyProduct,
                BasicAssemblyAttributeAccessors.EntryAssemblyVersionMajor);

        /// <summary>
        /// Get or set Basic DefaultPath
        /// </summary>
        public static string BasicDefaultPath
        {
            get { return (_basicDefaultPath); }
            set { _basicDefaultPath = value; }
        }

        /// <summary>
        /// Get Basic Directory of this application
        /// </summary>
        private static string _baseDirectory =
            System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        //System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

        /// <summary>
        /// Get Basic Directory of this application
        /// </summary>
        public static string BaseDirectory
        {
            get { return (_baseDirectory); }
        }

        /// <summary>
        /// Get Basic Directory for extra files
        /// </summary>
        private static string _basicExtraDataDirectory = "Data";

        /// <summary>
        /// Get Basic Directory for extra files
        /// </summary>
        public static string BasicExtraDataDirectory
        {
            get { return (_basicExtraDataDirectory); }
        }

        /// <summary>
        /// Get Basic Directory for extra images
        /// </summary>
        private static string _basicExtraImageDirectory = "Images";

        /// <summary>
        /// Get Basic Directory for extra images
        /// </summary>
        public static string BasicExtraImageDirectory
        {
            get { return (_basicExtraImageDirectory); }
        }

        /// <summary>
        /// Get or set Basic Default AnimatedEventIsPressed Duration
        /// Default is 100 Millisecond
        /// </summary>
        private static Duration _basicAnimatedEventIsPressedDuration = new Duration(TimeSpan.FromMilliseconds(100));

        /// <summary>
        /// Get or set Basic Default AnimatedEventIsPressed Duration
        /// Default is 100 Millisecond
        /// </summary>
        public static Duration BasicAnimatedEventIsPressedDuration
        {
            get { return (_basicAnimatedEventIsPressedDuration); }
        }

        #endregion
    }
}
