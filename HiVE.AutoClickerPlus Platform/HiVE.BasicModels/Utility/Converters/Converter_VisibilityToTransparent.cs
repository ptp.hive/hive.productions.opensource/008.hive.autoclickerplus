﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_VisibilityToTransparent : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Brush returnValue = (SolidColorBrush)(new BrushConverter().ConvertFromString("#00000000"));

            try
            {
                if (value != null)
                {
                    if ((Visibility)value != Visibility.Visible)
                    {
                        returnValue = (SolidColorBrush)(new BrushConverter().ConvertFromString("#FF2D3E52"));
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from Brushes back to Visibility");
        }
    }
}
