﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.MetroNumericInputs
{
    public class Converter_BooleanToVisibility : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Collapsed;

            try
            {
                if (value != null)
                {
                    if ((bool)value == true)
                    {
                        returnValue = Visibility.Visible;
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from Visibility back to Boolean");
        }
    }
}
