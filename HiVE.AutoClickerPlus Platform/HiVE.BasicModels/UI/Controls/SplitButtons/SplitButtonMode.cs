﻿namespace HiVE.BasicModels.UI.Controls.SplitButtons
{
    public enum SplitButtonMode
    {
        Split,
        Dropdown,
        Button
    }
}
