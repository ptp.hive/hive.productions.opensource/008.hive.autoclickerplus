﻿using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.MaskedTextBoxs
{
    public class Converter_TextInputToVisibility : IMultiValueConverter
    {
        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                // Always test MultiValueConverter inputs for non-null
                // (to avoid crash bugs for views in the designer)
                if (values[0] is bool && values[1] is bool)
                {
                    bool hasText = !(bool)values[0];
                    bool hasFocus = (bool)values[1];

                    if (hasFocus || hasText)
                        return Visibility.Collapsed;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return Visibility.Visible;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
