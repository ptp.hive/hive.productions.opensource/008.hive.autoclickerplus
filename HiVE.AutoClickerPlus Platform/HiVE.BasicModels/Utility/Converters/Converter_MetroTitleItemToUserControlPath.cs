﻿using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.Utility.Converters
{
    public class Converter_MetroTitleItemToUserControlPath : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            object returnValue = null;

            try
            {
                if (value != null)
                {
                    var metroTitleItem = (BoxMetroTitle)value;
                    returnValue = metroTitleItem.UserControlPath;
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from UserControlPath back to MetroTitleItem");
        }
    }
}
