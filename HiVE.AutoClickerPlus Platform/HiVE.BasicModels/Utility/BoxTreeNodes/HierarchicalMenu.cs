﻿using System.Collections.Generic;
using System.Collections;
using System.Windows.Controls;
using System;
using System.Windows;
using System.Windows.Media.Imaging;
using HiVE.BasicModels.Utility.Helpers;

namespace HiVE.BasicModels.BoxTreeNodes
{
    public class MenuObj
    {
        public int Id;
        public string Name;
        public int ParentId;
        public bool HasEvent;
        public string ImageFile;
    }

    public class HierarchicalMenu
    {
        RoutedEventHandler MouseDownEventHandler;

        public HierarchicalMenu(RoutedEventHandler MouseDownEventHandler)
        {
            this.MouseDownEventHandler = MouseDownEventHandler;
        }

        Hashtable MenuObjs = new Hashtable();

        public ContextMenu GetContextMenu(List<MenuObj> MenuObjects)
        {
            ContextMenu contextMenu = new ContextMenu();

            try
            {
                MenuObjs[0] = contextMenu;

                foreach (MenuObj mo in MenuObjects)
                {
                    MenuItem mi = new MenuItem();
                    mi.Header = mo.Name;
                    mi.Tag = mo.Id;

                    if (mo.ImageFile != null) mi.Icon = new Image() { Source = new BitmapImage(new Uri(mo.ImageFile, UriKind.Relative)), Width = 22 };

                    if (mo.HasEvent) mi.Click += MouseDownEventHandler;

                    MenuObjs[mo.Id] = mi;

                    if (mo.ParentId == 0)
                    {
                        (MenuObjs[mo.ParentId] as ContextMenu).Items.Add(mi);
                    }
                    else
                    {
                        (MenuObjs[mo.ParentId] as MenuItem).Items.Add(mi);
                    }
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }

            return contextMenu;
        }
    }
}