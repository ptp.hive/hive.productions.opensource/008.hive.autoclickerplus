﻿using HiVE.BasicModels.BoxItems;
using HiVE.BasicModels.Utility.Helpers;
using System;
using System.Collections.ObjectModel;
using static HiVE.BasicModels.BoxItems.BoxBasicMethods;

namespace HiVE.BasicModels.Helpers
{
    public class ColorList : ObservableCollection<BoxColorDetails>
    {
        public ColorList()
        {
            try
            {
                /// Get a list of color names.
                var results = EnumExtensions.GetEnumValues(new MethodColors());
                /// Add each one to this collection:
                foreach (var color in results)
                {
                    this.Add(
                        new BoxColorDetails(
                            color.ToString(),
                            color.ToDescriptionString()));
                }
            }
            catch (Exception ex) { TryCatch.GetCEM_Error(ex); }
        }
    }
}
