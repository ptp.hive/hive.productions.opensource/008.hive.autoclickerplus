﻿using System.ComponentModel;

namespace HiVE.BasicModels.MaskedTextBoxs
{
    public enum MaskType
    {
        /// <summary>
        /// None Mask
        /// Like as a normal textbox
        /// </summary>
        [SpecialDescription("")]
        [Description("")]
        None,
        /// <summary>
        /// Set Custom Mask
        /// </summary>
        [SpecialDescription("")]
        [Description("Custom")]
        Custom,

        [Description("^([0-9_\\-]+)$")]
        Number,
        [SpecialDescription(")-(")]
        [Description(@"(\d{3})(\d{3})(\d{4})($1) $2-$3")]
        Mobile,
        [SpecialDescription(")-(")]
        [Description(@"\d{2}-\d{3}-\d{4}-\d{3}")]
        Phone,
        [SpecialDescription("")]
        [Description(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        Email,

        [SpecialDescription("")]
        [Description(@"^-?\d+$")]
        Integer,
        [SpecialDescription("")]
        [Description("[+-]?\\d*\\.?\\d+")]
        DoublePlusChar,
        [SpecialDescription("")]
        [Description(@"^[0-9]*(?:\.[0-9]*)?$")]
        DoubleWithDot,
        [SpecialDescription("")]
        [Description(@"^[0-9]*(?:\/[0-9]*)?$")]
        DoubleWithBackSlash,
    }
}
